<?php

namespace App\Http\Middleware;

use Closure;

class CheckAPITokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //$data = $request->all();
        //$apiToken = $data['api_token'];

        $apiToken = $request->get('api_token');
        if ($apiToken == null) {
            return response()->json([
                'failed' => true,
                'errors' => ['API token is required.'],
                'data' => null,
            ]);
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiToken = $request->get('api_token');
        $user = User::where('api_token', '=', $apiToken)->first();
        if ($user == null) {
            return response()->json([
                'failed' => true,
                'errors' => ['API token is not matching any user.'],
                'data' => null,
            ]);
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers;

use App\GuestMessage;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TestController extends Controller
{
    /*
     *
     * */
    public function test1() {
        $s = "ali,mahmoud,ashraf";
        $arr = explode(",", $s);

        $record = Product::find(150);
        $s = $record->keywords;
        $arr = explode(",", $s);

        foreach($arr as $item) {
            echo $item . "<br>";
        }

    }

    public function test2(Request $request) {
        $rules = [
            'f' => 'required|max:2048|min:1|mimes:jpg,jpeg',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            dd($validator->errors());
        }

        $f = $request->file('f');
        $ext = $f->getClientOriginalExtension();
        $name = $f->getClientOriginalName();
        $hashedName = sha1(time()) . "." . $ext;

        Storage::disk('public')->put($hashedName, File::get($f));
        return 1;
    }
}

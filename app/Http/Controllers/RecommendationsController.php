<?php

namespace App\Http\Controllers;

use App\OrderDetail;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RecommendationsController extends Controller
{
    public function index() {

        $topSales = OrderDetail::select(DB::raw('sum(amount) as sAmount, product_id'))
            ->where('product_id', '!=', 'null')
            ->where('is_available', '=', true)
            ->groupBy('product_id')
            ->orderBy('sAmount', 'DESC')
            ->take(10)
            ->get();
       // dd($Recoprod);

        $data = [
            'topSales' => $topSales,

        ];
        return view('user/recommendations', ['data' => $data]);
    }

}

<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductDetailsController extends Controller
{
    //
    public function index()
    {

        $product = Product::where('is_available', '=', true)->get();
        $data = [
            'product' => $product,
        ];

        return view('user/product-details', ['data' => $data]);


    }

//get single  view
    public function getDetailsView($prodID)
    {
        $availproduct = Product::find($prodID)->toArray();
        $data = [

            'productName' => $availproduct['name'],
            'productPrice'=>$availproduct['selling_price'],
            'productDiscount'=>$availproduct['discount'],
            'productWords'=>$availproduct['keywords'],
            'productDesc'=>$availproduct['description'],
        ];
        return view('/user/product-details', ['data' => $data]);
    }
}
<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    //
    public function index(){
        $availCats =  Category::where('is_available', '=', true)->get();
        $product =  Product::where('is_available', '=', true)->get();

        $data = [
            'availCats' => $availCats,
            'product' => $product
        ];
        return view('user/category', ['data' => $data]);
    }

    //get single category view
    public function getCatView($catID) {
        $category = Category::find($catID)->toArray();
        $products = Product::where('category_id', '=', $catID)->get()->toArray();
        $data = [
            'categoryName' => $category['name'],
            'products' => $products
        ];
        return view('/user/category', ['data' => $data]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Product;
use App\UnconfirmedOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
   public function index() {
       return view('user/cart');
   }

   public function doAddToCart($productId) {
       $unconfirmedOrder = UnconfirmedOrder::where('product_id', '=', $productId)
           ->where('user_id', '=', Auth::id())->first();

       if($unconfirmedOrder) {
           $unconfirmedOrder->quantity= $unconfirmedOrder->quantity + 1;
           $unconfirmedOrder->save();
       } else {
           $newUnConfOrder = new UnconfirmedOrder();
           $newUnConfOrder->product_id = $productId;
           $newUnConfOrder->user_id = Auth::id();
           $newUnConfOrder->quantity = 1;
           $newUnConfOrder->is_available = 1;
           $newUnConfOrder->save();
       }
       return back();
   }

    public function getDeleteItem() {
        return view('user/delete-cart-item');
    }

    public function doDeleteItem($itemID) {
        $unconfirmedOrder = UnconfirmedOrder::where('id', '=', $itemID)->first();

        if($unconfirmedOrder) {
            $unconfirmedOrder->delete();
            $unconfirmedOrder->save();
        }
        //return back();
    }
}

<?php

namespace App\Http\Controllers;
use App\Category;

use App\Image;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function  index (){

        $availCats =  Category::where('is_available', '=', true)->get();

        $data = [
            'availCats' => $availCats,
        ];
        return view('user/categories', ['data' => $data]);

    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CategoriesDashController extends Controller
{
    public function getAddCategoryView(){
        if (Auth::check()) {
            if (User::find(Auth::id())->is_admin == true) {
                return view('dashboard/categories/add-category');
            }
            return redirect('/');
        }
    }
    public function formView(){
        return view('/dashboard/categories/category-form');
    }
    public function formAddCategory(Request $request){
        $data = $request->all();
        $rules = [
            'image' => 'required|image',
            'name' => 'required|min:3|max:75|unique:categories,name',
            'description' => 'max:500'
        ];

        //validation
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return redirect('dashboard/categories/category-form')
                ->withErrors($validator->errors());
        }

        $files = $request->file('image');
        $ext = $files->getClientOriginalExtension();
        $name = $files->getClientOriginalName();
        $newImgName = md5(time() . $name) . '.' . $ext;
        Storage::disk('public')->put($newImgName, \Illuminate\Support\Facades\File::get($files));

        $newCategory =  new Category() ;
        $newCategory -> name = $data['name'] ;
        $newCategory -> description = $data['description'] ;
        $newCategory -> is_available = true ;
        $newCategory -> save();

        $newImage = new Image();
        $newImage->category_id = DB::table('categories')->orderBy('id', 'desc')->first()->id;
        $newImage->path = 'uploads';
        $newImage->name = $newImgName;
        $newImage->extension = $ext;
        $newImage->save();

        return redirect('dashboard/categories/category-form');
    }
}

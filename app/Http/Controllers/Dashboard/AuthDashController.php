<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthDashController extends Controller
{
    public function getDashView()
    {
        if (Auth::check()) {
            if (User::find(Auth::id())->is_admin == true) {
                return view('dashboard/index');
            }
            return redirect('/');
        }
    }
}

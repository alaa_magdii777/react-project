<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SupportDashController extends Controller
{
   public function getSupportView() {
       return view('dashboard/support/index');
   }
}

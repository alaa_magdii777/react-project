<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Image;
use App\Product;
use App\Supplier;
use App\User;
use Faker\Provider\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductsDashController extends Controller
{
    public function getIndexView () {
        $products = \App\Product::paginate(15);

        return view('dashboard.products.index', compact('products'));
    }

    public function getAddProductView()
    {
        return view('dashboard.products.create');

    }

    public function getDeleteProductView($productID)
    {
        $product = Product::where('id', '=', $productID)->first();
        if($product) {
            $data = [
                'product_id' => $productID,
                'product_name' => Product::where('id', '=', $productID)->first()->name,
                'confirmation' => 'has been successfully deleted'
            ];
            \App\Product::where('id', '=', $data['product_id'])->delete();
            return view('dashboard/products/delete')
                ->with('status', 'Product has been successfully deleted!');
        }

       return redirect('/dashboard/products');

    }

    public function getEditView($productID) {
        $product = Product::where('id', '=', $productID)->first();
        if($product) {
            $product = Product::find($productID);
            $productImg = Image::where('product_id', '=', $productID)->first();

            $data = [
                'image' => $productImg->path . $productImg->name,
                'name' => $product->name,
                'category_id' => $product->category_id,
                'brand_id' => $product->brand_id,
                'supplier_id' => $product->supplier_id,
                'buying_price' => $product->buying_price,
                'selling_price' => $product->selling_price,
                'discount' => $product->discount,
                'description' => $product->description,
            ];

            return View('dashboard/products/edit', ['data' => $data]);
        }
        return redirect('/dashboard/products');
    }

    public function doAddProduct (Request $request)
    {
        $data = $request->all();
        $categories = [];
        $brands = [];
        $suppliers = [];

        foreach (Category::pluck('id') as $catID) {
            array_push($categories, $catID);
        }
        foreach (Brand::pluck('id') as $brandID) {
            array_push($brands, $brandID);
        }
        foreach (Supplier::pluck('id') as $supplierID) {
            array_push($suppliers, $supplierID);
        }

        $rules = [
            'image' => 'required|image',
            'name'=> 'required|min:3|max:75|unique:products',
            'category' => [
                'required',
                'numeric',
                Rule::in($categories)
            ],
            'buying-price' => 'required|numeric|min:0',
            'selling-price' => 'required|numeric|min:0',
            'discount' => 'numeric|min:0|max:100',
            'supplier' => [
                'required',
                'numeric',
                Rule::in($suppliers)
            ],
            'brand' => [
                'required',
                'numeric',
                Rule::in($brands)
            ],
            'description' => 'max:500'
        ];

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return redirect('dashboard/products/create')
                ->withErrors($validator->errors());
        }

        $files = $request->file('image');
        $ext = $files->getClientOriginalExtension();
        $name = $files->getClientOriginalName();
        $newImgName = md5(time() . $name) . '.' . $ext;
        Storage::disk('public')->put($newImgName, \Illuminate\Support\Facades\File::get($files));

        //storing data
        $newProduct = new Product();
        $newProduct->name = $data['name'];
        $newProduct->category_id = $data['category'];
        $newProduct->buying_price = $data['buying-price'];
        $newProduct->selling_price = $data['selling-price'];
        $newProduct->discount = $data['discount'];
        $newProduct->supplier_id = $data['supplier'];
        $newProduct->brand_id = $data['brand'];
        $newProduct->description = $data['description'];
        $newProduct->is_available = true;
        $newProduct->save();

        $newImage = new Image();
        $newImage->product_id = DB::table('products')->orderBy('id', 'desc')->first()->id;
        $newImage->path = 'uploads';
        $newImage->name = $newImgName;
        $newImage->extension = $ext;
        $newImage->save();

        return redirect('dashboard/products/create')
            ->with('status', 'Product has been successfully Added');
    }

    public function doEditProduct(Request $request, $productID) {
        $data = $request->all();
        $categories = [];
        $brands = [];
        $suppliers = [];
        $product = Product::find($productID);

        foreach (Category::pluck('id') as $catID) {
            array_push($categories, $catID);
        }
        foreach (Brand::pluck('id') as $brandID) {
            array_push($brands, $brandID);
        }
        foreach (Supplier::pluck('id') as $supplierID) {
            array_push($suppliers, $supplierID);
        }

        $imgRule = $files = $request->file('image') ? 'required|image' : '';

        $rules = [
            'image' => $imgRule,
            'name'=> 'required|min:3|max:75|unique:products,name,' . $productID,
            'category' => [
                'required',
                'numeric',
                Rule::in($categories)
            ],
            'buying-price' => 'required|numeric|min:0',
            'selling-price' => 'required|numeric|min:0',
            'discount' => 'numeric|min:0|max:100',
            'supplier' => [
                'required',
                'numeric',
                Rule::in($suppliers)
            ],
            'brand' => [
                'required',
                'numeric',
                Rule::in($brands)
            ],
            'description' => 'max:500'
        ];

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return redirect('dashboard/products/' . $productID . '/edit')
                ->withErrors($validator->errors());
        }

        if ($files = $request->file('image')) {
            //upload image
            $ext = $files->getClientOriginalExtension();
            $name = $files->getClientOriginalName();
            $newImgName = md5(time() . $name) . '.' . $ext;
            Storage::disk('public')->put($newImgName, \Illuminate\Support\Facades\File::get($files));
            //update image
            $productImage = Image::where('product_id', '=', $productID)->first();
            $productImage->path = 'uploads';
            $productImage->name = $newImgName;
            $productImage->extension = $ext;
            $productImage->save();
        }
        //update data
        $product->name = $data['name'];
        $product->category_id = $data['category'];
        $product->brand_id = $data['brand'];
        $product->supplier_id = $data['supplier'];
        $product->buying_price = $data['buying-price'];
        $product->selling_price = $data['selling-price'];
        $product->discount = $data['discount'];
        $product->description = $data['description'];
        $product->save();
        return redirect('dashboard/products/' . $productID . '/edit')
            ->with('status', 'Product has been successfully updated!');
    }
}

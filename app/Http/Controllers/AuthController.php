<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class AuthController extends Controller
{
    public function getLoginView() {
        if (Auth::check()) {
            return redirect('/');
        }
        return view('user.auth.login');
    }

    public function doLogin(Request $request) {
        $data = $request->all();

        // Validation
        $rules = [
            'username' => 'required|max:75',
            'password' => 'required|max:50',
        ];
        // Check
        $validator = Validator::make($data, $rules);


        if ($validator->fails()) {
            return redirect('/login')
                ->withErrors($validator->errors());
        }

        if (Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) {
            return redirect('/');


        }
  /*      elseif ('username'!=$data['username']){
            return redirect('/login')
                ->withErrors($validator->errors());
        }*/
        else {
            return redirect('/login')
                ->withErrors($validator->errors());


        }
    }

//


    public function doLogout() {
        Auth::logout();
        return redirect('/login');
    }
}


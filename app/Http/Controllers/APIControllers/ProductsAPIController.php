<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Product;

use Illuminate\Http\Request;

class ProductsAPIController extends Controller
{
    public function getProducts() {

        $products = Product::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' => $products
        ]);
    }


    public function getSelectedProduct(Request $request, $pid) {
        $product = Product::find($pid);

        if ($product == null) {
            return response()->json([
                'failed' => true,
                'errors' => ['Product is not found in the database.'],
                'data' => null
            ]);
        } else {
            return response()->json([
                'failed' => false,
                'errors' => null,
                'data' => $product
            ]);
        }
    }
}

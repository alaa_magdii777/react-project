<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthAPIController extends Controller
{
    public function doLogin(Request $request)
    {

        
        $data = $request->all();

        $rules = [
            'username' => 'required|max:75|min:3',
            'password' => 'required|max:100|min:6',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $reply = [
                'failed' => true,
                'errors' => $validator->errors()->toArray(),
                'data' => null,
            ];
            return response()->json($reply);
        }

        // Check
        if (Auth::attempt(['username' => $data['username'], 'password' => $data['password']])) {
            $apiToken = sha1(time());
            $user = Auth::user();
            $user->api_token = $apiToken;
            $user->save();

            $reply = [
                'failed' => false,
                'errors' => null,
                'data' => [
                    "apiToken" => $apiToken,
                    "fullName" => $user->first_name . " " . $user->last_name,
                    "imageUrl" => $user->image ? $user->image->path: null,
                ],
            ];
            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => ['Authentication failed'],
                'data' => null,
            ];
            return response()->json($reply);
        }
    }
}

<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\OrderDetail;

class TopSalesAPIController extends Controller
{
    public function getProductDetails() {

        $orderdetails = OrderDetail::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' =>  $orderdetails
        ]);
    }
}

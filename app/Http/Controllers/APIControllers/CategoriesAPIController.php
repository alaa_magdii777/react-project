<?php


namespace App\Http\Controllers\APIControllers;

use App\Category;
use App\Image;
use App\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesAPIController extends Controller
{


    public function getCategories()
    {

        $data = Category::where('is_available', '=', true)->get()->toArray();

        foreach ($data as $key => $datum) {
            $catID = $datum['id'];
            $numOfProducts = Product::select(DB::raw("count(id) AS c"))
                ->where('category_id', '=', $catID)->first()['c'];
            $data[$key]['inStock'] = $numOfProducts;
            $image = Image::where('category_id', '=', $catID)->first()->toArray();
            $imgURL = $image['path'] . $image['name'];
            $data[$key]['imgURL'] = $imgURL;
        }

        if($data) {
            return response()->json([
                'failed' => false,
                'errors' => null,
                'data' => $data
            ]);
        } else {
            return response()->json([
                'failed' => true,
                'errors' => ['No categories were found!'],
                'data' => null
            ]);
        }
    }

    public function getSelectedCategory(Request $request, $pid) {
        $category = Category ::find($pid);

        if ($category  == null) {
            return response()->json([
                'failed' => true,
                'errors' => ['Category is not found in the database.'],
                'data' => null
            ]);
        } else {
            return response()->json([
                'failed' => false,
                'errors' => null,
                'data' => $category
            ]);
        }
    }
}


<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Review;

class ReviewsAPIController extends Controller
{
   
    public function getReviews() {

        $reviews = Review::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' =>  $reviews
        ]);
    }
}

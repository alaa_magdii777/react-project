<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Chat;

class ChatsAPIController extends Controller
{
    public function getMessages() {

        $chats = Chat::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' =>  $chats
        ]);
    }
}

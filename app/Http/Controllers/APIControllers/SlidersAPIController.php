<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Slider;

class SlidersAPIController extends Controller
{
    
    public function getSliders() {

        $sliders = Slider::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' =>  $sliders
        ]);
    }

}

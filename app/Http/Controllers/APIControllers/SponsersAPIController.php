<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Sponser;

class SponsersAPIController extends Controller
{
    public function getSponsers() {

        $sponsers = Sponser::all();

        return response()->json([
            'failed' => false,
            'errors' => null,
            'data' =>  $sponsers 
        ]);
    }
}

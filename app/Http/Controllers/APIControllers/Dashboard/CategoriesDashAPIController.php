<?php

namespace App\Http\Controllers\APIControllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesDashAPIController extends Controller
{
    public function getCategories() {
        $categories = Category::all()->toArray();

        if ($categories) {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data'  => $categories
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => 'No categories were found!',
                'data'  => null
            ];

            return response()->json($reply);
        }
    }
}

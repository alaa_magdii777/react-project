<?php

namespace App\Http\Controllers\APIControllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashAPIController extends Controller
{
    public function getDashData() {
        $productsNumber = Product::select(DB::raw("count(id) AS count"))->first()->toArray('count');

        if ($productsNumber) {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data'  => $productsNumber
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => 'No products were found!',
                'data'  => null
            ];

            return response()->json($reply);
        }
    }
}

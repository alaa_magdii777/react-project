<?php

namespace App\Http\Controllers\APIControllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsDashAPIController extends Controller
{
    public function getProducts() {
        $products = Product::all()->toArray();

        if ($products) {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data'  => $products
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => 'No products were found!',
                'data'  => null
            ];

            return response()->json($reply);
        }
    }

    public function getProduct($productID) {
        $product = Product::find($productID);

        if ($product) {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data'  => $product
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => 'No products were found!',
                'data'  => null
            ];

            return response()->json($reply);
        }
    }
}

<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\SiteConfig;
use Dotenv\Validator;
use Illuminate\Http\Request;

class ContactUsAPIController extends Controller
{
    public  function doSendMessage(Request $request) {
        $data = $request->all();

        $rules = [
            'name' => 'required|min:3|max:75',
            'email' => 'required|max:100|email',
            'feedback' => 'required|max:1000',
            'phone' => 'required|max:50'
        ];

        $validator = \Illuminate\Support\Facades\Validator::make($data, $rules);

        if($validator->fails()) {
            $reply = [
                'failed' => true,
                'errors' => $validator->errors(),
                'data' => null
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data' => $data
            ];

            return response()->json($reply);
        }
    }

    public  function getContactInfo () {
        $data = SiteConfig::all()->toArray();

        if ($data) {
            $reply = [
                'failed' => false,
                'errors' => null,
                'data' => $data
            ];

            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => 'No Contact Info were found',
                'data' => null
            ];

            return response()->json($reply);
        }
    }
}

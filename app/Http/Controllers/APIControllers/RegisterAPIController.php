<?php

namespace App\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegisterAPIController extends Controller
{
    public function doLogin(Request $request)
    {

        
        $data = $request->all();

        $rules = [
            'username'=>'required|max:75|min:3',
            'password'=>'required|max:100|min:6',
        
            'first_name'=> 'required|max:75|min:3',
            'last_name'=> 'required|max:75|min:3',
            // 'gender'  =>'required',
            'email'   => 'required|max:100|min:10',
            'phone'   =>'required|max:30|min:10',
            'birth_date'=>'required',
            'address'  =>'required|max:125|min:10',
            // 'city'     =>'required',
        ];

        
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $reply = [
                'failed' => true,
                'errors' => $validator->errors()->toArray(),
                'data' => null,
            ];
            return response()->json($reply);
        }

        // Check
        if (Auth::attempt(['username' => $data['username'], 'password' => $data['password'],'first_name' => $data['first_name'],'last_name' => $data['last_name'],'email' => $data['email'],'address' => $data['address'],'phone' => $data['phone'],'birth_date' => $data['birth_date']])) {
            $apiToken = sha1(time());
            $user = Auth::user();
            $user->api_token = $apiToken;
            $user->save();

            $reply = [
                'failed' => false,
                'errors' => null,
                'data' => Auth::user(),
            ];
            return response()->json($reply);
        } else {
            $reply = [
                'failed' => true,
                'errors' => ['Authentication failed'],
                'data' => null,
            ];
            return response()->json($reply);
        }
    }
}

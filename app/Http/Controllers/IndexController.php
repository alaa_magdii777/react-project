<?php

namespace App\Http\Controllers;

use App\Category;
use App\Slider;
use App\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
       /* $userIDs = \App\User::pluck('id')->toArray();
        $existsUserIDs = \App\TechnicalSupport::pluck('user_id')->toArray();
        $is_unique = false;
        var_dump($existsUserIDs);
        echo  "<br>";

        while(!$is_unique) {
            $userIndex = array_rand($userIDs);
            $uID = $userIDs[$userIndex];
            var_dump($uID);
            echo "<br>";
            if(!in_array($uID, $existsUserIDs)) {
                echo 'unique' . '<br>';
                $is_unique = true;
            }
        }

        echo $uID;*/
        $availCats =  Category::where('is_available', '=', true)->get();
        $availSlids =  Slider::where('is_available', '=', true)->get();
        $topSales = OrderDetail::select(DB::raw('sum(amount) as sAmount, product_id'))
            ->where('product_id', '!=', 'null')
            ->where('is_available', '=', true)
            ->groupBy('product_id')
            ->orderBy('sAmount', 'DESC')
            ->take(10)
            ->get();

        //dd($topSales);

        $data = [
            'availCats' => $availCats,
            'topSales' => $topSales,
            'availSlids'=>$availSlids
        ];
        return view('user/index', ['data' => $data]);
    }



    // public function indexslider(){

    //     $availSlids = Slider::where('is_available', '=', true)->get();


    //         $data = [
    //             'availSlids ' => $availSlids ,
                
    //         ];
    //         return view('index', ['data' => $data]);
    // }

}

<?php

namespace App\Http\Controllers;

use App\GuestMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('user/contact-us');
    }

    public function sendGuestMessage(Request $request)
    {
        $data = $request->all();


        // Validation
        // https://laravel.com/docs/5.7/validation#available-validation-rules
        $rules = [
            'name' => 'required|min:3|max:75',
            'email' => 'required|max:100|email',
            'feedback' => 'required|max:1000',
            'phone' => 'required|max:50',
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            // PROBLEM!!!
            // dd($validator->errors());
            return redirect('user/contact-us')
                ->withErrors($validator->errors());
        }

        // Store if Ok!
        $newGuestMsg = new GuestMessage();
        $newGuestMsg->name = $data['name'];
        $newGuestMsg->email = $data['email'];
        $newGuestMsg->message = $data['feedback'];
        $newGuestMsg->phone = $data['phone'];
        $newGuestMsg->is_available = true;
        if (Auth::check())
            $newGuestMsg->user_id = Auth::id();

        $newGuestMsg->save();

        return redirect('user/contact-us');

    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{

    public function index()
    {
        return view('user.auth.register');
    }

    public function NewUser (Request $request)
    {
        $data = $request->all();

        // Validation
        // https://laravel.com/docs/5.7/validation#available-validation-rules
        $rules = [
            'firstname' => 'required|min:3|max:75',
            'lastname' => 'required|min:3|max:75',
            'gender' => 'required|max:20',
            'username' => 'required|min:3|max:75',
            'password' => 'required|min:3|max:75',
            'birthdate'=>'required',
            'email' => 'required|max:100|email',
            'phone' => 'required|max:50',
            'address' => 'required|max:1000',
            'city' => 'required|max:1000',
        ];


        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            // PROBLEM!!!

            return redirect('/register')
                ->withErrors($validator->errors());
        }

        // Store if Ok!
        $newUser = new User();
        $newUser->first_name = $data['firstname'];
        $newUser->last_name = $data['lastname'];
        $newUser->gender_id = $data['gender'];
        $newUser->username = $data['username'];
        $newUser->password = bcrypt($data['password']);
        $newUser->birth_date = $data['birthdate'];
        $newUser->email = $data['email'];
        $newUser->address = $data['address'];
        $newUser->phone = $data['phone'];
        $newUser->city_id = $data['city'];
        $newUser->is_available = true;
        $newUser->is_tech = false;
        $newUser->is_admin = false;


        $newUser->save();

        return redirect('user/index');

    }
}

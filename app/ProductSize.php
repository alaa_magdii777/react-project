<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    public  function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public  function size() {
        return $this->belongsTo(Size::class, 'size_id', 'id');
    }
}

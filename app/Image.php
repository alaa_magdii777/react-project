<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public  function users() {
        return $this->hasMany(User::class, 'user_id', 'id');
    }
    public  function brands() {
        return $this->hasMany(Brand::class, 'brand_id', 'id');
    }

    public  function categories() {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }

    public  function products() {
        return $this->hasMany(Product::class, 'product_id', 'id');
    }


    

}

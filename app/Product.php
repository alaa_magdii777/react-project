<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function suppliers() {
        return $this->hasMany(Supplier::class, 'supplier_id', id);
    }

    public function brand() {
        return $this->belongsTo(Brand::class, 'brand_id', id);
    }

    public function category() {
        return $this->belongsTo(category::class, 'category_id', id);
    }

    public function unconfirmedOrder() {
        return $this->belongsTo(Unconfirmed_order::class, 'product_id', 'id');
    }

    public  function image() {
        return $this->belongsTo(Image::class, 'product_id', 'id');
    }

    public  function sizes() {
        return $this->hasMany(ProductSize::class, 'product_id', 'id');
    }

    public function unconfirmedOrders() {
        return $this->belongsTo(UnconfirmedOrder::class, 'product_id', id);
    }
}

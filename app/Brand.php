<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function products() {
        return $this->hasMany(Product::class, 'brand_id', id);
    }

    public  function image() {
        return $this->belongsTo(Image::class, 'brand_id', 'id');
    }
}

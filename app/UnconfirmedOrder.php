<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnconfirmedOrder extends Model
{
    public function products() {
        return $this->hasMany(Product::class, 'product_id', 'id');
    }

    public function users() {
        $this->hasMany(User::class, 'user_id', 'id');
    }
}

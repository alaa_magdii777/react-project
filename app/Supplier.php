<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function city() {
        return $this->belongsTo(City::class, 'city_id', id);
    }

    public function product() {
        return $this->belongsTo(Product::class, 'supplier_id', id);
    }
}

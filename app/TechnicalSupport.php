<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicalSupport extends Model
{
    public function users() {
        $this->hasMany(User::class, 'user_id', 'id');
    }

    public  function chats() {
        return $this->hasMany(Chat::class, 'technical_support_id', 'id');
    }
}

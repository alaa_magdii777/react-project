<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public  function support() {
        return $this->belongsTo(TechnicalSupport::class, 'technical_support_id', 'id');
    }

    public function users() {
        $this->hasMany(User::class, 'user_id', 'id');
        $this->hasMany(User::class, 'tech_id', 'id');
    }
}

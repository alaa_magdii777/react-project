(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  /*$("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });*/

    $("#accordionSidebar, #sidebarToggleTop").on({
        'mouseleave' : nav,
        'mouseenter': nav
    });
    //$('#sidebarToggleTop').on('click', nav);
    function nav() {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        };
    }

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

    /************************** dashboard **************************/
    /*Trigger image upload file input on custom button click*/
    let imgUploadFile = $('#product-image-input');
    let imgCustomBtn = $('#custom-input-btn');
    let uploadImg = $('#upload-img');

    imgCustomBtn.on('click', ()=> {imgUploadFile.click()});

    imgUploadFile.on('change', function () {
        let file  = this.files[0];
        if(file.type.match('image')) {
            imgCustomBtn.css({
                'backgroundImage': 'url(' + URL.createObjectURL(this.files[0]) + ')',
                'border': 'none'
            });
            uploadImg.hide();
        } else {
            imgCustomBtn.css({
                'backgroundImage': 'url(' + URL.createObjectURL(this.files[0]) + ')',
                'border': '2px dashed #4E73DF'
            });
            uploadImg.show();
        }
    });

    /*Reset product's form data*/
    let resetBtn = $('#reset-data');
    let inputs = $('#product-form .form-control');
    let inputVal = [];

    for(let input of inputs) {
        inputVal.push({'ele': input, 'val': input.value});
    }
    resetBtn.on('click', () => {
        for (let input of inputVal) {
            input['ele'].value = input['val'];
        }
    });
})(jQuery); // End of use strict

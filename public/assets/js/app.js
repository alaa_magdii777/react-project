$(function () {
    /******************************* Product Details Page *******************************/
    /* Expand Details */
    let expandIcons = $('.expand-details');

    expandIcons.on('click', expandProductDetails);

    function expandProductDetails() {
        let id = $(this).attr('id').split('-')[1];
        let target = $('#' + id);

        //open and close details
        target.fadeToggle(200);

        //change arrow direction
        $(this).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
    }

    /******************************* Category Name Page Page *******************************/
    /* Change Price Display on Range Change */
    let rangeInputs = $('input[type="range"]');

    rangeInputs.on('input', changeDisplayValue);

    function changeDisplayValue() {
        let id = $(this).attr('id') + '-value';
        let value = $(this).val();
        let target = $('#' + id);

        //change display text
        target.text(value);

        //change input value attribute
        $(this).attr('value', value);
    }

    /******************************* faq Page *******************************/
    let categoryLinks = $('.faq-categories .cat');

    categoryLinks.on('click', function (e) {
        let catName = $(this).attr('id').split('-')[1];
        let activePage = $('.questions.active');
        let targetPage = $('#' + catName + '-questions');

        $('.faq-categories .cat.active').removeClass('active');
        $(this).addClass('active');

        activePage.fadeOut(function () {
            $(this).removeClass('active');
            targetPage.fadeIn(function () {
                $(this).addClass('active');
            })
        });
        e.preventDefault();
    });
    /* popup chat */
    //toggle chat
    let chatToggler = $('#chat-toggler');
    let chat = $('#chat');
    let closeChat = $('[id*="close-chat"]');

    chatToggler.on('click', function (e) {
        e.preventDefault();
        chat.fadeIn();
        $(this).fadeOut(200);
    });

    closeChat.on('click', () => {
        chat.fadeOut(200);
        chatToggler.fadeIn();
    });

    //close dialogue
    let closeDialogue = $('#close-dialogue');

    closeDialogue.on('click', function () {
        $('#no-messages-yet').fadeOut();
    });
    /******************************* Cart Page *******************************/

    /* shopping cart items */

    let addToCartButton = $('button.addToCart');
    let cartItemsNum = $('#cart-products-number');

    addToCartButton.click(function () {
        let id = $(this).attr('data-id');
        let price = $(this).attr('data-price');
        let discount = $(this).attr('data-discount');
        let max = $(this).attr('data-max');
        let name = $(this).attr('data-name');
        let imgURL = $(this).attr('data-imgURL');

        let cart = localStorage.getItem('cart');
        if (cart == null) {
            cart = [];
        } else {
            cart = JSON.parse(cart);
        }

        let index = cart.findIndex(function (item) {
            return item.id == id;
        });

        if (index == -1) {
            cart.push({
                id: id,
                price: price,
                discount: discount,
                name: name,
                max: max,
                imgURL: imgURL,
                amount: 1,
            });
        } else {
            cart[index].amount++;
        }

        localStorage.setItem('cart', JSON.stringify(cart));
        updateCartNumber();
    });

    function updateCartNumber() {
        let counter = 0;
        if(localStorage.getItem('cart') != null) {
            counter = JSON.parse(localStorage.getItem('cart')).length;
            console.log(JSON.parse(localStorage.getItem('cart')));
        };
        cartItemsNum.text(counter);
    }
    
    updateCartNumber();

    function generateCartItem(id, name, price, discount, max, quantity, imgURL) {
        let totalPrice = quantity * price * (1.0 - discount / 100.0);
        totalPrice = Math.ceil(totalPrice * 100) / 100;

        let html = '<tr>' +
            '<td class="product-info d-flex">' +
            '<input class="check mr-3" type="checkbox" name="cart-item[]" id="item-$productId">' +
            '<div class="thumbnail mr-3">' +
            '<img class="w-100" src="' + imgURL + '" alt="">' +
            '</div>' +
            '<ul class="info list-unstyled m-0 p-0">' +
            '<li class="name"><a href="product-' + id + '">' + name + '</a></li>' +
            '<li class="size">36</li>' +
            '<li class="color">Black</li></ul></td>' +
            '<td class="product-quantity"><div class="wrapper d-flex">' +
            '<span class="control decrease mr-3" id="decrease-item-' + id + '">-</span>' +
            '<input class="quantity mr-3" type="text" value="' + quantity + '" ' +
            'name="quantity" id="item-quantity-' + id + '"' +
            ' data-max="' + max + '"> <span class="control increase" id="increase-item-' + id + '">+</span>' +
            '</div></td><td class="product-price text-center"><span class="price" ' +
            'id="item-price-' + id + '" data-price="' + price + '"' +
            ' data-discount="' + discount + '">' + price + '</span><span class="currency">$</span>' +
            '</td><td class="product-edit text-center">' +
            '<a href="delete-cart-item-' + id + '" class="delete" id="delete-item-' + id + '">' +
            '<i class="fas fa-trash-alt"></i></a>' +
            '</td><td class="product-subtotal text-center">' +
            '<span class="subtotal" id="item-subtotal-' + id + '">' + totalPrice + '</span>' +
            '<span class="currency">$</span>' +
            '</td></tr>';
        return html;
    }

    let cart = localStorage.getItem('cart');
    if (cart == null) {
        cart = [];
    } else {
        cart = JSON.parse(cart);
    }


    let allItemsHTML = "";
    for (let i = 0; i < cart.length; i++) {
        let itemHTML = generateCartItem(cart[i].id, cart[i].name, cart[i].price, cart[i].discount,
            cart[i].max, cart[i].amount, cart[i].imgURL);
        allItemsHTML += itemHTML;
    }
    $('#cartBody').append(allItemsHTML);

    /* Check if cart is empty */
    function toggleEmptyCart() {
        let cart = localStorage.getItem('cart');
        let itemsNum = 0;
        let emptyCart = $('#empty-cart');
        let itemsWrapper = $('#items-wrapper');

        if(cart != null) {
            itemsNum = JSON.parse(cart).length;
        }

        if(cart == null || itemsNum <= 0) {
            itemsWrapper.hide();
            emptyCart.show();
            return;
        }

        itemsWrapper.show();
        emptyCart.hide();
    }

    toggleEmptyCart();
    // select all products
    let selectAllcheck = $('#select-all');

    // check and uncheck all boxes
    selectAllcheck.on('click', function () {
        if (this.checked == true) {
            $('.check').each(function () {
                this.checked = true;
            });
        } else {
            $('.check').each(function () {
                this.checked = false;
            });
        }
    });

    //remove check on select all on single box uncheck
    $('.check').on('click', function () {
        if ($('.check:checked').length == $('.check').length) {
            selectAllcheck.prop('checked', true);
        } else {
            selectAllcheck.prop('checked', false);
        }
    });

    //increase and decrease cartt items
    let increseBtn = $('[id^="increase-item"]');
    let decreaseBtn = $('[id^="decrease-item"]');

    increseBtn.on('click', function () {
        let id = $(this).attr('id').split('-')[2];
        updateCart(id, 'increase');
    });

    decreaseBtn.on('click', function () {
        let id = $(this).attr('id').split('-')[2];
        updateCart(id, 'decrease');
    });

    function updateCart(id, action) {
        let quantityInput = $("#item-quantity-" + id);
        let priceDisplay = $("#item-price-" + id);
        let subtotalDisplay = $("#item-subtotal-" + id);
        let price = parseInt(priceDisplay.data('price'));
        let discount = parseInt(priceDisplay.data('discount'));
        let maxQuantity = parseInt(quantityInput.data('max'));
        let oldValue = parseInt(quantityInput.val());
        let newValue;

        if (action === 'increase') {
            newValue = oldValue + 1;
            if (newValue <= maxQuantity) {

                quantityInput.val(newValue);
            }
        } else {
            newValue = oldValue - 1;
            if (newValue >= 0) {
                quantityInput.val(newValue);
            }
        }

        let newPrice = newValue * price;
        let newsubtotal = Math.round(newPrice - (discount * newPrice / 100));

        priceDisplay.text(newPrice);
        subtotalDisplay.text(newsubtotal);

    }

    /*Rateyo Plugin*/
    let ratingStars = $("[id^='product-rating']");
    ratingStars.each(function () {
        let rating = parseInt($(this).data('rating') || 0) * 10;
        $(this).rateYo({
            starWidth: "14px",
            rating: rating + '%',
            readOnly: true
        });
    });

    /*Mixit up*/
    let mixContainer = $('#mix-container');

    if(mixContainer.length) {
        let mixer = mixitup(mixContainer, {
            controls: {
                toggleLogic: 'or'
            }
        });
    }
    /*
    let checkboxes = $('[data-filter^="."]');
    let checkedCats = [];
    for (let check of checkboxes) {
        if (check.checked == true) {
            console.log(check);
        }
    }
     checkboxes.on('click', function () {
         let filters = '';
         let catName = this.dataset.filter.replace('.', '');
         if(this.checked == true) {
             if(checkedCats.indexOf(catName) < 0) {
                 checkedCats.push(catName);
                 checkedCats.forEach(function (item, i) {
                     filters += '.' + item + (i < (checkedCats.length -1) ? ',' : '');
                 });
             }
         } else  {
             let catIndex = checkedCats.indexOf(catName);
             checkedCats.splice(catIndex, 1);
             if(checkedCats.length <= 0) {
                 filters = 'all';
             } else {
                 checkedCats.forEach(function (item, i) {
                     filters += '.' + item + (i < (checkedCats.length -1) ? ',' : '');
                 });
             }
         }
         console.log(filters);
             triggerMixItUp(filters);
     });*/

    /*function triggerMixItUp(filters) {
        let mixer = mixitup(mixContainer, {
            controls: {
                toggleLogic: 'or'
            },
            load: {
                filter: filters
            }
        });
    }*/
});

/*Google maps API*/
let map = null;

function myMap() {
    let long = $('#map').data('long');
    let lat = $('#map').data('lat');

    var mapProp = {
        center: new google.maps.LatLng(lat, long), /*those two value numbers represent your latitude and      l
            ongitude you can spot your location from searching google or google maps*/
        zoom: 10
    };
        map = new google.maps.Map(document.getElementById('map'), mapProp);
}

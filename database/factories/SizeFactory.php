<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Size;
use Faker\Generator as Faker;

$factory->define(Size::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->sentence(2),
        'width'=>rand(1 , 500),
        'height'=>rand(1 , 500),
        'depth'=>rand(1 , 500),
        'is_available'=>$faker->boolean()
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TechnicalSupport;
use Faker\Generator as Faker;

$factory->define(TechnicalSupport::class, function (Faker $faker) {
    $userInChatsIDs = TechnicalSupport::pluck('user_id')->toArray();
    $userIDs = \App\User::whereNotIn('id', $userInChatsIDs)->pluck('id')->toArray();
    $techIDs = \App\User::pluck('id')->toArray();
    if (count($userIDs) <= 0) {
        $uID = null;
    } else {
        $userIndex = array_rand($userIDs);
        $uID = $userIDs[$userIndex];
    }

    $rIndex = array_rand($techIDs);
    $tID = $techIDs[$rIndex];
    return [
        'user_id' => $uID,
        'tech_id' => $tID,
        'is_available' => $faker->boolean(60),
    ];
});

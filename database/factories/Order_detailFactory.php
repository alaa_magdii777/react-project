<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order_detail;
use Faker\Generator as Faker;

$factory->define(Order_detail::class, function (Faker $faker) {
   
    $orderdetailsIDs = \App\Order_detail::pluck('id')->toArray();
    $orderdetailsIndex = array_rand( $orderdetailsIDs);

  
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);

    
    return [
        
       
        'order_id'=>$orderdetailsIDs[$orderdetailsIndex],
        'product_id'=>$productIDs[$productIndex],
       
       
        'amount'=>rand(1, 10),
        'price' =>round(rand(0, 30000),2),
    
        'discount'=>round(rand(5, 80),2),
         'is_available'=>$faker->boolean()
        
         
        
        
    ];
});

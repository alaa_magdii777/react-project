<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductColor;
use Faker\Generator as Faker;

$factory->define(ProductColor::class, function (Faker $faker) {
   
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);

    
    // $colorIDs = \App\color::pluck('id')->toArray();
    // $colorIndex = array_rand( $colorIDs);


    
    
    return [
        
        'product_id'=>$productIDs[$productIndex],
        // 'color_id'=> $colorIDs[ $colorIndex],
      
       
        'is_available'=>$faker->boolean()
    ];
});

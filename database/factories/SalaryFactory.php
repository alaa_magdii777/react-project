<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Salary;
use Faker\Generator as Faker;

$factory->define(Salary::class, function (Faker $faker) {
    return [
        'salary' => round(rand(1 , 6000),2),
        'is_available' => $faker ->boolean()
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserRole;
use Faker\Generator as Faker;

$factory->define(UserRole::class, function (Faker $faker) {
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);

    $roleIDs = \App\Role::pluck('id')->toArray();
    $roleIndex = array_rand($roleIDs);
    return [
        //
        'user_id'=>$userIDs[$userIndex],
        'role_id'=>$roleIDs[$roleIndex],
        'is_available'=>$faker->boolean()
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WarehouseProduct;
use Faker\Generator as Faker;

$factory->define(\App\WarehouseProduct::class, function (Faker $faker) {

    $warehouseIDs = \App\Warehouse::pluck('id')->toArray();
    $warehouseIndex = array_rand($warehouseIDs);

    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);

    return [
        //
        'warehouse_id'=>$warehouseIDs[$warehouseIndex],
        'product_id'=>$productIDs[$productIndex],
        'quantity'=>rand(0, 30),
        'is_available'=>$faker->boolean()

    ];
});

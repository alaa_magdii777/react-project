<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(\App\Order::class, function (Faker $faker) {

        $userIDs = \App\User::pluck('id')->toArray();
        $userIndex = array_rand($userIDs);

    return [
        'user_id' => $userIDs[$userIndex],
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'total_cost' => round(rand(1, 30000),2),
        'total_discount' => round(rand(5, 80),2),
        'delivery_date'=>$faker->dateTime(),
        'is_delivered' => $faker->boolean(),
        'is_available' => $faker->boolean()



    ];
});

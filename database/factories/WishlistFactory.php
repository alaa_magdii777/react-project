<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Wishlist;
use Faker\Generator as Faker;

$factory->define(\App\Wishlist::class, function (Faker $faker) {
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);

    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);

    return [
        //
        'user_id'=>$userIDs[$userIndex],
        'product_id'=>$productIDs[$productIndex],
        'is_available'=>$faker->boolean()


    ];
});

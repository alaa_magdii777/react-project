<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name'=>$faker->unique()->words(1)[0],
        'description' => $faker->sentence(10),
        'is_available' => $faker->boolean()
    ];
});

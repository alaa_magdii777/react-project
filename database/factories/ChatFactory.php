<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Chat;
use Faker\Generator as Faker;

$factory->define(Chat::class, function (Faker $faker) {
    $techSupportIDs = \App\TechnicalSupport::pluck('id')->toArray();
    $techSupportIndex = array_rand($techSupportIDs);
    //$userIDs = \App\User::pluck('id')->toArray();
    //$userIndex = array_rand($userIDs);
    //$techIDs = \App\User::where('is_tech', '=', true)
    //    ->where('id','!=', $userIDs[$userIndex])
    //    ->get()
    //    ->pluck('id')->toArray();
    //$techIndex = array_rand($techIDs);
    //$senders = [
    //    ['type' => 'user_id', 'id' => $userIDs[$userIndex]],
    //    ['type' => 'tech_id', 'id' => $techIDs[$techIndex]],
    //];
    //$senderIndex = array_rand($senders);
    return [
        'technical_support_id' => $techSupportIDs[$techSupportIndex],
        //$senders[$senderIndex]['type'] => $senders[$senderIndex]['id'],
        'is_seen' => $faker->boolean(80),
        'is_forward' => $faker->boolean(50),
        'message' =>$faker->sentence(4)
    ];
});

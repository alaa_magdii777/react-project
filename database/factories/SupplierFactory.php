<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    $cityIDs = \App\City::pluck('id')->toArray();
    $cityIndex = array_rand($cityIDs);

    return [
        'city_id' => $cityIDs[$cityIndex],
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'is_available' => $faker->boolean()
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GuestMessage;
use Faker\Generator as Faker;

$factory->define(GuestMessage::class, function (Faker $faker) {
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);
    return [
        'user_id' => $userIDs[$userIndex],
        'name'=>$faker->words(1)[0],
        'email'=>$faker->unique()->safeEmail,
        'message'=>$faker->text(75),
        'phone'=>$faker->phoneNumber,
        'is_available' => $faker->boolean()
    ];
});

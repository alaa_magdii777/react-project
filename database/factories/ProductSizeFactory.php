<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProductSize;
use Faker\Generator as Faker;

$factory->define(ProductSize::class, function (Faker $faker) {
   
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);

    
    $sizeIDs = \App\size::pluck('id')->toArray();
    $sizeIndex = array_rand( $sizeIDs );


    
       
   
    return [

        'product_id'=>$productIDs[$productIndex],
    
        'size_id'=> $sizeIDs[$sizeIndex],
      
        'is_available'=>$faker->boolean()
    ];
});

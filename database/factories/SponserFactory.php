<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sponser;
use Faker\Generator as Faker;

$factory->define(Sponser::class, function (Faker $faker) {
    return [
        'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
        'name' => $faker->name,
        'extension' => '',
        'is_available' => $faker ->boolean()
    ];
});

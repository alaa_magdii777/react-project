<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UnconfirmedOrder;
use Faker\Generator as Faker;

$factory->define(UnconfirmedOrder::class, function (Faker $faker) {
    // products
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);
    // users
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);

    return [
        'product_id' => $productIDs[$productIndex],
        'user_id' => $userIDs[$userIndex],
        'quantity' => 1,
        'is_available' => $faker->boolean()
    ];
});

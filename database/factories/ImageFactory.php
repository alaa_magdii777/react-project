<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Image;
use Faker\Generator as Faker;
$factory->define(Image::class, function (Faker $faker) {
    //IDs
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);
    $categoryIDs = \App\Category::pluck('id')->toArray();
    $categoryIndex = array_rand($categoryIDs);
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);
    $brandIDs = \App\Brand::pluck('id')->toArray();
    $brandIndex = array_rand($brandIDs);
    //image data
    /*return [
        'user_id' => $userIDs[$userIndex],
        'category_id' => $categoryIDs[$categoryIndex],
        'product_id' => $productIDs[$productIndex],
        'brand_id' => $brandIDs[$brandIndex],
        'path' => $path,
        'name' => $img['name'],
        'extension' => $img['extension'],
        'size_kb' => $img['size_kb']
    ];*/
});

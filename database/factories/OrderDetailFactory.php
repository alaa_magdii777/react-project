<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderDetail;
use Faker\Generator as Faker;

$factory->define(OrderDetail::class, function (Faker $faker) {

    $orderDetailsIDs = \App\Order::pluck('id')->toArray();
    $orderDetailsIndex = array_rand( $orderDetailsIDs);
    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);
    $sizeNams = \App\Size::pluck('name')->toArray();
    $sizeIndex = array_rand( $sizeNams );
    $colors = \App\Color::pluck('name')->toArray();
    $colorIndex = array_rand( $colors );


    return [
        'order_id'=>$orderDetailsIDs[$orderDetailsIndex],
        'product_id'=>$productIDs[$productIndex],
        'size'=> $sizeNams[$sizeIndex],
        'color' => $colors[$colorIndex],
        'amount'=>rand(1, 10),
        'price' =>round(rand(0, 30000),2),
        'discount'=>round(rand(5, 80),2),
        'is_available'=>$faker->boolean()
    ];
});

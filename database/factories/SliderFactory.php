<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Slider;
use Faker\Generator as Faker;

$factory->define(Slider::class, function (Faker $faker) {
    return [

        'name'=>$faker->unique()->words(1)[0],
        'description' => $faker->sentence(10),
        'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
        'extension' => '',
        
    ];
});

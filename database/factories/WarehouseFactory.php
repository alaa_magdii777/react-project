<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Warehouse;
use Faker\Generator as Faker;

$factory->define(\App\Warehouse::class, function (Faker $faker) {

    $cityIDs = \App\City::pluck('id')->toArray();
    $cityIndex = array_rand($cityIDs);
    return [
        'city_id'=>$cityIDs[$cityIndex],
        'name' =>$faker->text(75),
        'address' =>$faker->address(125),
        'phone' =>$faker->phoneNumber,
        'lat' =>$faker->latitude,
        'long'=>$faker->longitude,
        'is_available'=>$faker->boolean()
    ];
});

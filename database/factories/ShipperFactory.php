<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Shipper;
use Faker\Generator as Faker;

$factory->define(Shipper::class, function (Faker $faker) {
    return [
        'first_name' => $faker -> firstName,
        'last_name' => $faker ->lastName,
        'address' => $faker ->address,
        'phone' => $faker ->phoneNumber,
        'lat' => $faker ->latitude,
        'long' => $faker ->longitude,
        'is_available' => $faker ->boolean()
    ];
});

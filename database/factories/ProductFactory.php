<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $supplierIDs = \App\Supplier::pluck('id')->toArray();
    $supplierIndex = array_rand($supplierIDs);
    $brandIDs = \App\Brand::pluck('id')->toArray();
    $brandIndex = array_rand($brandIDs);
    $categoryIDs = \App\Category::pluck('id')->toArray();
    $categoryIndex = array_rand($categoryIDs);
    $keywords = ['elegant', 'fashion', 'men', 'women', 'new_arrivals'];
    $tempArr = [];
    $buyingPrice = round(rand(1, 1000),2);
    while(count($tempArr) < 3) {
        $keywordsIndex = array_rand($keywords);
        $kw = $keywords[$keywordsIndex];
        if(!in_array($kw, $tempArr)) {
            array_push($tempArr, $kw);
        }
    }
    $key = implode(',', $tempArr);

    return [
        'supplier_id' => $supplierIDs[$supplierIndex],
        'brand_id' => $brandIDs[$brandIndex],
        'category_id' => $categoryIDs[$categoryIndex],
        'name' => $faker->unique()->sentence(2),
        'buying_price' => round(rand(1, 1000),2),
        'selling_price' => $buyingPrice + round(rand(1, 50),2),
        'discount' => round(rand(0, 80),2),
        'description' => $faker->sentence(10),
        'keywords' => $key,
        'is_new' => $faker ->boolean(),
        //'is_soldOut' => $faker ->boolean(),
        'is_available' => $faker->boolean()

    ];
});

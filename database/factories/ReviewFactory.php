<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Review;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
   
    $userIDs = \App\User::pluck('id')->toArray();
    $userIndex = array_rand($userIDs);

    $productIDs = \App\Product::pluck('id')->toArray();
    $productIndex = array_rand($productIDs);


       
   
    return [

        'product_id'=>$productIDs[$productIndex],
    
        'user_id'=>$userIDs[$userIndex],
        'rating'=>rand(1, 5),
        'review'=>$faker->text(100),
        'is_available'=>$faker->boolean()
    ];
});

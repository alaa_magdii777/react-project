<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderAlert;
use Faker\Generator as Faker;

$factory->define(OrderAlert::class, function (Faker $faker) {

    $orderIDs = \App\Order::pluck('id')->toArray();
    $orderIndex = array_rand($orderIDs);
    return [
        'order_id' => $orderIDs[$orderIndex],
        'content' => $faker->sentence(6),
        'is_seen' => $faker->boolean(),
        'is_available' => $faker->boolean()


    ];
});

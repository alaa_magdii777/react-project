<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $genderIDs = \App\Gender::pluck('id')->toArray();
    $genderIndex = array_rand($genderIDs);
    $cityIDs = \App\City::pluck('id')->toArray();
    $cityIndex = array_rand($cityIDs);
    
    return [
        'city_id' => $cityIDs[$cityIndex],
        'gender_id' => $genderIDs[$genderIndex],
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'),
        'birth_date'=>$faker->date(),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'is_admin' => $faker->boolean(),
        'is_tech' => $faker->boolean(),
        'is_available' => $faker->boolean()
    ];
});

<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('countries')->delete();
        
        $countryArr = [
            ['name' => 'Egypt', 'region' => 'Middle East'],
            ['name' => 'KSA', 'region' => 'Middle East'],
            ['name' => 'UAE', 'region' => 'Middle East'],
            ['name' => 'Qatar', 'region' => 'Middle East'],
            ['name' => 'Palestine', 'region' => 'Middle East'],
            ['name' => 'Syria', 'region' => 'Middle East'],
            ['name' => 'Algeria', 'region' => 'Northwestern Africa'],
            ['name' => 'Morocco', 'region' => 'Northwestern Africa'],
            ['name' => 'Tunisia', 'region' => 'Northwestern Africa'],
            ['name' => 'UK', 'region' => 'Europe'],
            ['name' => 'France', 'region' => 'Europe'],
            ['name' => 'Germany', 'region' => 'Europe'],
            ['name' => 'Italy', 'region' => 'Europe'],
            ['name' => 'USA', 'region' => 'North America'],
            ['name' => 'Canada', 'region' => 'North America'],
            ['name' => 'Argentina', 'region' => 'South America'],
            ['name' => 'Brazil', 'region' => 'South America']
        ];

        foreach($countryArr as $country) {
            DB::table('countries')->insert([
                'name' => $country['name'],
                'region' => $country['region']
            ]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;

class OrderAlertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('order_alerts')->delete();
        factory(\App\OrderAlert::class , 15)->create();
    }
}

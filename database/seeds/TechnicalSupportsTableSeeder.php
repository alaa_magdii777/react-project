<?php

use Illuminate\Database\Seeder;

class TechnicalSupportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('technical_supports')->delete();
        factory(\App\TechnicalSupport::class, 20)->create();
    }
}

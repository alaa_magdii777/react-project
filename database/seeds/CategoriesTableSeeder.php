<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        //$categories = ['women', 'men', 'kids', 'babies', 'elecrtonics', 'kitchen', 'books', 'furniture', 'personal care', 'music'];
        
        /* foreach($categories as $category) {
            DB::table('categories')->insert([
                'name' => $category
            ]);
        } */
        factory(App\Category::class, 10)->create();
    }
}

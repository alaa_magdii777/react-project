<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ChatsTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('chats')->delete();
        factory(\App\Chat::class, 20)->create();
//        //$userIDs = \App\User::pluck('id')->toArray();
//        $techtIDs = \App\User::where('is_tech', '=', 1)->pluck('id')->toArray();
//        $techIdex = array_rand($techtIDs);
//        $supports = \App\TechnicalSupport::all()->toArray();
//        $faker = Faker::create();
//        foreach ($supports as $support) {
//            for($i = 1; $i <= random_int(1, 10); $i++) {
//                $senders = [
//                    ['name' => 'user_id', 'id' => $support['user_id']],
//                    ['name' => 'tech_id', 'id' => $techtIDs[$techIdex]]
//                ];
//                $senderIdex = array_rand($senders);
//                \Illuminate\Support\Facades\DB::table('chats')->insert([
//                    'technical_support_id' => $support['id'],
//                    $senders[$senderIdex]['name'] => $senders[$senderIdex]['id'],
//                    'message' => $faker->text(45),
//                    'is_available' => $faker->boolean(90)
//                ]);
//            }
//        }
    }
}

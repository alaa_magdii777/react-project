<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class SponsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('sponsers')->delete();

        factory(App\Sponser::class, 20)->create();

    }
}

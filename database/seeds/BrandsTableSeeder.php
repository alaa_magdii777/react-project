<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->delete();
        
        $brands = ['Apple', 'Adidas', 'Gucci', 'Max', 'Nike', 'El-Araby', 'gibson', 'Furtex'];
        
        foreach($brands as $brand) {
            DB::table('brands')->insert([
                'name' => $brand,
                'website' => 'https://' . $brand . '.com' 
            ]);
        }
    }
}

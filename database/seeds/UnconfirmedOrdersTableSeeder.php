<?php

use Illuminate\Database\Seeder;

class UnconfirmedOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unconfirmed_orders')->delete();

        factory(App\UnconfirmedOrder::class, 12)->create();
    }
}

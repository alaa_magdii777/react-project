<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(GendersTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(TechnicalSupportsTableSeeder::class);
        $this->call(ChatsTableSeeders::class);
        $this->call(ColorsTableSeeder::class);
        // coupons
        // dictionary
        $this->call(GuestMessagesTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrderAlertsTableSeeder::class);
        $this->call(OrderDetailsTableSeeder::class);
        $this->call(ProductColorsTableSeeder::class);
        $this->call(ProductSizesTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        $this -> call(RolesTableSeeder::class);
        $this ->call(SalariesTableSeeder::class);
        $this ->call(ShippersTableSeeder::class);
        $this ->call(SiteConfigsTableSeeder::class);
        $this->call(UnconfirmedOrdersTableSeeder::class);
        // userscoupons
        $this->call(UserRolesSeeder::class);
        $this->call(WarehousesTableSeeder::class);
        $this->call(WarehouseProductsTableSeeder::class);
        $this->call(WishlistsTableSeeder::class);

        $this->call(SlidersTableSeeder::class);

        $this->call(SponsersTableSeeder::class);

    }
}


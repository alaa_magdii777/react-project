<?php

use Illuminate\Database\Seeder;

class ProductSizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\ProductSize::class,20)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('genders')->delete();
        $genders = [ 'male', 'female' ];
        foreach($genders as $gender) {
            DB::table('genders')->insert([
                'name' => $gender,
            ]);
        }
    }
}

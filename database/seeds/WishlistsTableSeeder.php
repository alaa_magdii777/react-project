<?php


use App\Wishlist;
use Illuminate\Database\Seeder;

class WishlistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \Illuminate\Support\Facades\DB::table('wishlists')->delete();
        factory(App\Wishlist::class, 30)->create();
    }
}

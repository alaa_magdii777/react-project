<?php

use Illuminate\Database\Seeder;

class GuestMessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\GuestMessage::class,20)->create();
    }
}

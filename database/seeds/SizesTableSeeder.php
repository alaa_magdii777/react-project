<?php

use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\App\Size::class,20)->create();
        $sizes = [
            ['name' => 'xsmall', 'width' => 30, 'height' => 40, 'depth' => 10],
            ['name' => 'small', 'width' => 40, 'height' => 50, 'depth' => 15],
            ['name' => 'medium', 'width' => 50, 'height' => 60, 'depth' => 20],
            ['name' => 'large', 'width' => 60, 'height' => 70, 'depth' => 25],
            ['name' => 'xlarge', 'width' => 70, 'height' => 80, 'depth' => 30]
        ];

        foreach ($sizes as $size) {
            \Illuminate\Support\Facades\DB::table('sizes')->insert([
                'name' => $size['name'],
                'width' => $size['width'],
                'height' => $size['height'],
                'depth' => $size['depth']
            ]);
        }
    }
}

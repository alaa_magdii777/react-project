<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \Illuminate\Support\Facades\DB::table('colors')->delete();
        factory(\App\Color::class, 10)->create();
      /*$colors=['Red','Blue','Brown','Black','Yellow','Grey','White'];
      foreach ($colors as $color){
          \Illuminate\Support\Facades\DB::table('colors')->insert([
              'name'=>$color
          ]);
       }*/

    }

}

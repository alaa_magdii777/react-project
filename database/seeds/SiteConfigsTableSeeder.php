<?php

use Illuminate\Database\Seeder;

class SiteConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('site_configs')->insert([
                'name' => 'market',
                'description'=>'Hear what’s trending with best price in the Market community',
                'phone_code' => '+20',
                'phone_one' => '0100-123-456-7',
                'phone_two' => '0100-987-654-3',
                'phone_three' => '0100-999-555-4',
                'address' => '21,XYZ street, Mansoura, Egypt',
                'fax' => '0000-666-555-222',
                'mission' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam vel quam elementum pulvinar etiam non quam. Sapien pellentesque habitant morbi tristique senectus et netus.Eu turpis egestas pretium aenean. Adipiscing bibendum est ultricies integer quis auctor elit.',
                'vision' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aenean pharetra magna ac placerat vestibulum lectus mauris. Metus aliquam eleifend mi in nulla posuere. Ac tincidunt vitae semper quis lectus. Diam vel quam elementum pulvinar etiam non quam. Sapien pellentesque habitant morbi tristique senectus et netus. Et malesuada fames ac turpis egestas integer eget aliquet.',
                'long' => 29.9187,
                'lat' => 31.2001,
                'logo_path' => 'assets/images/',
                'logo_name' => 'logo.svg',
                'logo_extension' => 'svg',
                'logo_size_kb' => 1.39,
                'privacy_updated_at' => '2019-09-05',
                'is_available' => 1,
            ]);
    }
}

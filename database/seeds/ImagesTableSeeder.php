<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->delete();
        //factory(\App\Image::class, 100)->create();
        $userIDs = \App\User::pluck('id')->toArray();
        $brandIDs = \App\Brand::pluck('id')->toArray();
        $productIDs = \App\Product::pluck('id')->toArray();
        $categoryIDs = \App\Category::pluck('id')->toArray();
       
        $faker = Faker::create();

        foreach ($userIDs as $userID) {
            \Illuminate\Support\Facades\DB::table('images')->insert([
                'user_id' => $userID,
                'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
                'name' => '',
                'extension' => ''
            ]);
        }

        foreach ($brandIDs as $brandID) {
            \Illuminate\Support\Facades\DB::table('images')->insert([
                'brand_id' => $brandID,
                'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
                'name' => '',
                'extension' => ''
            ]);
        }

        foreach ($productIDs as $productID) {
            \Illuminate\Support\Facades\DB::table('images')->insert([
                'product_id' => $productID,
                'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
                'name' => '',
                'extension' => ''
            ]);
        }

        foreach ($categoryIDs as $categoryID) {
            \Illuminate\Support\Facades\DB::table('images')->insert([
                'category_id' => $categoryID,
                'path' => $faker->imageUrl(400, 400, 'cats', true, 'Faker'),
                'name' => '',
                'extension' => ''
            ]);
        }



    }
}

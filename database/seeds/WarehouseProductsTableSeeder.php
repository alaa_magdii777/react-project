<?php

use Illuminate\Database\Seeder;

class WarehouseProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('warehouse_products')->delete();
        //factory(App\WarehouseProduct::class, 30)->create();
        $productIDs = \App\Product::pluck('id')->toArray();

        foreach ($productIDs as $productID) {
            $warehouseIDs = \App\Warehouse::pluck('id')->toArray();
            $warehouseIndex = array_rand($warehouseIDs);
            \Illuminate\Support\Facades\DB::table('warehouse_products')->insert([
                'warehouse_id' => $warehouseIDs[$warehouseIndex],
                'product_id' => $productID,
                'quantity' => random_int(1, 30),
            ]);
        }
    }
}

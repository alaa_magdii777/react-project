<?php

use Illuminate\Database\Seeder;

class ShippersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('Shippers') -> delete();
        factory(\App\Shipper::class,7)->create();
    }
}

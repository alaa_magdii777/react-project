<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('city_id')
            ->nullable();
            $table->string('first_name', 75)
            ->nullable(false);
            $table->string('last_name', 75)
            ->nullable();
            $table->string('address', 125)
            ->nullable();
            $table->string('phone', 30)
            ->nullable();
            $table->float('lat')
            ->nullable();
            $table->float('long')
            ->nullable();
            $table->boolean('is_available')->default(0);
            $table->timestamps();

            //relations
            $table->foreign('city_id')
            ->references('id')
            ->on('cities')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}

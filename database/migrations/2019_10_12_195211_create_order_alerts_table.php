<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id')
            ->nullable();
            $table->text('content')
            ->nullable(false);
            $table->boolean('is_seen')
            ->default(0);
            $table->boolean('is_available')
            ->default(0);
            $table->timestamps();

            //relations
            $table->foreign('order_id')
            ->references('id')
            ->on('orders')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_alerts');
    }
}

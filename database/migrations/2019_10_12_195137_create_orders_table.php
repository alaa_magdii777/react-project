<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')
            ->nullable();
            $table->string('first_name', 75)
            ->nullable(false);
            $table->string('last_name', 75)
            ->nullable();
            $table->string('email', 100)
            ->nullable();
            $table->string('address', 125)
            ->nullable(false);
            $table->string('phone', 25)
            ->nullable(false);
            $table->decimal('total_cost')
            ->nullable(false);
            $table->float('total_discount')
            ->nullable(false);
            $table->dateTime('delivery_date')
            ->nullable(false);
            $table->boolean('is_delivered')
            ->default(0);
            $table->boolean('is_available')
            ->default(0);
            $table->timestamps();

            //relations
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 75)->nullable();
            $table->string('description', 125)->nullable();
            $table->string('phone_code', 10)->nullable();
            $table->string('phone_one', 75)->nullable();
            $table->string('phone_two', 75)->nullable();
            $table->string('phone_three', 75)->nullable();
            $table->string('address', 100)->nullable();
            $table->string('fax', 25)->nullable();
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->float('lat')->nullable();
            $table->float('long')->nullable();
            $table->text('logo_path')->nullable(true);
            $table->text('logo_name')->nullable(true);
            $table->string('logo_extension', 10)->nullable(true);
            $table->float('logo_size_kb')->unsigned()->nullable();
            $table->date('privacy_updated_at')->nullable();
            $table->boolean('is_available')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_configs');
    }
}

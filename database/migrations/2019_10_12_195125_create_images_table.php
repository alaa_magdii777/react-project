<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')
            ->nullable();
            $table->unsignedBigInteger('category_id')
            ->nullable();
            $table->unsignedBigInteger('product_id')
            ->nullable();
            $table->unsignedBigInteger('brand_id')
            ->nullable();
            $table->text('path')
            ->nullable(false);
            $table->text('name')
            ->nullable(false);
            $table->string('extension', 10)
            ->nullable(false);
            $table->integer('size_kb')
            ->unsigned()
            ->nullable();
            $table->boolean('is_available')
            ->default(0);
            $table->timestamps();

            //relations
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
            
            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('brand_id')
            ->references('id')
            ->on('brands')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}

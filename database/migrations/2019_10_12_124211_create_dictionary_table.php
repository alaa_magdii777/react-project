<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key', 125)
            ->nullable(false);
            $table->string('arabic', 125)
            ->nullable(false);
            $table->string('english', 125)
            ->nullable(false);
            $table->string('turkish', 125)
            ->nullable(false);
            $table->boolean('is_available')
            ->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary');
    }
}

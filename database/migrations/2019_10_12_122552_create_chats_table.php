<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('technical_support_id')->nullable();
            $table->text('message')->nullable(false);
            $table->boolean('is_seen')->default(0);
            $table->boolean('is_forward')->default(0);
            $table->boolean('is_available')->default(0);
            $table->timestamps();

            //relations
            $table->foreign('technical_support_id')
            ->references('id')
            ->on('technical_supports')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}

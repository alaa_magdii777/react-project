<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->string('name')->unique()->nullable(false);
            $table->decimal('buying_price', 8, 2)->nullable(false);
            $table->decimal('selling_price', 8, 2)->nullable(false);
            $table->float('discount')->nullable(false);
            $table->text('description')->nullable();
            $table->string('keywords', 125)->nullable();
            $table->boolean('is_new')->default(1);
            //$table->boolean('is_soldOut');
            $table->boolean('is_available')->default(0);
            $table->timestamps();

            //relations
            $table->foreign('supplier_id')
            ->references('id')
            ->on('suppliers')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('brand_id')
            ->references('id')
            ->on('brands')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id')
            ->nullable();
            $table->unsignedBigInteger('product_id')
            ->nullable();
            $table->string('color', 25)
            ->nullable();
            $table->string('size', 75)
            ->nullable();
            $table->integer('amount')
            ->unsigned()
            ->nullable(false);
            $table->decimal('price', 8, 2)
            ->nullable(false);
            $table->float('discount')
            ->nullable(false);
            $table->boolean('is_available')->default(0);
            $table->timestamps();

            //relations
            $table->foreign('order_id')
            ->references('id')
            ->on('orders')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('city_id')->unsigned()->nullable();
            $table->unsignedBigInteger('gender_id')->unsigned()->nullable();
            $table->string('first_name', 75)->nullable(false);
            $table->string('last_name', 75)->nullable();
            $table->string('username', 75)->unique()->nullable(false);
            $table->string('email', 100)->unique()->nullable(false);
            $table->string('password', 100)->nullable(false);
            $table->date('birth_date', 100)->nullable();
            $table->string('address', 125)->nullable();
            $table->string('phone', 30)->nullable();
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_tech')->default(0);
            $table->boolean('is_available')->default(1);
            $table->string('api_token', 125)->nullable(true);
            $table->timestamps();

            //relations
            $table->foreign('city_id')
            ->references('id')
            ->on('cities')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

            $table->foreign('gender_id')
            ->references('id')
            ->on('genders')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('country_id')
            ->nullable();
            $table->string('name', 75)
            ->unique()
            ->nullable(false);
            $table->float('lat')
            ->nullable();
            $table->float('long')
            ->nullable();
            $table->boolean('is_available')
            ->default(0);
            $table->timestamps();
            
            //relations
            $table->foreign('country_id')
            ->references('id')
            ->on('countries')
            ->onUpdate('CASCADE')
            ->onDelete('SET NULL');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}

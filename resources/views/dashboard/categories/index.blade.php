@extends('dashboard.common.layout')

@section('content')
    <div class="dash categories-page">
        <div class="container clearfix">
            {{--Start Bread crump--}}
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Categories</li>
                </ol>
            </nav>
            {{--End Bread crump--}}

            {{--Start add category--}}
            <button type="button" class="btn btn-primary primary float-right">
                <a href="/dashboard/categories/add-category"> <i class="fas fa-fw fa-plus-circle"></i>
                    Add Categories</a>
            </button>
            <div class="clearfix"></div>
            {{--End add category--}}

            <main>
                <div class="row">
                    @foreach(\App\Category::all() as $Category)
                        @include('dashboard.common.category-card')
                    @endforeach
                </div>
            </main>
        </div>
    </div>
@endsection

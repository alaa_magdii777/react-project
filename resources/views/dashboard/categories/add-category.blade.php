@extends('dashboard.common.layout')

@section('content')

<div class="container dash edit mt-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>

    <div class="text-right">
        <button type="button" class="btn btn-primary primary mb-3">
            <a href="/dashboard/categories/category-form" class="text-white"> <i class="fas fa-fw fa-plus-circle"></i>
                Add Category</a>
        </button>
    </div>
    <div class="row">

@foreach(\App\Category::all() as $category )

        <div class="card shadow col-lg-5 col-md-5 col-5 mb-5 ml-5">
            <img src="">
            <div class="card-body">
                <div class="card-title">
                    <h5>{{$category->name}}</h5>
                </div>
                <div class="card-text">
                    {{$category->description}}
                </div>

                    <button class="btn btn-sm btn-warning mt-5"><a href="" style="color: #fff">Edit</a> </button>

            </div>
        </div>
@endforeach


    </div>
</div>

@endsection

@extends('dashboard.common.layout')

@section('content')

<div class="container dash">
    @include('user.common.errors')
    <div class="row">
        <form action="" method="post" class="border m-5 p-5 shadow col-lg-10 col-10" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="capital d-block" for="image">upload category's image*</label>
                <input class="form-control" type="file" name="image" id="product-image-input" hidden>
                <div id="custom-input-btn" class="custom-input-btn d-flex align-items-center justify-content-center"><img id="upload-img" src="{{asset('assets/dashboard/img/upload-image.svg')}}" alt=""></div>
            </div>
            <div class="form-group">
                <label for="name">Category Name</label>
                <input type="text" class="w-100 rounded p-2" name="name" id="name">
            </div>
            <div class="form-group">
                <label class="pt-3" for="description">Category Description</label>
                <textarea type="text" class="w-100 rounded p-2" name="description" id="description"></textarea>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-success btn-sm">Add</button>
            </div>
        </form>
    </div>
</div>

@endsection

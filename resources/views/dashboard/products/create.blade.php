@extends('dashboard.common.layout')

@section('content')
    <div class="dash add-product-page">
       <div class="wrapper">
           <div class="container">
               <h1 class="dash-title mb-5">Add Product</h1>
               {{--Start Bread crump--}}
               <nav aria-label="breadcrumb">
                   <ol class="breadcrumb">
                       <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                       <li class="breadcrumb-item"><a href="/dashboard/products">Products</a></li>
                       <li class="breadcrumb-item active" aria-current="page">Add Product</li>
                   </ol>
               </nav>
               @if(session('status'))
                   @include('dashboard.common.confirmation-message')
               @else
                   <main class="">
                       @include('user.common.errors')
                       {{--End Bread crump--}}
                       <form id="product-form" action="" method="POST" enctype="multipart/form-data">
                           @csrf
                           {{--Start row--}}
                           <div class="row">
                               {{--Start column--}}
                               <div class="col-lg-6 mb-lg-0 mb-5">
                                   {{--Product Image--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="image">upload product's image*</label>
                                       <input class="form-control" type="file" name="image" id="product-image-input" hidden>
                                       <div id="custom-input-btn" class="custom-input-btn d-flex align-items-center justify-content-center"><img id="upload-img" src="{{asset('assets/dashboard/img/upload-image.svg')}}" alt=""></div>
                                   </div>
                                   {{--Product Name--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="name">product name*</label>
                                       <input class="form-control bg-light border-0 small"
                                              type="text"
                                              id="product-name"
                                              name="name"
                                              placeholder="Name" >
                                   </div>
                                   {{--Product Category--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="category">category*</label>
                                       <select class="form-control bg-light border-0 small"
                                               type="text"
                                               id="product-category"
                                               name="category"
                                               placeholder="Category" >
                                           <option selected disabled hidden>Select</option>
                                           @foreach(\App\Category::all() as $cat)
                                               <option value="{{$cat->id}}">{{$cat->name}}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                   {{--Product buying pricee--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="buying-price">buying price*</label>
                                       <input class="form-control bg-light border-0 small"
                                              type="text"
                                              id="product-buying-price"
                                              name="buying-price"
                                              placeholder="buying price" >
                                   </div>
                                   {{--Product selling pricee--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="selling-price">selling price*</label>
                                       <input class="form-control bg-light border-0 small"
                                              type="text"
                                              id="product-selling-price"
                                              name="selling-price"
                                              placeholder="Selling price" >
                                   </div>
                                   {{--Product Discount--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="discount">discount</label>
                                       <input class="form-control bg-light border-0 small"
                                              type="text"
                                              id="product-discount"
                                              name="discount"
                                              placeholder="discount" >
                                       <p class="text-muted mt-2"><small>Please enter discount percentage %</small></p>
                                   </div>
                               </div>
                               {{--End column--}}
                               {{--Start column--}}
                               <div class="col-lg-6">
                                   {{--Product Supplier--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="supplier">supplier*</label>
                                       <select class="form-control bg-light border-0 small"
                                               type="text"
                                               id="product-supplier"
                                               name="supplier"
                                               placeholder="supplier" >
                                           <option selected disabled hidden>Select</option>
                                           @foreach(\App\Supplier::all() as $supplier)
                                               <option value="{{$supplier->id}}">{{$supplier->first_name}} {{$supplier->last_name}}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                   {{--Product brand--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="brand">brand*</label>
                                       <select class="form-control bg-light border-0 small"
                                               type="text"
                                               id="product-brand"
                                               name="brand"
                                               placeholder="brand" >
                                           <option selected disabled hidden>Select</option>
                                           @foreach(\App\Brand::all() as $brand)
                                               <option value="{{$brand->id}}">{{$brand->name}}</option>
                                           @endforeach
                                       </select>
                                   </div>
                                   {{--Product Description--}}
                                   <div class="form-group">
                                       <label class="capital d-block" for="description">description</label>
                                       <textarea class="form-control bg-light border-0 small"
                                                 type="text"
                                                 id="product-description"
                                                 name="description"
                                                 placeholder="description"></textarea>
                                   </div>
                               </div>
                               {{--End column--}}
                           </div>
                           {{--End row--}}

                           <div class="btn-wrapper d-flex align-items-start">
                               <button class="btn-primary primary btn mr-3">add Product</button>
                               <button id="reset-data" type="button" class="text-muted btn btn-light">Reset</button>
                           </div>
                       </form>
                   </main>
               @endif
           </div>
       </div>
   </div>
@endsection

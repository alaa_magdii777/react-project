@php
    $counter = 0;
    $productsNumber = \App\Product::select(\Illuminate\Support\Facades\DB::raw('count(id) AS c'))->first()['c'];
    $products = \App\Product::paginate(25);
//dd($products);
@endphp
@extends('dashboard.common.layout')

@section('content')
    <div class="dash products-page">
        <div class="wrapper">
            <div class="container clearfix">
                <h1 class="dash-title mb-5">Products</h1>
                {{--Start Bread crump--}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Products</li>
                    </ol>
                </nav>
                {{--End Bread crump--}}

                {{--Start add products--}}
                <button type="button" class="btn btn-primary primary float-right">
                    <a href="/dashboard/products/create"> <i class="fas fa-fw fa-plus-circle"></i>
                        Add Products</a>
                </button>
                {{--<div class="badge badge-light btn float-left">{{$productsNumber}} Products found</div>--}}
                <div class="clearfix"></div>
                {{--End add products--}}

                <main>
                    <div class="row">
                            <table class="text-center" data-toggle="table">
                                <thead class="bg-primary text-light">
                                    <tr>
                                        <th>NO.</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Quantity</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $product)
                                        @php
                                            $imageObj = \App\Image::where('product_id', '=', $product->id)->first();
                                            $image = $imageObj->path . $imageObj->name;
                                            $quantity = \App\WarehouseProduct::Select(DB::raw('sum(quantity) as q'))
                                            ->where('product_id', '=', $product->id)->first()['q'] ?? 0;
                                            $counter++;
                                        @endphp
                                        <tr>
                                            <td>{{($products->currentpage() -1) * $products->perpage() + $counter}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->buying_price}}$</td>
                                            <td>{{$product->discount}}%</td>
                                            <td>{{$quantity}}</td>
                                            <td> <a href="/dashboard/products/{{$product->id}}/edit" class="align-self-start mt-auto">View Product</a></td>
                                            <td><a href="/dashboard/products/{{$product->id}}/delete" class="align-self-start mt-auto">Delete Product</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                    <div class="pagination-wrapper mt-5">
                        {{$products->links()}}
                    </div>
                </main>
            </div>
        </div>
    </div>
@endsection

@extends('dashboard.common.layout')

@section('content')
    <div class="dash edit">
        <div class="wrapper">
            <div class="container clearfix">
                <h1 class="dash-title mb-5">Edit Product</h1>
                {{--Start Bread crump--}}
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/dashboard/products/">Products</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Product</li>
                    </ol>
                </nav>

                {{--End Bread crump--}}
                @if(session('status'))
                    @include('dashboard.common.confirmation-message')
                    @else
                    {{--Start Edit Form--}}
                    <main class="">
                        @include('user.common.errors')
                        <form id="product-form" class="edit-product-form" action="" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{--Product Image--}}
                            <div class="form-group">
                                <label class="capital d-block" for="image">upload products image*</label>
                                <input value="{{$data['image']}}" class="form-control" type="file" name="image" id="product-image-input" hidden>
                                <div id="custom-input-btn" class="custom-input-btn d-flex align-items-center justify-content-center" style="background-image: url({{$data['image']}})"><img id="upload-img" src="{{asset('assets/dashboard/img/upload-image.svg')}}" alt=""></div>
                            </div>
                            {{--Product Name--}}
                            <div class="form-group">
                                <label class="capital d-block" for="name">product name*</label>
                                <input class="form-control bg-light border-0 small"
                                       type="text"
                                       id="product-name"
                                       name="name"
                                       placeholder="Name" value="{{$data['name']}}">
                            </div>
                            {{--Product Category--}}
                            <div class="form-group">
                                <label class="capital d-block" for="category">category*</label>
                                <select class="form-control bg-light border-0 small"
                                        type="text"
                                        id="product-category"
                                        name="category"
                                        placeholder="Category" >
                                    @foreach(\App\Category::all() as $cat)
                                        @php
                                            $attr = ($cat->id == $data['category_id']) ? 'selected' : ''
                                        @endphp
                                        <option value="{{$cat->id}}" {{$attr}}>{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--Product buying pricee--}}
                            <div class="form-group">
                                <label class="capital d-block" for="buying-price">buying price*</label>
                                <input class="form-control bg-light border-0 small"
                                       type="text"
                                       id="product-buying-price"
                                       name="buying-price"
                                       placeholder="buying price"
                                        value="{{$data['buying_price']}}">
                            </div>
                            {{--Product selling pricee--}}
                            <div class="form-group">
                                <label class="capital d-block" for="selling-price">selling price*</label>
                                <input class="form-control bg-light border-0 small"
                                       type="text"
                                       id="product-selling-price"
                                       name="selling-price"
                                       placeholder="Selling price"
                                        value="{{$data['selling_price']}}">
                            </div>
                            {{--Product Discount--}}
                            <div class="form-group">
                                <label class="capital d-block" for="discount">discount</label>
                                <input class="form-control bg-light border-0 small"
                                       type="text"
                                       id="product-discount"
                                       name="discount"
                                       placeholder="discount" value="{{$data['discount']}}">
                                <p class="text-muted mt-2"><small>Please enter discount percentage %</small></p>
                            </div>
                            {{--Product Supplier--}}
                            <div class="form-group">
                                <label class="capital d-block" for="supplier">supplier*</label>
                                <select class="form-control bg-light border-0 small"
                                        type="text"
                                        id="product-supplier"
                                        name="supplier"
                                        placeholder="supplier" >
                                    @foreach(\App\Supplier::all() as $supplier)
                                        @php
                                            $attr = ($supplier->id == $data['supplier_id']) ? 'selected' : ''
                                        @endphp
                                        <option value="{{$supplier->id}}" {{$attr}}>{{$supplier->first_name}} {{$supplier->last_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--Product brand--}}
                            <div class="form-group">
                                <label class="capital d-block" for="brand">brand*</label>
                                <select class="form-control bg-light border-0 small"
                                        type="text"
                                        id="product-brand"
                                        name="brand"
                                        placeholder="brand" >
                                    @foreach(\App\Brand::all() as $brand)
                                        @php
                                            $attr = ($brand->id == $data['brand_id']) ? 'selected' : ''
                                        @endphp
                                        <option value="{{$brand->id}}" {{$attr}}>{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--Product Description--}}
                            <div class="form-group">
                                <label class="capital d-block" for="description">description</label>
                                <textarea class="form-control bg-light border-0 small"
                                          type="text"
                                          id="product-description"
                                          name="description"
                                          placeholder="description">{{$data['description']}}</textarea>
                            </div>
                            <div class="btn-wrapper d-flex align-items-start">
                                <button class="btn-primary primary btn mr-3">Edit Product</button>
                                <button id="reset-data" type="button" class="text-muted btn btn-light">Reset</button>
                            </div>
                        </form>
                    </main>
                    {{--End Edit Form--}}
                @endif

            </div>
        </div>
    </div>
@endsection

@extends('dashboard.common.layout')
@php
    $productsNumber = \App\Product::select(\Illuminate\Support\Facades\DB::raw('count(id) AS c'))->first()['c'];
@endphp
@section('content')

    <div class="dash dash-page">
        <div class="wrapper">
            <!-- Begin Page Content -->
            <div class="container-fluid dash">
                <h1 class="dash-title mb-5">Dashboard</h1>
                <div class="row">
                    {{--Card--}}
                    <div class="col-lg-4 col-md-5 mb-4">
                        <a class="card text-white bg-primary mb-3 mx-auto mx-md-0" style="max-width: 18rem;" href="dashboard/products/">
                            <div class="card-body">
                                <h5 class="card-title d-flex justify-items-center"><i class="fas fa-shopping-cart mr-2"></i><span class="mr-auto">Products</span><span class="badge badge-light text-muted">{{$productsNumber}}</span></h5>
                                <p class="card-text">View, Add, Delete, Update Products</p>
                            </div>
                        </a>
                    </div>
                    {{--Card--}}
                    <div class="col-lg-4 col-md-5 mb-4">
                         <a class="card text-white bg-success mb-3 mx-auto mx-md-0 " style="max-width: 18rem;" href="dashboard/categories/add-category">
                              <div class="card-body">
                              <h5 class="card-title">Categories</h5>
                              <p class="card-text">View, Add, Delete, Update Categories</p>
                            </div>
                         </a>
                    </div>

                    {{--Card--}}
                    <div class="col-lg-4 col-md-5">
                        <a class="card text-white bg-warning mb-3 mx-auto mx-md-0" style="max-width: 18rem;" href="dashboard/support/">
                            <div class="card-body">
                                <h5 class="card-title"><i class="fas fa-comment-alt mr-2"></i>Support</h5>
                                <p class="card-text">Receive & Respond to user's Issues</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- /.container-fluid -->
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
@endsection

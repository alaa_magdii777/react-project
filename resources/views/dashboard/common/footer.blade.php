<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="{{asset('assets/dashboard/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/dashboard/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('assets/dashboard/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('assets/dashboard/js/sb-admin-2.js')}}"></script>

<!-- Page level plugins -->
<script src="{{asset('assets/dashboard/vendor/chart.js/Chart.min.js')}}"></script>
{{--bootstrap table library--}}
<script src="{{asset('assets/dashboard/js/bootstrap-table.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('assets/dashboard/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('assets/dashboard/js/demo/chart-pie-demo.js')}}"></script>

</body>

</html>

<div class="alert alert-success d-flex align-items-center">
    <i class="fas fa-check-circle mr-2"></i>
    {{session('status') ?? $status}}
</div>
<button class="btn btn-primary primary"><a class="text-light" href="/dashboard/products">Back to products</a></button>

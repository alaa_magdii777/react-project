@php
    $imageObj = \App\Image::where('product_id', '=', $product->id)->first();
    $image = $imageObj->path . $imageObj->name;
@endphp

<div class="col-lg-6 col-xl-4 mb-4">
    <div class="card mx-auto" style="width: 18rem;">


            <tr>
                <td>2</td>
                <td>Item 2</td>
                <td>$2</td>
            </tr>
        <img src="{{$image}}" class="card-img-top" alt="...">
        <div class="card-body d-flex flex-column">
            <h2 class="card-title text-primary">{{$product->name}}</h2>
            <div class="cost-wrapper">
                <h3 class="price">{{$product->price}}$</h3>
                @if($product->discount > 0)
                    <div class="discount badge badge-primary">{{$product->discount}}%</div>
                @endif
            </div>
            <p class="card-text">{{$product->description}}</p>
            <a href="{{URL::to('dashboard/products/edit-product-' . $product->id)}}" class="btn btn-primary align-self-start mt-auto">View Product</a>
        </div>
    </div>
</div>

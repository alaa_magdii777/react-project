{{--head--}}
@include('dashboard.common.head')
{{--nave--}}
@include('dashboard.common.nav')

{{--page content--}}
@yield('content')

{{--footer--}}
@include('dashboard.common.footer')

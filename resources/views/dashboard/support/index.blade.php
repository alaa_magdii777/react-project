@include('dashboard.common.head')

<div class="dash support">
    <div class="row">
        {{--all messages--}}
        <div class="col-md-6">
            <div class="side all-messages">
                <div class="container">
                    <div class="head-wrapper d-flex align-items-start">
                        <a class="text-light mr-auto" href="/dashboard"><i class="fas fa-arrow-left"></i></a>
                        <div class="logo font-weight-bold text-center"><a href="/" class="text-light">Market</a></div>
                    </div>
                </div>
                <div class="boxes-wrapper">
                    @php
                        $supports =  \App\TechnicalSupport::all()->toArray();
                    @endphp
                    @foreach ($supports as $support)
                        @php
                            $chat = \App\Chat::where('technical_support_id', '=', $support['id'])->orderBy('created_at', 'DESC')
                            ->get()->toArray();
                            $lastMessage = end($chat)['message'];
                            $user_id = $support['user_id'];
                            $user = \App\User::find($user_id);
                            $userFullName = $user->first_name . ' ' . $user->lastName;
                            $userImage = \App\Image::where('user_id', '=', $user_id)->first();
                            $userImage = $userImage['path'] . $userImage['name'];
                        @endphp
                            <a class="message-box d-block" href="chat-{{$support['id']}}">
                                <div class="container">
                                    <div class="message-wrapper d-flex align-items-center">
                                        <div class="avatar mr-3"><img class="rounded-circle w-100" src="{{$userImage}}" alt=""></div>
                                        <div class="info">
                                            <p class="name mb-2 clearfix"><span class="full-name font-weight-bold float-left">{{$userFullName}}</span> <span class="time float-right">10:50</span></p>
                                            <p class="last-msg mb-0">{{$lastMessage}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('dashboard.common.footer')

@php

    /*$product= \App\OrderDetail::select(\Illuminate\Support\Facades\DB::raw('count(id) AS c'))->first()['c'];*/
    $categories = \App\Category::all()->toArray();
    $newItemsCounter = 0;
    $withDiscountCounter = 0;
    foreach ($data['products'] as $product) {
        $newItemIsFound = \App\Product::where('id', '=', $product['id'])
        ->where('is_new', '=', true)->first();

        $discountFound = \App\Product::where('id', '=', $product['id'])
        ->where('discount', '>', 0)->first();

        if($newItemIsFound) {
            $newItemsCounter++;
        }

        if($discountFound) {
            $withDiscountCounter++;
        }
    }
@endphp

@extends('user.common.layout')

@section('title')
Market
@endsection

@section('content')

  <!-- header -->
  <header class="main-header categories-header">
        <div class="container">
            <h1>{{$data['categoryName']}}</h1>
            <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
        </div>
    </header>

    <!-- main content -->
    <main class="categories-page category-details">
        <div class="container">
            <div class="row">
                <!-- categories controls-->
                <div class="col-lg-3 mb-lg-0 mb-5">
                    <section class="categories-controls">
                        <!-- sort -->
                        <form class="sort">
                            <section class="form-group sort">
                                <h2 class="h4 text-center">Sorting</h2>
                                <hr>
                                <!-- by name -->
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-name-desc">
                                    <i class="fas fa-long-arrow-alt-down"></i>
                                    <label for="filter-name-desc">Name Desc</label>
                                </p>
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-name-asc">
                                    <i class="fas fa-long-arrow-alt-up"></i>
                                    <label for="filter-name-asc">Name Asc</label>
                                </p>
                                <!-- by price -->
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-price-desc">
                                    <i class="fas fa-long-arrow-alt-down"></i>
                                    <label for="filter-price-desc">Price Desc</label>
                                </p>
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-price-asc">
                                    <i class="fas fa-long-arrow-alt-up"></i>
                                    <label for="filter-price-desc">Price Asc</label>
                                </p>
                                <!-- by discount -->
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-discount-desc">
                                    <i class="fas fa-long-arrow-alt-down"></i>
                                    <label for="filter-discount-desc">Discount Desc</label>
                                </p>
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-discount-asc">
                                    <i class="fas fa-long-arrow-alt-up"></i>
                                    <label for="filter-discount-desc">Discount Asc</label>
                                </p>
                                <!-- by sales -->
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-sales-desc">
                                    <i class="fas fa-long-arrow-alt-down"></i>
                                    <label for="filter-sales-desc">Sales Desc</label>
                                </p>
                                <p class="input-field m-0 px-4">
                                    <input type="radio" name="sort" id="filter-sales-asc">
                                    <i class="fas fa-long-arrow-alt-up"></i>
                                    <label for="filter-sales-desc">Sales Asc</label>
                                </p>
                            </section>
                            <hr>
                            <section class="form-group filter mb-0">
                                <h2 class="h4 text-center">Filtering</h2>
                                <hr>
                                <section class="category-filter">
                                    <!-- field -->
                                {{--<p class="input-field m-0 px-4">
                                    <input type="checkbox" name="filer[]" id="all-cats" data-filter="all">
                                    <label for="all-ages">All Categories</label>
                                </p>--}}
                                @foreach($categories as $category)
                                    @php
                                        $productsCounter = 0;
                                            foreach ($data['products'] as $product) {
                                                $is_found = \App\Product::where('id', '=', $product['id'])
                                                ->where('category_id', '=', $category['id'])->first();
                                                if($is_found) {
                                                    $productsCounter++;
                                                }
                                            }
                                    @endphp
                                    @if($productsCounter > 0)
                                        <!-- field -->
                                            <p class="input-field d-flex align-items-baseline m-0 px-4">
                                                <input type="checkbox" name="filer[]" id="filter-{{$category['name']}}" data-toggle=".{{$category['name']}}">
                                                <label class="d-flex align-items-center w-100 ml-2 mb-0" for="all-ages"><span>{{$category['name']}}</span> <span class="ml-auto">({{$productsCounter}})</span></label>
                                            </p>
                                        @endif
                                    @endforeach

                                </section>
                                <hr>
                                <section class="state-filter">
                                    <!-- field -->
                                    <p class="input-field d-flex align-items-baseline m-0 px-4">
                                        <input type="checkbox" name="state[]" id="newOnly" data-toggle=".new">
                                        <label class="d-flex align-items-center w-100 ml-2 mb-0" for="newOnly"><span>New Only</span> <span class="ml-auto">({{$newItemsCounter}})</span></label>
                                    </p>
                                    <!-- field -->
                                    <p class="input-field d-flex align-items-baseline m-0 px-4">
                                        <input type="checkbox" name="state[]" id="withDiscount" data-toggle=".discount">
                                        <label class="d-flex align-items-center w-100 ml-2 mb-0" for="withDiscount"><span>With Discount</span> <span class="ml-auto">({{$withDiscountCounter}})</span></label>
                                    </p>
                                </section>
                                <hr>
                                <section class="price-filter">
                                    <!-- field -->
                                    <div class="input-field mb-3 px-4">
                                        <div class="wrapper d-flex align-items-center">
                                            <label class="mr-auto mb-0" for="from-price">From Price</label>
                                            <p class="d-inline-block display-value m-0" id="from-price-value">2000</p>
                                        </div>
                                        <input type="range" name="from-price" id="from-price" value="2000" min="0" max="5000">
                                    </div>
                                    <!-- field -->
                                    <div class="input-field px-4">
                                        <div class="wrapper d-flex align-items-center">
                                            <label class="mr-auto mb-0" for="to-price">To Price</label>
                                            <p class="d-inline-block m-0 display-value" id="to-price-value">4000</p>
                                        </div>
                                        <input type="range" name="to-price" id="to-price" value="4000" min="0" max="5000">
                                    </div>
                                </section>
                                <hr>
                                <section class="products-limit-number">
                                    <!-- field -->
                                    <p class="input-field m-0 px-4">
                                        <label class="d-block" for="to-price">Numbet of Products per Page</label>
                                        <select name="products-limit" id="products-limit">
                                            <option value="5" default>5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </p>
                                </section>
                            </section>
                        </form>
                    </section>
                </div>
                <!-- products -->
                <div class="col-lg-9">
                    <section class="products">
                        <!-- search form -->
                        <form action="" class="search-category-form">
                            <p class="input-field d-flex">
                                <button type="submit"><i class="fas fa-search"></i></button><input type="text" id="search-category" name="search-category" placeholder='search'>
                            </p>
                        </form>
                        <!-- categories -->
                        {{--Start row--}}
                        <div id="mix-container" class="row">
                            @foreach($data['products'] as $product)
                                @include('user.common.product-card')
                            @endforeach
                            @for($i = 0; $i < 6; $i++)
                                    @include('user.common.product-card')
                            @endfor
                        </div>
                        {{--End row--}}
                        <div class="row">
                            <button class="btn btn-circle btn-lg mx-auto" style="background-color:#a7c6d9; color: white; ">See More</button>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>
@endsection

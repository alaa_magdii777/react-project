<div class="container">
    <div class="chat" id="chat">
        <div class="wrapper">
            <div class="chat-head d-flex align-content-center p-3">
                <div class="user-info d-flex">
                    <div class="image-wrapper mr-2">
                        <img class="user-image w-100" src="{{asset('assets/images/users-images/weam-adel-10.jpg')}}" alt="">
                    </div>
                    <p class="user-name mb-0 font-weight-bold">Weam Adel</p>
                </div>
                <div class="close-chat ml-auto" id="close-chat-1">x</div>
            </div>
            <div class="chat-room p-3">
            </div>
            <form action="" method="POST" class="message-form d-flex">
                <textarea name="message" id="message"></textarea><button class="p-0"><i class="far fa-paper-plane d-flex justify-content-center align-items-center"></i></button>
            </form>
        </div>
        <div id="no-messages-yet" class="no-messages-yet p-3 justify-content-center align-items-center text-center">
            <div class="wrapper">
                <div class="close-chat font-weight-bold" id="close-chat-2">x</div>
                <div class="image">
                    <img class="w-100" src="assets/images/chat/no-messages.png" alt="">
                </div>
                <div class="text">
                    <h2 class="capital">Hi i am a little popup</h2>
                    <p>Click below to chat with me about issues, reports and help</p>
                </div>
                <button id="close-dialogue" class="close-dialogue border-0 px-3 py-2">Chat Now</button>
            </div>
        </div>
    </div>

    <div class="toggle-chat">
        <a href="" class="chat-toggler" id="chat-toggler"><img src="assets/images/chat/chat-icon.png" alt=""></a>
    </div>
</div>

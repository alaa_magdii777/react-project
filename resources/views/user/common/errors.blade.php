@if($errors->any())
    <div class="card shadow mt-3">
        <div class="card-body">
            <ul class="list-unstyled mb-0">
                @foreach($errors->all() as $error)
                    <li class="text-danger">
                        <span class="fa fa-caret-right"></span>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
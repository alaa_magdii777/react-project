@php
    $product = \App\Product::find($topSale->product_id);
    $totalAmount = \App\OrderDetail::select(\Illuminate\Support\Facades\DB::raw('sum(amount) as q, product_id'))
            ->where('product_id', '=', $topSale -> product_id)
            ->where('is_available', '=', true)
            ->groupBy('product_id')
            ->first();
    $image = \App\Image::where('product_id', '=', $topSale->product_id)
        ->first();
    $imgURL = $image->path . $image->name;
    $productRating = \App\Review::select(DB::raw('avg(rating) AS avg'))
        ->where('product_id', '=', $topSale->product_id)
        ->first()['avg'];
    $categoryName = \App\Category::find(\App\Product::find($topSale->product_id)->category_id)->name;
    $with_discount = ($product->discount > 0) ? 'discount' : '';
    $is_new = ($product->is_new) ? 'new' : '';

@endphp
{{--Start grid--}}
<div class="col-lg-4 col-md-6 mb-5 mix {{$categoryName . ' ' . $with_discount . ' ' . $is_new}}">
    {{--Start product card--}}
    <div class="product category pb-4 m-auto m-md-0">
        {{--Start image --}}
        <div class="image">
            @if ($image != null)
                <img src="{{asset($image->path . $image->name . "." . $image->extension)}}" alt="">
            @else
                <span class='state discount position-absolute rounded text-white'>{{$product->discount}}%</span>
                @if($product->is_new != null)
                    <span class="state new text-white rounded">New</span>
                @endif
                @if($product->is_soldOut != null)
                    <span class="state sold-out text-white rounded text-center"> Sold Out</span>
                @endif
            @endif
        </div>
        {{--End image --}}

    <!-- Title -->
        <h3 class="item-name h5 text-center m-0 mt-2"> {{$product->name}} </h3>
        <h4 class="item-name h5 text-center text-danger m-0 mt-2"> {{$categoryName}} </h4>
        {{--Start wrapper--}}
        <div class="wrapper text-center my-2 px-4">
            <p class="extra mb-2">
                <span class="mr-1 badge badge-purple badge-pill">
                   Price: {{$product->selling_price}} $
                </span>
                @if($product->is_soldOut == null)
                    @if($totalAmount ->q != null)
                        <span class="mr-1 badge badge-info badge-pill mt-1">
                           Quantity: {{$totalAmount->q != null ? $totalAmount->q: 0}}
                        </span>
                    @endif
                @endif
            </p>
            {{--End wrapper--}}

            {{--Start wrapper--}}
            <div class="wrapper d-flex justify-content-center">
                <p id="product-rating-{{$topSale->product_id}}" class="rating mb-0"
                   data-rating="{{$productRating}}">{{$productRating}}</p>
            </div>
            {{--End wrapper--}}
        </div>
        <p class="item-summary mb-2 px-4" title="{{$product->description}}">
            {{$product->description}}
        </p>
        {{--Start buttons wrapper--}}
        <div class="buttons-wrapper text-center">
            <button class="secondary p-0 mb-2">
                <a class="p-0" href="/{{$product->id}}/product-details">
                    Details
                </a>
            </button>
            {{--End buttons wrapper --}}
            @if($product->is_soldOut == null)
{{--                <button id="product-add-btn-{{$topSale->product_id}}" class="primary"><a--}}
{{--                            href="add-to-cart-{{$topSale->product_id}}">Add to Cart</a></button>--}}
                <button data-price="{{$product->selling_price}}" data-discount="{{$product->discount}}"
                        data-max="{{$totalAmount->q}}"
                        data-name="{{$product->name}}" data-imageUrl="{{asset('/assets/images/p-02-01.jpg')}}"
                data-id="{{$topSale->product_id}}" data-imgURL="{{$imgURL}}" class="addToCart primary">Add to Cart</button>
            @endif
        </div>
        {{--End buttons wrapper--}}
    </div>
    {{--end product card--}}
</div>
{{--End grid--}}

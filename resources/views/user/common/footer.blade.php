        <!-- Footer -->
        <footer class="main-footer">
            <div class="container">
                <div class="row">
                    <!-- Quick links -->
                    <div class="col-lg-2 mb-5 mb-lg-0">
                        <div class="footer-column quik-links">
                            <h3 class="h4 mb-4">Quick Links</h3>
                            <ul class="list-unstyled list">
                                <li class="mb-2"><a href="/about"><i class="fas fa-caret-right mr-2"></i>About Us</a></li>
                                <li class="mb-2"><a href="#"><i class="fas fa-caret-right mr-2"></i>Customer Service</a></li>
                                <li class="mb-2"><a href="/privacypolicy"><i class="fas fa-caret-right mr-2"></i>Privacy Policy</a></li>
                                <li class="mb-2"><a href="/contact-us"><i class="fas fa-caret-right mr-2"></i>Contact Us</a></li>
                                <li><a href="/faq"><i class="fas fa-caret-right mr-2"></i>FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- contact us -->
                    @php
                        $siteConfig = \App\SiteConfig::all()->first()->toArray();
                        $phoneCode = $siteConfig['phone_code'];
                        $phone = $phoneCode . $siteConfig['phone_one'] . '-'
                                .$phoneCode . $siteConfig['phone_two'] . '-'
                                .$phoneCode . $siteConfig['phone_three'];
                        $address = $siteConfig['address'];
                        $fax = $siteConfig['fax'];
                        $lat = $siteConfig['lat'];
                        $long = $siteConfig['long'];
                    @endphp
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="footer-column contact-us">
                            <h3 class="h4 mb-4">Contact Us</h3>
                            <ul class="list-unstyled list">
                                <li class="mb-2"><i class="fas fa-id-card mr-2"></i>{{$address}}</li>
                                <li class="mb-2"><i class="fas fa-phone-square mr-2"></i>{{$phone}}</li>
                                <li><i class="fas fa-fax mr-2"></i>{{$fax}}</li>
                            </ul>
                        </div>
                    </div>
                    <!-- map -->
                    <div class="col-lg-6 mb-5 mb-lg-0">
                        <div class="footer-column map">
                            <h3 class="h4 mb-4">Location</h3>
                            <div id="map" class="map" style="height: 300px" data-long="{{$long}}" data-lat="{{$lat}}"></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- scripts -->
        <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/dashboard/js/bootstrap-table.min.js')}}"></script>
        <script src="{{asset('assets/dashboard/js/bootstrap-table-locale-all.min.js')}}"></script>
        <script src="{{asset('assets/dashboard/js/bootstrap-table-export.min.js')}}"></script>
        <!--Google Maps API-->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoJRxwJxByeiZ8mp8d5Es54aPqAwJH464&callback=myMap"></script>
        <script src="{{asset('assets/js/jquery.rateyo.min.js')}}"></script>
        <script src="{{asset('assets/js/mixitup.min.js')}}"></script>
        @yield('more-script')
        <script src="{{asset('assets/js/app.js?v=6')}}"></script>
    </body>
</html>

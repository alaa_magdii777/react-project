

@include('user.common.head')
@include('user.common.nav')

@yield('content')

@if (Auth::check())
    @include('user.common.popup-chat')
@endif

@include('user.common.footer')


<div class="col-md-6 mb-5">
    <div class="category pb-4">
        <!-- image -->
        <div class="image">
            <img src="https://via.placeholder.com/400x250/#62A718/FFFFFF" alt="">
        </div>
        <!-- Title -->
        <h2 class="item-name h4 text-center m-0 mt-4">
                {{$availCat->name}}
            </h2>
        <div class="wrapper text-center my-4 px-4">
            <p class="extra mb-4"><span class="products-number">ProductNumber : NUM</span></p>
            <p class='cat'><span class="mr-2">Kid</span><span class="mr-2">Men</span><span>Women</span></p>
        </div>
        <p class="item-summary px-4">

        </p>
        <div class="buttons-wrapper text-center">
            <button class="primary"><a href="/{{$availCat->id}}/category">Display Products</a></button>
        </div>
    </div>
</div>

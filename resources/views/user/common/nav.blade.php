<div class="top-bar">
    <div class="container">
        <ul class="m-0 list-unstyled d-flex align-items-center">
            <li class="mr-2">
                <div class="dropdown">
                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false"> EN
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Ar</a>
                        <a class="dropdown-item" href="#">Tu</a>
                    </div>
                </div>
            </li>
            @if(\Illuminate\Support\Facades\Auth::check())
                <li class="mx-auto">
                    {{\Illuminate\Support\Facades\Auth::user()->first_name . " ". \Illuminate\Support\Facades\Auth::user()->last_name}}
                </li>
                @if(\App\User::find(\Illuminate\Support\Facades\Auth::id())->is_admin)
                    <a class="nav-link mr-2" href="/dashboard">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                @endif
            @endif
            <li class="mr-2">
                <!-- <a class="nav-link" href="signout.php">Sign out</a> -->
                @if(!\Illuminate\Support\Facades\Auth::check())
                    <a class="nav-link" href="/login">Sign in</a>
                @else
                    <a class="nav-link" href="/logout">Logout</a>
                @endif
            </li>
            <li><a href="/cart"><i class="fas fa-cart-plus"></i><span id="cart-products-number" class="products-number ml-2"></span></a></li>
        </ul>
    </div>
</div>
<nav class="navbar navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img class="rounded-circle" style="max-height:30px;" src="{{asset('assets/images/logo.svg')}}" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-align-center"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav w-100">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="categories.php" id="categories-dropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>

                    <div class="dropdown-menu" style="width:300px" aria-labelledby="categories-dropdown">
                        <div class="container container-sm">
                            <div class="row">
                                <div class="col-6">
{{--                                    @foreach($data['availCats']->take(3) as $availCat)--}}
{{--                                    <a class="dropdown-item" href="/category/{{$availCat->id}}">{{$availCat->name}}</a>--}}

{{--                                    @endforeach--}}

                                </div>
                                <div class="col-6">

{{--                                    @foreach($data['availCats']->take(2) as $availCat)--}}
{{--                                        <a class="dropdown-item" href="/category/{{$availCat->id}}">{{$availCat->name}}</a>--}}

{{--                                    @endforeach--}}

                                    <a class="nav-link text-info" href="/categories" >see more</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="categories-dropdown">
                        <a class="dropdown-item" href="/categories">All</a>
                        <a class="dropdown-item" href='#'>Category Name</a>

                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="/recommendations">Recommendations</a>
                </li>

            </ul>
        </div>

    </div>
</nav>

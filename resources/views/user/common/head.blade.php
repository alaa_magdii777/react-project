<!DOCTYPE html>
<html  lang="en">
    <head>
        <title>@yield('title')</title>
        <!--Charachter Set-->
        <meta charset="utf-8">
        <!-- First Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--Description Meta-->
        <meta name="description" content="">
        <!--Keywords Meta-->
        <meta name="keywords" content="">
        <!-- Icon -->
		<link href='' rel='icon'>
        <!--Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Stencil+One&display=swap" rel="stylesheet">
        <!-- Font Awsome -->
        <link href="{{asset('assets/css/all.min.css')}}" rel="stylesheet">
        <!--Bootstrap Library-->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <!--Bootstrap Table Library-->
        <link href="{{asset('assets/dashboard/css/bootstrap-table.min.css')}}" rel="stylesheet">
        <!-- rateyo plugin-->
        <link href="{{asset('assets/css/jquery.rateyo.min.css')}}" rel="stylesheet">
        <!-- mixitup plugin-->
        <link href="{{asset('assets/css/jquery.rateyo.min.css')}}" rel="stylesheet">
        <!-- Normalization Style Sheet-->
        <link href="{{asset('assets/css/Normalize.min.css')}}" rel="stylesheet">
        <!-- My style Sheet -->
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
        <!-- Shiv library -->
	    <script src="{{asset('assets/js/html5shiv.min.js')}}"></script>
        <script src="{{asset('assets/js/respond.min.js')}}"></script>
    </head>
    <body>

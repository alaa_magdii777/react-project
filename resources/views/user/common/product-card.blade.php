@php
    $productName = $product['name'];
    $categoryName = $data['categoryName'];
    $totalAmount = \App\WarehouseProduct::select(\Illuminate\Support\Facades\DB::raw('sum(quantity) as q'))
            ->where('product_id', '=', $product['id'])
            ->first()->toArray()['q'];
    $image = \App\Image::where('product_id', '=', $product['id'])->first();
    $productRating = \App\Review::select(DB::raw('avg(rating) AS avg'))
        ->where('product_id', '=', $product['id'])
        ->first()['avg'];
    $productPrice = $product['selling_price'];
    $productDiscount = $product['discount'];
    $with_discount = ($product['discount'] > 0) ? 'discount' : '';
    $is_new = ($product['is_new']) ? 'new' : '';
@endphp

{{--Start grid--}}
<div class="col-lg-4 col-md-6 mb-5 mix {{$categoryName . ' ' . $with_discount . ' ' . $is_new}}">
    {{--Start product card--}}
    <div class="product category pb-4 m-auto m-md-0">
        {{--Start image --}}
        <div class="image">
                <img src="{{asset($image->path . $image->name . "." . $image->extension)}}" alt="">
                <span class='state discount rounded text-white'>{{$productDiscount}}%</span>
                @if($is_new)
                    <span class="state new text-white rounded">New</span>
                @endif
                @if($totalAmount == 0)
                    <span class="state sold-out text-white rounded text-center"> Sold Out</span>
                @endif
        </div>
        {{--End image --}}

    <!-- Title -->
        <h3 class="item-name h5 text-center m-0 mt-2"> {{$productName}} </h3>
        <h4 class="item-name h5 text-center text-danger m-0 mt-2"> {{$categoryName}} </h4>
        {{--Start wrapper--}}
        <div class="wrapper text-center my-2 px-4">
            <p class="extra mb-2">
                <span class="mr-1 badge badge-purple badge-pill">
                   Price: {{$productPrice}} $
                </span>
                @if($totalAmount > 0)
                    <span class="mr-1 badge badge-info badge-pill mt-1">
                       Quantity: {{$totalAmount}}
                    </span>
                @endif
            </p>
            {{--End wrapper--}}

            {{--Start wrapper--}}
            <div class="wrapper d-flex justify-content-center my-2">
                <p id="product-rating-{{$product['id']}}" class="rating mb-0"
                   data-rating="{{$productRating}}">{{$productRating}}</p>
            </div>
            {{--End wrapper--}}
        </div>
        <p class="item-summary px-4" title="{{$product['description']}}">
            {{$product['description']}}
        </p>
        {{--Start buttons wrapper--}}
        <div class="buttons-wrapper text-center">
            <button class="secondary p-0 mb-2">
                <a class="p-0" href="/{{$product['id']}}/product-details">
                    Details
                </a>
            </button>
            {{--End buttons wrapper --}}
            @if($totalAmount)
                {{--                <button id="product-add-btn-{{$topSale->product_id}}" class="primary"><a--}}
                {{--                            href="add-to-cart-{{$topSale->product_id}}">Add to Cart</a></button>--}}
                <button data-price="{{$productPrice}}" data-discount="{{$productDiscount}}"
                        data-max="{{$totalAmount}}"
                        data-name="{{$product['name']}}" data-imageUrl="{{asset('/assets/images/p-02-01.jpg')}}"
                        data-id="{{$product['id']}}" class="addToCart primary">Add to Cart</button>
            @endif
        </div>
        {{--End buttons wrapper--}}
    </div>
    {{--end product card--}}
</div>
{{--End grid--}}

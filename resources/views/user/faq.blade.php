@extends('user.common.layout')

@section('title')
    FAQ
@endsection

@section('content')
    <!-- header -->
    <header class="main-header about-header">
        <div class="container">
            <h1>FAQ</h1>
            <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
        </div>
    </header>

    <!-- maun content -->
    <main class='faq'>
        <div class="container">
            <!-- faq categories -->
            <section class="faq-categories text-center">
                <div class="row">
                    <!-- category -->
                    <div class="col-md-4 mb-5 mb-md-0">
                        <a id="cat-general" class="cat general active d-block" href="#">
                            <span class="d-block capital">general</span>
                            <i class="fas fa-search"></i>
                        </a>
                    </div>
                    <!-- category -->
                    <div class="col-md-4 mb-5 mb-md-0">
                        <a id="cat-payment" class="cat payment d-block" href="#">
                            <span class="d-block capital">payment</span>
                            <i class="fab fa-paypal"></i>
                        </a>
                    </div>
                    <!-- category -->
                    <div class="col-md-4">
                        <a id="cat-bugs" class="cat bugs d-block" href="#">
                            <span class="d-block capital">reports & bugs</span>
                            <i class="fas fa-bug"></i>
                        </a>
                    </div>
                </div>
            </section>
            <!-- questions -->
            <div class="questions-wrapper mt-5">
                @php
                    //faq
                   $faqs = [
                       // general
                       [
                           'catId' => 1,
                           'name' => 'general',
                           'questionsAnswers' => [
                               'eget velit aliquet sagittis id consectetur purus ut' => 'pretium fusce id velit',
                               'pretium fusce id velit' => 'porta nibh venenatis cras sed felis eget velit aliquet sagittis id consectetur',
                               'ut lectus arcu' => 'pretium fusce id velit',
                               'ultrices mi tempus imperdiet nulla malesuada pellentesque elit ' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque diam volutpat commodo sed egestas egestas. Felis eget velit aliquet sagittis id.',
                               'id nibh tortor id aliquet' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                               'venenatis urna cursus eget nunc scelerisque' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem sed risus ultricies tristique nulla aliquet enim tortor at. Felis imperdiet proin fermentum leo vel orci porta non. Morbi non arcu risus quis varius quam.'
                           ]
                       ],
                       // payment
                       [
                           'catId' => 2,
                           'name' => 'payment',
                           'questionsAnswers' => [
                               'pretium fusce id velit' => 'porta nibh venenatis cras sed felis eget velit aliquet sagittis id consectetur',
                               'eget velit aliquet sagittis id consectetur purus ut' => 'pretium fusce id velit',
                               'ultrices mi tempus imperdiet nulla malesuada pellentesque elit ' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque diam volutpat commodo sed egestas egestas. Felis eget velit aliquet sagittis id.',
                               'id nibh tortor id aliquet' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                               'ut lectus arcu' => 'pretium fusce id velit',
                               'venenatis urna cursus eget nunc scelerisque' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem sed risus ultricies tristique nulla aliquet enim tortor at. Felis imperdiet proin fermentum leo vel orci porta non. Morbi non arcu risus quis varius quam.',
                               'id nibh tortor id aliquet' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                           ]
                       ],
                       // bugs and reports
                       [
                           'catId' => 3,
                           'name' => 'bugs',
                           'questionsAnswers' => [
                               'ultrices mi tempus imperdiet nulla malesuada pellentesque elit ' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque diam volutpat commodo sed egestas egestas. Felis eget velit aliquet sagittis id.',
                               'pretium fusce id velit' => 'porta nibh venenatis cras sed felis eget velit aliquet sagittis id consectetur',
                               'eget velit aliquet sagittis id consectetur purus ut' => 'pretium fusce id velit',
                               'ut lectus arcu' => 'pretium fusce id velit'
                           ]
                       ]
                   ];
                @endphp
                @foreach($faqs as $cat)
                <section id="{{$cat['name'] . '-questions'}}" class="{{"questions " . $cat['name'] . ($cat['name'] == 'general' ? " active" : '')}}">
                    @foreach($cat['questionsAnswers'] as $q => $a)
                    <div class="question d-flex align-items-start mb-4">
                        <div class="icon mr-3"><i class="far fa-question-circle"></i></div>
                        <div class="q-a">
                            <h2 class="q">{{$q}}?</h2>
                            <p class="mb-0 a">{{$a}}</p>
                        </div>
                    </div>
                    @endforeach
                </section>
                @endforeach
            </div>
        </div>
    </main>
@endsection

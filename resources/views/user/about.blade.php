@php
    $siteConfig = \App\SiteConfig::all()->first()->toArray();
    $siteDesc = $siteConfig['description'];
    $siteMission= $siteConfig['mission'];
    $siteVision = $siteConfig['vision'];

    $Roles = \App\Role::where('is_available', '=', true)->get()->take(4);
    $Sponsers = \App\Sponser::where('is_available', '=', true)->get()->take(6);
    $Reviews = \App\Review::where('is_available', '=', true)->get()->take(8);


@endphp


@extends('user.common.layout')

@section('title')
    About us
@endsection

@section('content')
    <!-- header -->
    <header class="main-header about-header">
        <div class="container">
            <h1>About Us</h1>
            <h3>{{$siteDesc}}.</h3>
        </div>
    </header>
    <!-- breadcrumb -->
    <nav class="breadcrumb-nav mt-5 mb-5" aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">About Us</li>
            </ol>
        </div>
    </nav>
    <!-- main content -->
    <main class='about-us pb-5'>
        <div class="container">
            <!-- items -->
            <div class="items-wrapper mb-5">
                <div class="row">
                    <!-- item -->
                    @foreach($Roles as $role)
                        <div class="col-lg-3 col-md-6 mb-lg-0 mb-5">
                        <div class="item manager">
                            <!-- image -->
                            <div class="image">
                                <img class="radius-top" src="{{asset('assets/images/about/manager.jpg')}}" alt="">
                            </div>
                            <!-- Title -->
                            <h2 class="title h4 text-center m-0 py-3">{{$role->name}}</h2>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- anonymous section -->
{{--            <section class="anonymous-section">--}}
{{--                <h2 class="text-center">WHAT IS 'MARKET' !!</h2>--}}

{{--                <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aenean pharetra magna ac placerat vestibulum lectus mauris. Metus aliquam eleifend mi in nulla posuere. Ac tincidunt vitae semper quis lectus. Diam vel quam elementum pulvinar etiam non quam. Sapien pellentesque habitant morbi tristique senectus et netus. Et malesuada fames ac turpis egestas integer eget aliquet. Feugiat in ante metus dictum at tempor commodo ullamcorper. Eu turpis egestas pretium aenean. </p>--}}
{{--            </section>--}}
{{--            <!-- anonymous section -->--}}
{{--            <section class="anonymous-section">--}}
{{--                <h2 class="text-center">Who we are !</h2>--}}
{{--                <div class="row">--}}
{{--                    <!-- text -->--}}
{{--                    <div class="col-lg-6 mb-lg-0 mb-5">--}}
{{--                        <div class="text">--}}
{{--                            <p class="lead mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aenean pharetra magna ac placerat vestibulum lectus mauris. Metus aliquam eleifend mi in nulla posuere. Ac tincidunt vitae semper quis lectus. Diam vel quam elementum pulvinar etiam non quam. Sapien pellentesque habitant morbi tristique senectus et netus. Et malesuada fames ac turpis egestas integer eget aliquet. Feugiat in ante metus dictum at tempor commodo ullamcorper. Eu turpis egestas pretium aenean. Adipiscing bibendum est ultricies integer quis auctor elit.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- image -->--}}
{{--                    <div class="col-lg-6">--}}
{{--                        <div class="image"><img class="w-100" src="{{asset('assets/images/about/illustration.png')}}" alt="" width="200px" height="350px"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}
            <!-- anonymous section -->
            <section class="anonymous-section">
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-6 mb-lg-0 mb-5">
                        <div>
                            <h2>OUR VISION</h2>
                            <p class="lead mb-0">{{$siteVision}}</p></div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-6">
                        <div>
                            <h2>OUR MISSION</h2>
                            <p class="lead mb-0">{{$siteMission}}</p>
                        </div>
                    </div>
                </div>
            </section>

            <!-- anonymous section -->
            <section class="anonymous-section">
                <h2 class="text-center">What We Provide ?</h2>
                <div class="row">
                    <!-- column -->
                    <div class="col-lg-4 mb-lg-0 mb-5">
                        <div class="wrapper">
                            <h2>Best Prices &amp; Offers</h2>

                            <div class="wrapper d-md-flex">
                                <!-- image -->
                                <div class="image mr-3 mb-md-0 mb-3">
                                    <i class="fas fa-shopping-cart fa-3x" style="color:#608ba6;"></i>                                </div>
                                <!-- text -->
                                <div class="text">
                                    <p class="lead mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-4 mb-lg-0 mb-5">
                        <div class="wrapper">
                            <h2>Easy Returns</h2>

                            <div class="wrapper d-md-flex">
                                <!-- image -->
                                <div class="image mr-3 mb-md-0 mb-3">
                                    <i class="fas fa-exchange-alt fa-3x" style="color:#608ba6;"></i>
                                </div>
                                <!-- text -->
                                <div class="text">
                                    <p class="lead mb-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-4 mb-lg-0 mb-5">
                        <div class="wrapper">
                            <h2>Great Deals Discount </h2>

                            <div class="wrapper d-md-flex">
                                <!-- image -->
                                <div class="image mr-3 mb-md-0 mb-3">
                                    <i class="fas fa-tag fa-3x" style="color:#608ba6;"></i>
                                </div>
                                <!-- text -->
                                <div class="text">
                                    <p class="lead mb-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- anonymous section -->
            <section class="anonymous-section">
                <h2 class="text-center"> SPONSORS </h2>

                <div class="row">
                    <!-- image -->
                    @foreach($Sponsers as $Sponser)
                    <div class="col-md-4 mb-4">
                        <div class="image">
                            <img class="w-100" src="{{$Sponser->path}}" alt="" width="80" height="200">
                        </div>
                    </div>
               @endforeach
                </div>
            </section>
            <!-- anonymous section -->
            <section class="anonymous-section">
                <h2 class="text-md-center">Our Reviews </h2>

                <div class="row">
                    <!-- item -->
                    @foreach($Reviews as $Review)
                    <div class="col-lg-3 col-md-6 mb-lg-0 mb-5">
                        <div class="item" style="padding: 2em">
                            <!-- description -->
                            <p class="description text-center mt-3 mb-0">{{$Review->review}}</p>
                        </div>
                    </div>
                @endforeach
                </div>
            </section>

        </div>
    </main>
@endsection

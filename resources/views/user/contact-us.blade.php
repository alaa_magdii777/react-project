@php
    $siteConfig = \App\SiteConfig::all()->first()->toArray();
    $phoneCode = $siteConfig['phone_code'];
    $phone1 = $phoneCode . $siteConfig['phone_one'] ;
     $phone2= $phoneCode . $siteConfig['phone_two'] ;
     $phone3= $phoneCode . $siteConfig['phone_three'];
    $address = $siteConfig['address'];
    $fax = $siteConfig['fax'];
    $lat = $siteConfig['lat'];
    $long = $siteConfig['long'];
@endphp


@extends('user.common.layout')

@section('title')
    Contact Us
@endsection
@section('content')
    <!-- header -->
    <header class="main-header about-header">
        <div class="container">
            <h1>Contact Us</h1>
            <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas
                accumsan lacus vel facilisis.</p>
        </div>
    </header>

    <!-- breadcrumb -->
    <nav class="breadcrumb-nav mt-3" aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
        </div>
    </nav>

    <!-- maun content -->
    <main class='contact-us'>
        <div class="container">
            <!-- map -->
            <section class="map">
                <iframe class="w-100"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13673.85366520079!2d31.36574148969448!3d31.041196241327384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f79dd3586ece8b%3A0x8a183606fd57cbb2!2sThe+Olympic+Village!5e0!3m2!1sen!2seg!4v1565045792920!5m2!1sen!2seg"
                        height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </section>
        </div>

        <!-- contact info -->
        <section class="contact-info">
            <div class="container">
                <div class="row">
                    <!-- info -->
                    <div class="col-lg-6 mb-lg-0 px-0">
                        <section class="info h-100 w-100">
                            <h2 class="mb-5">Contact Information</h2>
                            <div class="text mb-4">
                                <h3 class="h4 mb-3">Adress (1)</h3>
                                <p>    {{$address}}
                                </p>
                            </div>
                            <div class="text mb-4">
                                <h3 class="h4 mb-3">Adress (2)</h3>
                                <p>{{$address}}</p>
                            </div>
                            <div class="text">
                                <h3 class="h4 mb-3">Telephones and Faxes</h3>
                                <ul class="list-unstyled list mb-0">
                                    <li class="mb-2"><i class="fas fa-phone-volume mr-2"></i>{{$phone1}}</li>
                                    <li class="mb-2"><i class="fas fa-fax mr-2"></i>{{$phone2}}</li>
                                    <li><i class="fas fa-phone-volume mr-2"></i>{{$phone3}}</li>
                                </ul>
                            </div>
                        </section>
                    </div>
                    <!-- contact-form -->
                    <div class="col-lg-6">


                        @include('user.common.errors')


                        <form action="" class="contact-form" method="POST">
                            @csrf
                            <h2 class="mb-5">Your Feedback</h2>
                            <p class="input-field m-0 mb-3 d-sm-table d-block">
                                <label class="mb-3 d-sm-table-cell d-block" for="full-name">Name*</label>
                                <input type="text" name="name" id="name">
                            </p>
                            <p class="input-field m-0 mb-3 d-sm-table d-block">
                                <label class="mb-3 d-sm-table-cell d-block" for="email">Email*</label>
                                <input type="email" name="email" id="email">
                            </p>
                            <p class="input-field m-0 mb-3 d-sm-table d-block">
                                <label class="mb-3 d-sm-table-cell d-block" for="phone">Phone</label>
                                <input type="text" name="phone" id="phone">
                            </p>
                            <p class="input-field m-0 d-sm-table d-block">
                                <label class="mb-3 d-sm-table-cell d-block" for="feedback">Feedback*</label>
                                <textarea type="text" name="feedback" id="feedback"> </textarea>
                            </p>
                            <div class="button-wrapper text-sm-right text-center">
                                <button class="mt-3 p-2" type="submit">
                                    Send
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>




@endsection

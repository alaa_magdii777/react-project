@php

    $products = \App\Product::all()
    //->where('is_available', '=', true)
    //->where('is_new', '=', true)
    ->take(10)
    ->toArray();

   //dd($products);
      for($i=1;$i<10;$i++)
      $allKeywords= $products[$i]['keywords'];
      $Keywords = explode(',' , $allKeywords  );



@endphp


@extends('user.common.layout')

@section('title')
    Market
@endsection

@section('content')
    <!-- header -->

    <header class="main-header home-header">
        <div class="bd-example">

            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-interval="7000">

                <ol class="carousel-indicators">
                    @foreach($data['availSlids'] as $k => $availSlid)
                        <li data-target="#carouselExampleCaptions" data-slide-to="{{$k}}"
                            class="{{$k == 0 ? "active": ""}}"></li>
                    @endforeach
                </ol>

                <div class="carousel-inner">
                    @foreach($data['availSlids'] as $k => $availSlid)
                        <div class="carousel-item {{$k == 0 ? "active": ""}}">
                            <img src="{{$availSlid->path}}" class="d-block w-100" alt="">
                            <div class="carousel-caption d-none d-md-block">
                                <h1 class="h1 mb-3">{{$availSlid->name}}</h1>
                                <p>{{$availSlid->description}}</p>
                               
                            </div>
                        </div>
                    @endforeach
                </div>

                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

        </div>
    </header>


    <!-- main content -->
    <main class="home">
        <div class="container">
            <div class="row">
                <!-- categories controls-->
                <div class="col-lg-3 mb-lg-0 mb-5">
                    <aside>
                        <!-- categories list -->
                        <section class='categories-list mb-5 text-center'>
                            <h3>Categories</h3>
                            <hr>
                            <ul class="list-unstyled px-4 mb-0">
                                @foreach($data['availCats'] as $availCat)
                                    <li>
                                        <a href="/category/{{$availCat->id}}">
                                            {{$availCat->name}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </section>
                        <!-- popular tags -->
                        <section class='popular-tags'>
                            <h3 class='text-center'>Popular Tags</h3>
                            <hr>
                            <ul class="list-unstyled px-4 d-flex mb-0">

                                @foreach($Keywords as $Keyword)
                                <li class="mb-2"><a class="d-inline-block" href="#">#{{$Keyword}}</a></li>
                                @endforeach
                            </ul>
                        </section>
                    </aside>
                </div>
                <!-- products -->
                <div class="col-lg-9">
                    <section class="products">
                        <h2 class="mb-5 h3 border-heading">Top Sales</h2>
                        <!-- products -->
                        <div class="row">
                            @foreach($data['topSales'] as $topSale)
                                @include('user.common.top-sale-card')
                            @endforeach
                        </div>
                        <div class="row">
                            <button class="btn btn-circle btn-lg mx-auto"
                                    style="background-color:#a7c6d9; color: white; ">See More
                            </button>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </main>
@endsection

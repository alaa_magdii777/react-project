@php
    $itemsNumber = \App\UnconfirmedOrder::select(DB::raw('count(id) AS c'))
            ->where('user_id', '=', \Illuminate\Support\Facades\Auth::id())->first()['c'];
    $items = $unconfirmedOrder = \App\UnconfirmedOrder::where('user_id', '=', Auth::id())
    ->where('product_id', '!=', null)->get()->toArray();
@endphp
@extends('user.common.layout')

@section('content')
    <main class="cart py-5">
        <div class="container">
            {{-- @if($itemsNumber <= 0) --}}
                <div id="empty-cart" class="empty-cart text-center">
                    <h1>Your cart is empty</h1>
                    <p class="mb-5">Go to <a class="tertiary" href="/">shopping </a>now</p>
                    <img class="w-100" src="assets/images/cart/empty-cart.svg" alt="">
                </div>
            {{-- @else --}}
            <div class="items-wrapper" id="items-wrapper">
                <table id="cart-table"
                        data-toolbar="#toolbar"
                        data-search="true"
                        data-show-refresh="true"
                        data-show-toggle="true"
                        data-show-fullscreen="true"
                        data-show-columns="true"
                        data-show-columns-toggle-all="true"
                        data-detail-view="true"
                        data-show-export="true"
                        data-click-to-select="true"
                        data-detail-formatter="detailFormatter"
                        data-minimum-count-columns="2"
                        data-show-pagination-switch="true"
                        data-pagination="true"
                        data-id-field="id"
                        data-page-list="[10, 25, 50, 100, all]"
                        data-show-footer="true"
                        data-side-pagination="server"
                        data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data"
                        data-response-handler="responseHandler">
                <thead class="upper">
                <th>selected items</th>
                <th>quantity</th>
                <th>unit price</th>
                <th>edit</th>
                <th>subtotal</th>
                </thead>
            <tbody id="cartBody"></tbody>
        </table>
        <!-- cart controls -->
        <form class="cart-controls" action="" method="POST">
            <input class="mr-3" type="checkbox" name='select-all' id="select-all">
            <label for="select-all">Select all</label>
            <button class="secondary" type="button"><a href="">Delete selected</a></button>
        </form>
        </div>
            {{-- @endif --}}
        </div>
    </main>


@endsection

@section('more-script')
    <script>
        function generateCartItem(id, name, price, discount, max, quantity, imageUrl) {
            let totalPrice = quantity * price * (1.0 - discount / 100.0);
            totalPrice = Math.ceil(totalPrice * 100) / 100;

            let html = '<tr>' +
                '<td class="product-info d-flex">' +
                '<input class="check mr-3" type="checkbox" name="cart-item[]" id="item-$productId">' +
                '<div class="thumbnail mr-3">' +
                '<img class="w-100" src="' + imageUrl + '" alt="">' +
                '</div>' +
                '<ul class="info list-unstyled m-0 p-0">' +
                '<li class="name"><a href="product-' + id + '">' + name + '</a></li>' +
                '<li class="size">36</li>' +
                '<li class="color">Black</li></ul></td>' +
                '<td class="product-quantity"><div class="wrapper d-flex">' +
                '<span class="control decrease mr-3" id="decrease-item-' + id + '">-</span>' +
                '<input class="quantity mr-3" type="text" value="' + quantity + '" ' +
                'name="quantity" id="item-quantity-' + id + '"' +
                ' data-max="' + max + '"> <span class="control increase" id="increase-item-' + id + '">+</span>' +
                '</div></td><td class="product-price text-center"><span class="price" ' +
                'id="item-price-' + id + '" data-price="' + price + '"' +
                ' data-discount="' + discount + '">' + price + '</span><span class="currency">$</span>' +
                '</td><td class="product-edit text-center">' +
                '<a href="delete-cart-item-' + id + '" class="delete" id="delete-item-' + id + '">' +
                '<i class="fas fa-trash-alt"></i></a>' +
                '</td><td class="product-subtotal text-center">' +
                '<span class="subtotal" id="item-subtotal-' + id + '">' + totalPrice + '</span>' +
                '<span class="currency">$</span>' +
                '</td></tr>';
            return html;
        }

        // $(document).ready(function () {

        //     let cart = localStorage.getItem('cart');
        //     if (cart == null) {
        //         cart = [];
        //     } else {
        //         cart = JSON.parse(cart);
        //     }

        //     console.log(cart);

        //     let allItemsHTML = "";
        //     for (let i = 0; i < cart.length; i++) {
        //         let itemHTML = generateCartItem(cart[i].id, cart[i].name, cart[i].price, cart[i].discount,
        //             cart[i].max, cart[i].amount, cart[i].imageUrl);
        //         allItemsHTML += itemHTML;
        //     }
        //     $('#cartBody').append(allItemsHTML);

        // });
        </script>
@endsection

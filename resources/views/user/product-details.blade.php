
@extends('user.common.layout')
<!-- header -->
@section('title')

@endsection
@section('content')

<header class="main-header product-details-header">
    <div class="container">
        <div class="row">
            <div class="col-md-2 order-md-last order-first mb-5 mb-md-0">
                <div class="availability float-md-right float-none">
                    <!-- <img class='rounded-circle' src="assets/images/product-details/available.png" alt=""> -->
                    Available
                </div>
            </div>
            <div class="col-md-10">
                <h1> {{$data['productName']}}</h1>
                <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
            </div>
        </div>
    </div>
</header>

<!-- maun content -->
<main class='product-details-page'>
    <div class="container">
        <!-- product overview -->
        <section class="product-overview mb-5">
            <div class="row">
                <!-- info -->
                <div class="col-lg-7">

                    <div class="info">
                        <h2 class="px-4">{{$data['productName']}}</h2>
                        <hr>
                        <ul class="list-unstyled px-4">
                            <li class="mb-2">Price: <span>{{$data['productPrice']}}</span></li>
                            <li class="mb-2">Discount: <span>{{$data['productDiscount']}}</span></li>
                            <li class="mb-2">Description: <span>{{$data['productDesc']}}</span></li>
                            <li class="mb-2">Priceafter Disc%:  <span>555</span></li>
                            <li class="mb-2">Age Categories: <span>{{$data['productWords']}}</span></li>
                            <li>Categories: <span><a href="#">Category 01</a>, <a href="#">Category 02</a></span></li>
                        </ul>
                    </div>
                </div>
                <!-- image -->
                <div class="col-lg-5 mb-lg-0 mb-5 order-lg-last order-first">
                    <div class="image h-100 text-center text-lg-right">
                        <img class="h-100" src="{{asset('/assets/images/p-02-01.jpg')}} " alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- details -->
        <section class='product-details'>
            <!-- specific -->
            <section class="details specific mb-4">
                <h3 class="d-flex align-items-center px-4 h4 mb-0"><span>Other Specific Details</span> <i id="expand-specificDetails" class="fas fa-caret-right expand-details ml-auto"></i></h3>
                <div class="dropdown-wrapper px-4" id="specificDetails">
                    <hr>
                    <p>Sagittis nisl rhoncus mattis rhoncus urna neque viverra justo. Habitant morbi tristique senectus et netus et malesuada. Dignissim convallis aenean et tortor at risus viverra. Tellus molestie nunc non blandit massa enim nec dui. Morbi tristique senectus et netus et. Nisl condimentum id venenatis a condimentum vitae. Commodo odio aenean sed adipiscing diam. Velit dignissim sodales ut eu sem. Tincidunt lobortis feugiat vivamus at augue eget arcu dictum varius. Ipsum dolor sit amet consectetur adipiscing. Turpis massa sed elementum tempus egestas. Neque aliquam vestibulum morbi blandit cursus risus at ultrices. Molestie at elementum eu facilisis sed. Non quam lacus suspendisse faucibus interdum posuere. Viverra orci sagittis eu volutpat odio facilisis. Mi tempus imperdiet nulla malesuada.</p>
                    <p>Velit euismod in pellentesque massa. Aenean sed adipiscing diam donec adipiscing. Risus quis varius quam quisque id diam vel quam. Quis commodo odio aenean sed adipiscing diam donec adipiscing tristique.</p>
                </div>
            </section>
            <!-- description -->
            <section class="details description mb-4">
                <h3 class="d-flex align-items-center px-4 h4 mb-0"><span>Product Description</span> <i id="expand-description" class="fas fa-caret-right expand-details ml-auto"></i></h3>
                <div class="dropdown-wrapper px-4" id="description">
                    <hr>
                    <p>  {{$data['productDesc']}}</p>
                </div>
            </section>
            <!-- description -->
            <section class="details images mb-5">
                <h3 class="d-flex align-items-center px-4 h4 mb-0"><span>More Images on the Product</span> <i id="expand-productImages" class="fas fa-caret-right expand-details ml-auto"></i></h3>
                <div class="dropdown-wrapper px-4" id="productImages">
                    <hr>
                    <div class="images-wrapper d-md-flex">
                        <!-- row -->
                        <div class="image mb-3">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <!-- row -->
                        <div class="image mb-3 mb-md-0">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3 mb-md-0">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3 mb-md-0">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image mb-3 mb-md-0">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                        <div class="image">
                            <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt="">
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <!-- Recommended Products -->
{{--        <section class="recommended-products">--}}
{{--            <h3 class="mb-5 border-heading">Recommended Products</h3>--}}
{{--            <!-- products -->--}}
{{--            <div class="row">--}}
{{--                @for($i=0;$i<=2;$i++)--}}
{{--                    @include('user.common.reco-card')--}}
{{--                    @endfor--}}
{{--            </div>--}}
{{--        </section>--}}
    </div>
</main>
@endsection

{{--@extends('common.layout')--}}

{{--@section('title')--}}
{{--    Sign In--}}
{{--@endsection--}}

{{--@section('content')--}}
@include('user.common.head')
    <main class="member signin">
        <div class="container">
            <form class=" clearfix" action="" method="POST">
                @include('user.common.errors')
                @csrf
                <figure><img src="assets/images/signin-image.jpg" alt="sing up image" style="    margin-left: 80px;
"></figure>
                <div class="form-group">


                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn primary-button float-md-right">Signin</button>
                <div class="clearfix mb-3"></div>
                <p class="mb-0 extra">
                    <small>Don't have an acount? <a class="tertiary" href="/register">Register</a> Now</small>
                </p>
            </form>
        </div>

    </main>
@include('user.common.footer')

{{--@endsection--}}
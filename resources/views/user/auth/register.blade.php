@php
    $genders = \App\Gender::all()->toArray();
    $cities = \App\City::all()->toArray();


@endphp

@include('user.common.head')

<main class="member register">
    <div class="container">

        <form class="clearfix" action="" method="POST">
            @csrf
            @include('user.common.errors')

            <div class="signup-image">
                <figure><img src="assets/images/signup-image.jpg" alt="sing up image" style="margin-left: 80px;">
                </figure>
            </div>


            <div class="form-group">
                <label for="firstname">firstname</label>
                <input type="text" class="form-control" id="firstname" name="firstname"
                       placeholder="Enter firstname">
            </div>
            <div class="form-group">
                <label for="lastname">lastname</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter lastname">
            </div>
            <div class="form-group">
                <label for="gender">Gender</label>
                <select type="text" class="form-control" id="gender" name="gender">
                    @foreach($genders as $gender)
                        <option value="{{$gender['id']}}"> {{$gender['name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="email">email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="email">
            </div>
            <div class="form-group">
                <label for="phone">phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="phone">
            </div>
            <div class="form-group">
                <label for="birthdate">Birth Date</label>
                <input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="birthdate">
            </div>
            <div class="form-group">
                <label for="address">address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="address">
            </div>
            <div class="form-group">
                <label for="City">City</label>
                <select type="text" class="form-control" id="city"  name="city" placeholder="city">

                    @foreach($cities as $city)
                        <option value=" {{$city['id']}}"> {{$city['name']}}</option>
                    @endforeach

                </select>
            </div>

            <button type="submit" class="btn primary-button float-md-right">Register</button>
        </form>
    </div>
</main>

@include('user.common.footer')
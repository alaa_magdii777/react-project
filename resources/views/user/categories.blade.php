@extends('user.common.layout')
<!-- header -->
@section('title')
   Categories
@endsection
@section('content')
<!-- header -->
<header class="main-header categories-header">
    <div class="container">
        <h1>Categories</h1>
        <p class='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
    </div>
</header>

<!-- main content -->
<main class="categories-page">
    <div class="container">
        <div class="row">
            <!-- categories controls-->
            <div class="col-lg-3 mb-lg-0 mb-5">
                <section class="categories-controls">
                    <!-- sort -->
                    <form>
                        <section class="form-group sort">
                            <h2 class="h4 text-center">Sorting</h2>
                            <hr>
                            <p class="input-field m-0 px-4">
                                <input type="radio" name="sort" id="filter-name-desc">
                                <i class="fas fa-long-arrow-alt-down"></i>
                                <label for="filter-name-desc">Name Desc</label>
                            </p>
                            <p class="input-field m-0 px-4">
                                <input type="radio" name="sort" id="filter-name-asc">
                                <i class="fas fa-long-arrow-alt-up"></i>
                                <label for="filter-name-asc">Name Asc</label>
                            </p>
                        </section>
                        <hr>
                        <section class="form-group filter mb-0">
                            <h2 class="h4 text-center">Filtering</h2>
                            <hr>
                            <!-- field -->
                            <p class="input-field m-0 px-4">
                                <input type="checkbox" name="filer[]" id="all-ages">
                                <label for="all-ages">All Ages</label>
                            </p>
                            <!-- field -->
                            <p class="input-field m-0 px-4">
                                <input type="checkbox" name="filer[]" id="kids">
                                <label for="kids">Kids</label>
                            </p>
                            <!-- field -->
                            <p class="input-field m-0 px-4">
                                <input type="checkbox" name="filer[]" id="men">
                                <label for="men">Men</label>
                            </p>
                            <!-- field -->
                            <p class="input-field m-0 px-4">
                                <input type="checkbox" name="filer[]" id="women">
                                <label for="women" class="mb-0">Wemen</label>
                            </p>
                        </section>
                    </form>
                </section>
            </div>
            <!-- categories -->
            <div class="col-lg-9">
                <section class="categories">
                    <!-- search form -->
                    <form action="" class="search-category-form">
                        <p class="input-field d-flex">
                            <button style="height: 45px" type="submit"><i style="padding-left: 12px;padding-top: 5px" class="fas fa-search"></i></button>
                            <input style="height: 45px" type="text" id="search-category" name="search-category" placeholder='search'>
                        </p>
                    </form>
                    <!-- categories -->
                    <div class="row">

                   <!--categories-->
                        @foreach($data['availCats'] as $availCat)
                            @include('user.common.category-card')
                        @endforeach
                    </div>
                    <div class="row">
                        <button class="btn btn-circle btn-lg mx-auto" style="background-color:#a7c6d9; color: white; ">See More</button>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
@endsection

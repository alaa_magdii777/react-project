import * as ActionTypes from './ActionTypes';

const initialState = {
    userInfo: {
        fullName: "",
        imageUrl: "",
        apiToken: "",
    },
    login: {
        data: {
            username: "",
            password: "",
        },
        errors: [],
        isLoading: false,
    }
};

const Reducers = (state = initialState, action) => {
    console.log(action.type);
    if (action.type === ActionTypes.UPDATE_LOGIN_DATA) {
        const login = {...state.login};
        const data = {...login.data};
        if (action.payload.field == "username") {
            data.username = action.payload.newValue;
        } else if (action.payload.field == "password") {
            data.password = action.payload.newValue;
        }
        login.data = data;
        // state.login = login;
        // By Ref x
        // By Value.
        return {...state, login: login};
    } else if (action.type === ActionTypes.VALIDATE_LOGIN_DATA) {
        let errors = [];
        if (state.login.data.username.length < 3) {
            errors.push("Username must be more than or equal 3 characters.");
        } else if (state.login.data.username.length > 75) {
            errors.push("Username must be less than or equal 75 characters.");
        }

        if (state.login.data.password.length < 3) {
            errors.push("Password must be more than or equal 3 characters.");
        } else if (state.login.data.password.length > 100) {
            errors.push("Username must be less than or equal 100 characters.");
        }
        const login = {...state.login};
        login.errors = errors;
        return {...state, login: login};
    } else if (action.type === ActionTypes.ADD_LOGIN_ERRORS) {
        const login = {...state.login};
        login.errors = action.payload;
        return {...state, login: login};
    } else if (action.type === ActionTypes.UPDATE_USER_INFO) {
        const data = action.payload;
        const userInfo = {
            apiToken: data.apiToken,
            fullName: data.fullName,
            imageUrl: data.imageUrl,
        };
        return {...state, userInfo: userInfo};
    }
    return state;
};

export default Reducers;
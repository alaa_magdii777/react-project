import * as ActionTypes from './ActionTypes';
import axios from "axios";
import {API_PREFIX} from "../configs";

// Any function => Object
// Object => {type: "", payload: ""}

export const updateLoginData = (field, newValue) => {
    return {
        type: ActionTypes.UPDATE_LOGIN_DATA,
        payload: {
            field: field,
            newValue: newValue,
        },
    }
};

export const validateLoginData = () => {
    return {
        type: ActionTypes.VALIDATE_LOGIN_DATA,
        payload: null,
    }
};

export const addLoginErrors = (errors) => {
    return {
        type: ActionTypes.ADD_LOGIN_ERRORS,
        payload: errors,
    }
};

export const updateUserInfo = (userInfo) => {
    return {
        type: ActionTypes.UPDATE_USER_INFO,
        payload: userInfo,
    }
};


// =================================================

export const doAPILogin = (loginData) => (dispatch) => {
    axios.post(API_PREFIX + 'login', {...loginData}
    ).then((response) => {
        const data = response.data;
        if (data.failed) {
            dispatch(addLoginErrors(data.errors));
        } else {
            dispatch(updateUserInfo(data.data));
        }
    });
};
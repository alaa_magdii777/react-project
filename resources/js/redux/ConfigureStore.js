import {combineReducers, createStore, applyMiddleware} from 'redux';
import Reducers from "./Reducers";
import thunk from 'redux-thunk';

const store = combineReducers({
    reducer: Reducers
});

// export default store;
export default createStore(store, applyMiddleware(thunk));
import React from 'react';
import {withRouter} from 'react-router';
import TopNavBar from "../common/TopNavBar";
import NavBar from "../common/NavBar";
import Footer from "../common/Footer";
import Product from "../common/Product";


class Recommendation extends React.Component{
    constructor(){
        super();
        this.state = {
            products: [
                {id: 1, name: "ASD 1", price: 2000, discount: 10, amount: 500, isNew: true,isSoldOut :false,
                    categoryName: "Clothes", description: "LOREM LOREM LOREM LOREM LOREM",
                    imageUrl: "/assets/images/kitchen/p-07-01.jpg"},
                {id: 2, name: "ASD 2", price: 3453, discount: 54, amount: 453, isNew: false,isSoldOut :true,
                    categoryName: "Clothes", description: "LOREM LOREM LOREM LOREM LOREM",
                    imageUrl: "/assets/images/kitchen/p-02-01.jpg"},
                {id: 3, name: "ASD 3", price: 4524, discount: 43, amount: 45345, isNew: false,isSoldOut :false,
                    categoryName: "Clothes", description: "LOREM LOREM LOREM LOREM LOREM",
                    imageUrl: "/assets/images/kitchen/p-01-01.jpg"},
                {id: 4, name: "ASD 4", price: 2515, discount: 30, amount: 6518, isNew: true,isSoldOut :true,
                    categoryName: "Clothes", description: "LOREM LOREM LOREM LOREM LOREM",
                    imageUrl: "/assets/images/kitchen/p-04-01.jpg"}
            ],
        };
    }
    _addToCartHandler(productID) {
        alert(productID);
    }
    render() {
        let productsJSX = this.state.products.map((item, index) => {
            return (<Product cartHandler={(productID) => {
                this._addToCartHandler(productID);
            }} key={item.id} {...item}/>);
        });
        return(
            <div>
                <TopNavBar/>
                <NavBar/>
                <header className="main-header categories-header">
                    <div className="container">
                        <h1>Recommendations</h1>
                        <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae
                            congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
                    </div>
                </header>

                
                <main className="categories-page category-details">
                    <div className="container">
                        <div className="row">
                            
                            <div className="col-lg-3 mb-lg-0 mb-5">
                                <section className="categories-controls">
                                   
                                    <form className="sort">
                                        <section className="form-group sort">
                                            <h2 className="h4 text-center">Sorting</h2>
                                            <hr/>
                                                
                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-name-desc"/>
                                                        <i className="fa fa-long-arrow-alt-down"></i>
                                                        <label htmlFor="filter-name-desc">Name Desc</label>
                                                </p>
                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-name-asc"/>
                                                        <i className="fa fa-long-arrow-alt-up"></i>
                                                        <label htmlFor="filter-name-asc">Name Asc</label>
                                                </p>

                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-price-desc"/>
                                                        <i className="fa fa-long-arrow-alt-down"></i>
                                                        <label htmlFor="filter-price-desc">Price Desc</label>
                                                </p>
                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-price-asc"/>
                                                        <i className="fa fa-long-arrow-alt-up"></i>
                                                        <label htmlFor="filter-price-desc">Price Asc</label>
                                                </p>

                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-discount-desc"/>
                                                        <i className="fa fa-long-arrow-alt-down"></i>
                                                        <label htmlFor="filter-discount-desc">Discount Desc</label>
                                                </p>
                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-discount-asc"/>
                                                        <i className="fa fa-long-arrow-alt-up"></i>
                                                        <label htmlFor="filter-discount-desc">Discount Asc</label>
                                                </p>

                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-sales-desc"/>
                                                        <i className="fa fa-long-arrow-alt-down"></i>
                                                        <label htmlFor="filter-sales-desc">Sales Desc</label>
                                                </p>
                                                <p className="input-field m-0 px-4">
                                                    <input type="radio" name="sort" id="filter-sales-asc"/>
                                                        <i className="fa fa-long-arrow-alt-up"></i>
                                                        <label htmlFor="filter-sales-desc">Sales Asc</label>
                                                </p>
                                        </section>
                                        <hr/>
                                            <section className="form-group filter mb-0">
                                                <h2 className="h4 text-center">Filtering</h2>
                                                <hr/>
                                                    <section className="category-filter">


                                                            <p className="input-field d-flex align-items-baseline m-0 px-4">
                                                            <input type="checkbox" name="filer[]" id="filter-{{$category['name']}}"
                                                                   data-toggle=".{{$category['name']}}"/>
                                                            <label className="d-flex align-items-center w-100 ml-2 mb-0" htmlFor="all-ages">
                                                                <span>categoryName</span>
                                                            </label>
                                                            </p>


                                                            </section>
                                                            <hr/>
                                                            <section className="state-filter">

                                                            <p className="input-field d-flex align-items-baseline m-0 px-4">
                                                            <input type="checkbox" name="state[]" id="newOnly" data-toggle=".new"/>
                                                            <label className="d-flex align-items-center w-100 ml-2 mb-0" htmlFor="newOnly">
                                                                <span>New Only</span>
                                                                </label>
                                                            </p>

                                                            <p className="input-field d-flex align-items-baseline m-0 px-4">
                                                            <input type="checkbox" name="state[]" id="withDiscount" data-toggle=".discount"/>
                                                            <label className="d-flex align-items-center w-100 ml-2 mb-0" htmlFor="withDiscount">
                                                                <span>With Discount</span> </label>
                                                            </p>
                                                            </section>
                                                            <hr/>
                                                            <section className="price-filter">

                                                            <div className="input-field mb-3 px-4">
                                                            <div className="wrapper d-flex align-items-center">
                                                            <label className="mr-auto mb-0" htmlFor="from-price">From Price</label>
                                                            <p className="d-inline-block display-value m-0" id="from-price-value">2000</p>
                                                            </div>
                                                            <input type="range" name="from-price" id="from-price" value="2000" min="0" max="5000"/>
                                                            </div>

                                                            <div className="input-field px-4">
                                                            <div className="wrapper d-flex align-items-center">
                                                            <label className="mr-auto mb-0" htmlFor="to-price">To Price</label>
                                                            <p className="d-inline-block m-0 display-value" id="to-price-value">4000</p>
                                                            </div>
                                                            <input type="range" name="to-price" id="to-price" value="4000" min="0" max="5000"/>
                                                            </div>
                                                            </section>
                                                            <hr/>
                                                            <section className="products-limit-number">

                                                            <p className="input-field m-0 px-4">
                                                            <label className="d-block" htmlFor="to-price">Numbet of Products per Page</label>
                                                            <select name="products-limit" id="products-limit">
                                                            <option value="5" default>5</option>
                                                            <option value="10">10</option>
                                                            <option value="20">20</option>
                                                            <option value="50">50</option>
                                                            <option value="100">100</option>
                                                            </select>
                                                            </p>
                                                            </section>
                                                            </section>
                                                            </form>
                                                            </section>
                                                            </div>

                                                            <div className="col-lg-9">
                                                            <section className="categories">

                                                            <form action="" className="search-category-form">
                                                            <p className="input-field d-flex">
                                                            <button type="submit" style={{height: 45}}>
                                                                <i style={{padding:12}} className=" fa fa-search"></i></button>
                                                            <input type="text" id="search-category"
                                                            style={{height: 45}} name="search-category" placeholder='search'/>
                                                            </p>
                                                            </form>

                                                            <div id="mix-container" className="row">
                                                                {productsJSX}
                                                            </div>
                                                            <div className="row">
                                                            <button className="btn btn-circle btn-lg mx-auto btn-danger" >
                                                                See More</button>
                                                            </div>
                                                            </section>


                                                            </div>
                                                            </div>
                                                            </div>
                                                            </main>
                <Footer/>

            </div>
            )

    }
}
export default withRouter(Recommendation) ;
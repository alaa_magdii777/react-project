import React from 'react';
import {withRouter} from 'react-router';
import TopNavBar from "../common/TopNavBar";
import NavBar from "../common/NavBar";
import Slider from "./Slider";
import Footer from "../common/Footer";
import Product from "../common/Product";
import axios from 'axios';

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            categories: [
                {id: 1, name: "Clothes"},
                {id: 2, name: "Food"},
                {id: 3, name: "Glasses"},
            ],
            tags: [ ],
            errors: [],
            products: [ ],
        };
    }

    // _fetchTags(){
    //     axios.get('http://localhost/api/products').then((response) => {
    //         this.setState({tags: [...response.data.data]});
    //
    //     }).catch((errors) => {
    //         this.setState({errors: errors});
    //     });
    // }
    _fetchProductData() {
        axios.get('http://localhost/api/products').then((response) => {
            this.setState({products: [...response.data.data]});
            console.log(response.data.data)

        }).catch((errors) => {
            this.setState({errors: errors});
        });

    };
    render() {
        let productsJSX = this.state.products.map((item, index) => {
            console.log(item);
            return (<Product key={item.id} {...item}/>);
        });

        // let tagsJSX = this.state.tags.map((item, index) => {
        //     return (<li className="mb-2 "> <a key={item.id} className="d-inline-block" href="#">#{item}</a></li>);
        // });

        // let catsJSX = this.state.categories.map((item, index) => {
        //     return (<li key={item.id + item.name}><a href="#">{item.name}</a></li>);
        // });



        return (

            <div onLoad={() => {
                this._fetchProductData();
                // this._fetchTags();
            }}>

                <TopNavBar/>
                <NavBar/>
                <Slider/>
                <main className="home">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 mb-lg-0 mb-5">
                                <aside>
                                    <section className='categories-list mb-5 text-center'>
                                        <h3>Categories</h3>
                                        <hr/>
                                        <ul className="list-unstyled px-4 mb-0 text-danger">
                                            {/*{catsJSX}*/}
                                        </ul>
                                    </section>
                                    <section className='popular-tags'>
                                        {/*TASK*/}
                                        <h3 className='text-center'>Popular Tags</h3>
                                        <hr/>
                                        <ul className="list-unstyled px-4 d-flex mb-0">
                                            {/*{tagsJSX}*/}
                                        </ul>
                                    </section>
                                </aside>
                            </div>
                            <div className="col-lg-9">
                                <section className="products">
                                    <h2 className="mb-5 h3 border-heading">Top Sales</h2>
                                    <div className="row">
                                        {productsJSX}
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </main>
                <Footer/>
            </div>
        );
    }
}

export default withRouter(Home);
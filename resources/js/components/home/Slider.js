import React from 'react';

class Slider extends React.Component {
    render() {
        return (
            <header className="main-header home-header">
                <div className="bd-example">
                    <div id="carouselExampleCaptions" className="carousel slide" data-ride="carousel"
                         data-interval="7000">
                        <ol className="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" className="active"></li>
                            {/*<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>*/}
                            {/*<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>*/}
                        </ol>
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img src="https://via.placeholder.com/1200x700/#62A718/FFFFFF"
                                     className="d-block w-100"
                                     alt=""/>
                                <div className="carousel-caption d-none d-md-block">
                                    <h1 className="h1 mb-3">Product Name</h1>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Eu consequat ac felis
                                        donec et odio pellentesque.
                                    </p>
                                    <button><a href="#">Shop Now</a></button>
                                </div>
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleCaptions" role="button"
                           data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleCaptions" role="button"
                           data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </header>
        );
    }
}

export default Slider;
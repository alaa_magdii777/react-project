
import React from 'react';
import TopNavBar from '../common/TopNavBar';
import NavBar from '../common/NavBar';
import Footer from '../common/Footer';




class PrivacyPolicy extends React.Component{
      
    constructor(){

        super();
        this.state={};
    }
    

    render(){

        return (

           <div>  
         
           <TopNavBar/>
           <NavBar/>

<header className="main-header privacy-policy-header">
    <div className="container">
        <h1>Privacy Policy</h1>
        <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
    </div>
</header>


<main className='privacy-policy'>
    <div className="container">
       
        <section className="privacy-sec intro">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">introduction to market privacy policy</h2>
                </div>
                
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>Our privacy policy describes the ways in which we collect, store, use and protect your personal information and it is important for you to review this privacy policy. By "personal information", we mean information that can be associated with a specific person and can be used to identify that person.</li>
                        <li>We do not consider anonymised information to constitute personal information, as it cannot be used to identify a specific person. We collect personal information from you when you use souq.com or its related websites and services ("Site") (including, without limitation, when you buy and sell items or when you telephone or email our customer support team). By providing us with your personal information, you expressly consent to us processing your personal information in accordance with the terms of our privacy policy.</li>
                        <li>We may amend our privacy policy at any time by posting a revised version on the Site. The revised version will be effective at the time we post it and, following such posting, your continued use of the Site will constitute your express consent to us continuing to process your personal information in accordance with the terms of our revised privacy policy.</li>
                        <li>We would encourage you to check the Site regularly for the announcement of any amendments to our privacy policy.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec topics">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">Our privacy policy covers the following topics</h2>
                </div>
               
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>our collection of your personal information</li>
                        <li>our use of your personal information</li>
                        <li>your use of your and other users’ personal information</li>
                        <li>accessing, reviewing and amending your personal information</li>
                        <li>third party website links</li>
                        <li>cookies</li>
                        <li>no spam or spoof emails</li>
                        <li>protecting your personal information</li>
                        <li>how you can contact us about privacy questions</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
       
        <section className="privacy-sec personal-info">
            <div className="row">
                
                <div className="col-md-4">
                    <h2 className="capital">Our collection of your personal information</h2>
                </div>
               
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>As part of your registration on or use of the Site, you will be asked to provide us with certain personal information, such as your name, shipping address, email address and/or telephone number and other similar information as well as some additional information about you such as your date of birth or other identifying information.</li>
                        <li>Additionally, in order for us to verify your identity, we may need to request from you valid proof of identification (e.g. in the form of a copy of your passport, resident’s visa or permit, national ID card and/or driver’s license).</li>
                        <li>We will also need to collect selected financial information from you, such as your credit card and/or bank account details.</li>
                        <li>You will need to input this financial information into the "My Account" section of the Site.</li>
                        <li>We use this financial information for billing purposes and for the fulfilment of your orders.</li>
                        <li>Following your registration on the Site, you should not post any personal information (including any financial information) anywhere on the Site other than on the My Account section of the Site.</li>
                        <li>Restricting the posting of personal information to the My Account section of the Site protects you from the possibility of fraud or identity theft. The posting by you of any personal information anywhere on the Site other than on the My Account section of the Site may lead to the suspension of your use of the Site.</li>
                        <li>We will collect transactional information based on your activities using the Site (such as buying and selling items).</li>
                        <li>Please note that we may use your Internet protocol (or IP) address (which is a unique number assigned to your computer server or your Internet service provider (or ISP)) to analyse user trends and improve the administration of the Site.</li>
                        <li>We may also collect information about your computer (for example, browser type) and navigation information (for example, the pages you visit on the Site) along with the times that you access the Site.</li>
                    </ol>
                    <p>Finally, we may collect additional information from or about you in other ways not specifically described here. For example, we may collect information related to your contact with our customer support team or store results when you respond to a survey. We may also collect feedback ratings and other comments relating to your use of the Site. Where we aggregate personal information for statistical purposes, such aggregated personal information shall be anonymized.</p>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec use-personal-info">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">Our use of your personal information</h2>
                </div>
               
                <div className="col-md-8">
                    <p>We share customer information only as described below and with Amazon.com, Inc. and its subsidiaries.</p>
                    <ol className="p-0 mb-0 pl-2">
                        <li>We use your personal information to provide services and customer support to you; to measure and improve our services to you; to prevent illegal activities and implement our user agreement with you ("User Agreement"); troubleshoot problems; collect fees; provide you with promotional emails and verify information you give us with third parties. For example, we may share some of the personal information you give us with banks or credit card authorisation, processing and verification services or with third parties for fraud screening purposes.</li>
                        <li>Though we make every effort to preserve your privacy, we may need to disclose your personal information to law enforcement agencies, government agencies or other third parties where we are compelled so to do by court order or similar legal procedure; where we are required to disclose your personal information to comply with law; where we are cooperating with an ongoing law enforcement investigation or where we have a good faith belief that our disclosure of your personal information is necessary to prevent physical harm or financial loss, to report suspected illegal activity or to investigate a possible violation of our User Agreement and other agreements; or protect the rights, property, or safety of Souq, our users or others.</li>
                        <li>In the event of a sale of Souq.com FZ-LLC, any of its affiliates and subsidiaries or any related business assets, your personal information may be disclosed to any potential purchaser for the purposes of the continued provision of the Site or otherwise in relation to any such sale. We may share your personal information with our other group companies for the purposes outlined in this privacy policy.</li>
                        <li>This contact information will be the personal information provided by you to us on registration and, as a result, should always be kept up-to-date.</li>
                        <li>Additionally, information relating to items you are buying or selling will be displayed on the Site. This information can include details of your user ID, feedback ratings and associated comments relating to your use of the Site. Otherwise, we will only disclose your personal information to a third party with your express permission.</li>
                        <li>We do not sell or rent any of your personal information to third parties in the normal course of doing business and will only share your personal information with third parties in accordance with this privacy policy.</li>
                        <li>By registering on or using the Site, you give us your express consent to receive promotional emails about our services and emails announcing changes to, and new features on, the Site. If, at any time, you decide that you do not wish to receive any such emails, you can opt out of receiving such emails by clicking on the link at the bottom of any of the emails or by going to the My Account section of the Site.</li>
                        <li>Additionally, we do use comments made by you about the Site for marketing purposes and by making such comments you expressly consent to our using such comments for marketing purposes.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec other-personal-info">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">Your use of your and other users’ personal information.</h2>
                </div>
                
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>Site members may need to share personal information (including financial information) with each other to complete transactions on the Site. You should respect, at all times, the privacy of other Site members.</li>
                        <li>We cannot guarantee the privacy of your personal information when you share personal information with other Site members so you should always seek information on the privacy and security policies of any other Site members with whom you are transacting prior to sharing any of your personal information with another Site member.</li>
                        <li>This privacy policy does not cover your release of your personal information to another Site member.</li>
                        <li>You agree to use any personal information received from another Site member in relation to a transaction on the Site solely in relation to such transaction and shall not use the information received from another Site member for any other purposes (except with the express consent of the other Site member).</li>
                        <li>You acknowledge and agree that you shall use personal information received from another Site member in accordance with all applicable laws.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec access-personal-info">
            <div className="row">
                
                <div className="col-md-4">
                    <h2 className="capital">Accessing, reviewing and amending your personal information</h2>
                </div>
               
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>You can access and review your personal information in the My Account section of the Site. If your personal information changes in any way or is incorrectly presented on the Site you should immediately update or correct your personal information (as applicable) by accessing the My Account section on the Site or, alternatively, by contacting our customer support team. The "Customer Support" link at the top of each Site webpage contains our customer support email and phone details.</li>
                        <li>Please note that we shall retain your personal information during and following the end of your use of the Site as required to comply with law, for technical troubleshooting requirements, to prevent fraud, to assist in any legal investigations and to take any other actions otherwise permitted by law.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec third-party">
            <div className="row">
                
                <div className="col-md-4">
                    <h2 className="capital">Third party website links</h2>
                </div>
               
                <div className="col-md-8">
                    <p className="mb-0">The Site may contains links to other websites. Please be aware that we are not responsible for the privacy practices of such other websites. We encourage you when you leave the Site to read the privacy statements of each and every website you visit if you intend to provide personal information to that website. Our privacy policy applies solely to your personal information that we collect on the Site.</p>
                </div>
            </div>
        </section>
        <hr/>
       
        <section className="privacy-sec cookies">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">cookies</h2>
                </div>
               
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>Like many websites, the Site uses 'cookie' technology (small computer files placed on your computer’s hard drive). When you go to particular website pages on the Site, the cookie identifies your browser with a unique, random number.</li>
                        <li>The cookies we use do not reveal any personal information about you. Cookies help us improve your experience of the Site and also help us understand which parts of the Site are the most popular. You are always free to decline our cookies if your browser permits, although doing so may interfere with your use of the Site.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
       
        <section className="privacy-sec spam">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">No spam or spoof emails</h2>
                </div>
              
                <div className="col-md-8">
                    <p className="mb-0">We do not tolerate spam. To report Site related spam or spoof emails, please forward the email to spam@souq.com or spoof@souq.com. You may not use our communication tools to send spam or otherwise send content that would violate our User Agreement. We automatically scan and may manually filter messages to check for spam, viruses, phishing attacks and other malicious activity or illegal or prohibited content.</p>
                </div>
            </div>
        </section>
        <hr/>
       
        <section className="privacy-sec protect-info">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">Protecting your personal information</h2>
                </div>
               
                <div className="col-md-8">
                    <ol className="p-0 mb-0 pl-2">
                        <li>By providing us with personal information, you consent to the transfer of your personal information outside of the Kingdom of Saudi Arabia. We take every precaution to safeguard all your personal information from unauthorized access, use or disclosure.</li>
                        <li>All personal information is encrypted. However, the Internet is not a secure medium and we cannot guarantee the privacy of your personal information.</li>
                        <li>You must enter your username and password each time you want to access your account or make transactions on the Site.</li>
                        <li>Choose your password carefully using unique numbers, letters and special characters.</li>
                        <li>Never share your username and password with anyone.</li>
                        <li>If you are concerned that your username or password has been compromised, please contact our customer support team immediately and ensure you change your password by logging onto the My Account section of the Site.</li>
                    </ol>
                </div>
            </div>
        </section>
        <hr/>
        
        <section className="privacy-sec protect-info">
            <div className="row">
               
                <div className="col-md-4">
                    <h2 className="capital">How you can contact us about privacy questions</h2>
                </div>
               
                <div className="col-md-8">
                    <p className="mb-0">If you have questions or concerns about our collection and use of your personal information, please contact our customer support team at the "Customer Support" link at the top of any Site webpage.</p>
                </div>
            </div>
        </section>
        <hr/>
       
        <p className="mb-0 last-updated"><span>Last update:</span></p>
    </div>
</main>


<Footer/>
        </div>

        );
    }
}


export default PrivacyPolicy;

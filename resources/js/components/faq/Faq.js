

import React from 'react';
import NavBar from '../common/NavBar';
import TopNavBar from '../common/TopNavBar';
import Footer from '../common/Footer';




class Faq extends React.Component{
      
    constructor(){

        super();
        this.state={

            questions: [
                {id: 1, title: "question1",answer:"answer1"},
                {id: 2,  title: "question2",answer:"answer2"},
                {id: 3,  title: "question3",answer:"answer3"},
                {id: 4,  title: "question4",answer:"answer4"},
                {id: 5,  title: "question5",answer:"answer5"},
                {id: 6,  title: "question6",answer:"answer6"},
                {id: 7,  title: "question7",answer:"answer7"},
                {id: 8,  title: "question8",answer:"answer8"},
                {id: 9,  title: "question9",answer:"answer9"},
                {id: 10,  title: "question10",answer:"answer10"},
               
            ],
            
        };
    }
    

    render(){


        let quesJSX = this.state.questions.map((item, index) => {

            return (<div className="q-a" key={item.id}><i className="fa fa-question-circle"><span className="q">{item.title}</span></i><p class="mb-0 a">{item.answer}</p></div>);

 
        });
       
        return (


            <div>  
           
            <TopNavBar/>
            <NavBar/>

<header className="main-header about-header">
        <div className="container">
            <h1>FAQ</h1>
            <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae
                congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.
            </p>
        </div>
    </header>

  
    <main className='faq'>
        <div className="container">
           
            <section className="faq-categories text-center">
                <div className="row">
                

                    <div className="col-md-4 mb-5 mb-md-0">
                        <a id="cat-general" class="cat general active d-block" href="#">
                            <span className="d-block capital">general</span>
                            <i className="fa fa-search"></i>
                        </a>
                    </div>
                   
                    <div className="col-md-4 mb-5 mb-md-0">
                        <a id="cat-payment" className="cat payment d-block" href="#">
                            <span className="d-block capital">payment</span>
                            <i className="fa fa-paypal"></i>
                        </a>
                    </div>
                   
                    <div className="col-md-4">
                        <a id="cat-bugs" className="cat bugs d-block" href="#">
                            <span className="d-block capital">reports & bugs</span>
                            <i className="fa fa-bug"></i>
                        </a>
                    </div>
                </div>
            </section>
           
            <div className="questions-wrapper mt-5">
               
                <section>
                <div className="question d-flex align-items-start mb-4">
                <div className="icon mr-3">
                        
                        {quesJSX}
                        </div>
                       
                    </div>
                   
                </section>
                
            </div>
        </div>
    </main>



            <Footer/>
        </div>

        );
    }
}


export default Faq;

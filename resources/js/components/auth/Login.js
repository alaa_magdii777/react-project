import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import axios from 'axios';

import NavBar from '../common/NavBar';
import TopNavBar from '../common/TopNavBar';
import Footer from '../common/Footer';
import {updateLoginData, validateLoginData, doAPILogin} from "../../redux/ActionCreators";
import {API_PREFIX, URL_PREFIX} from "../../configs";

const mapStateToProps = (state) => {
    return {
        userInfo: state.reducer.userInfo,
        login: state.reducer.login,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateLoginData: (field, newValue) => {
            dispatch(updateLoginData(field, newValue));
        }, validateLoginData: () => {
            dispatch(validateLoginData());
        }, doAPILogin: (loginData) => {
            dispatch(doAPILogin(loginData));
        }
    }
};

class Login extends React.Component {

    _handleFormSubmission(ev) {
        ev.preventDefault();

        // Validation..
        this.props.validateLoginData();
        if (this.props.login.errors.length > 0) return;
        this.props.doAPILogin(this.props.login.data);
    }

    render() {
        if (this.props.userInfo.apiToken.length > 0) {
            this.props.history.push(URL_PREFIX + "home");
        }

        let errorsJSX = this.props.login.errors.map((item, index) => {
            return (
                <li className="text-danger" key={index}>
                    <span className="fa fa-caret-right mr-1"/>
                    {item}
                </li>
            );
        });

        return (
            <div>

                <main className="member signin">
                    <div className="container">
                        <form className="clearfix" onSubmit={(ev) => {
                            this._handleFormSubmission(ev);
                        }}>

                            <figure><img src="assets/images/signin-image.jpg" alt="sing up image"/></figure>
                            <div className="form-group">


                                <label htmlFor="username">Username</label>
                                <input type="text" className="form-control"
                                       onChange={(ev) => {
                                           this.props.updateLoginData('username', ev.target.value.trim());
                                       }} value={this.props.login.data.username}
                                       id="username" name="username"
                                       placeholder="Enter username"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="password" className="form-control"
                                       onChange={(ev) => {
                                           this.props.updateLoginData('password', ev.target.value.trim());
                                       }} value={this.props.login.data.password}
                                       id="password" name="password"
                                       placeholder="Password"/>
                            </div>
                            <button type="submit" className="btn primary-button float-md-right">
                                Signin
                            </button>
                            <div className="clearfix mb-3"/>
                            <p className="mb-0 extra">
                                <small>Don't have an acount? <a className="tertiary" href="#">Register</a> Now</small>
                            </p>

                            {
                                this.props.login.errors.length > 0 ? (
                                    <div className="card mt-3 mb-0">
                                        <div className="card-body">
                                            <ul className="list-unstyled mb-0">
                                                {errorsJSX}
                                            </ul>
                                        </div>
                                    </div>
                                ) : null
                            }

                        </form>
                    </div>
                </main>
            </div>
        );
    }
}

// mapStateToProps
// mapDispatchToProps

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));

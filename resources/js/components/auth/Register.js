
import React from 'react';
import axios from 'axios';
import TopNavBar from '../common/TopNavBar';





class Register extends React.Component{
      
    constructor() {

        super();
        this.state = {
            loginErrors: [],
        };
    }


    _handleFormSubmission(ev) {
        ev.preventDefault();

        let username = ev.target.username.value.trim();
        let password = ev.target.password.value.trim();
        let first_name= ev.target.first_name.value.trim();
        let last_name = ev.target.last_name.value.trim();
        
        let email    = ev.target.email.value.trim();
        let phone    = ev.target.phone.value.trim();
        let birth_date= ev.target.birth_date.value.trim();
        let address =  ev.target.address.value.trim();
        

        let loginData = {
            username : username,
            password : password,
            first_name: first_name,
            last_name : last_name ,
          
            email    : email,
            phone    :phone,
            birth_date:birth_date,
            address  :address,
         
        };

        let errors = [];

        // Validation.
        if (username.length < 6) {
            errors.push("Username must be more than or equal 6 characters.");
        } else if (username.length > 75) {
            errors.push("Username must be less than or equal 75 characters.");
        }

        if (first_name.length < 6) {
            errors.push("Firstname must be more than or equal 6 characters.");
        } else if (first_name.length > 75) {
            errors.push("Firstname must be less than or equal 75 characters.");
        }

        if (last_name.length < 6) {
            errors.push("Lastname must be more than or equal 6 characters.");
        } else if (last_name.length > 75) {
            errors.push("Lastname must be less than or equal 75 characters.");
        }

        if (password.length < 6) {
            errors.push("Password must be more than or equal 6 characters.");
        } else if (password.length > 100) {
            errors.push("Username must be less than or equal 100 characters.");
        }


        if (email.length < 6) {
            errors.push("email must be more than or equal 6 characters.");
        } else if (email.length > 100) {
            errors.push("email must be less than or equal 100 characters.");
        }


        if (address.length < 10) {
            errors.push("Address must be more than or equal 10 characters.");
        } else if (address.length > 125) {
            errors.push("Address must be less than or equal 125 characters.");
        }


        if (phone.length < 10) {
            errors.push("phone must be more than or equal 10 characters.");
        } else if (phone.length > 30) {
            errors.push("phone must be less than or equal 30 characters.");
        }


        this.setState({loginErrors: errors});

        if (errors.length > 0) return;

        

        axios.post('http://localhost/api/register').then((response) => {
            // axios.get('http://localhost/api/login').then((response) => {
            console.log(response.data);
        }).catch((error) => {
            console.log(error);
        });

        //
        let apiToken = "alaaalaaa";
        localStorage.setItem('apiToken', apiToken);

        // Redirect

    }

    render() {

        let errorsJSX = this.state.loginErrors.map((item, index) => {
            return (
                <li className="text-danger" key={index}>
                    <span className="fa fa-caret-right mr-1"></span>
                    {item}
                </li>
            );
        });
        return (

           <div>  

           
           <TopNavBar/>

<main className="member register">
    <div className="container">

        <form className="clearfix" onSubmit={(ev) => {
                            this._handleFormSubmission(ev);
                        }}>
           

            <div className="signup-image">
                <figure><img src="assets/images/signup-image.jpg" alt="sing up image"/>
                </figure>
            </div>


            <div className="form-group">
                <label htmlFor="firstname">firstname</label>
                <input type="text" className="form-control" id="firstname" name="first_name"
                       placeholder="Enter firstname"/>
            </div>
            <div className="form-group">
                <label htmlFor="lastname">lastname</label>
                <input type="text"className="form-control" id="lastname" name="last_name" placeholder="Enter lastname"/>
            </div>
            
            <div className="form-group">
                <label htmlFor="username">Username</label>
                <input type="text" className="form-control" id="username" name="username" placeholder="Enter username"/>
            </div>
            <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password" className="form-control" id="password" name="password" placeholder="Password"/>
            </div>
            <div className="form-group">
                <label htmlFor="email">email</label>
                <input type="email" className="form-control" id="email" name="email" placeholder="email"/>
            </div>
            <div className="form-group">
                <label htmlFor="phone">phone</label>
                <input type="num" className="form-control" id="phone" name="phone" placeholder="phone"/>
            </div>
            <div className="form-group">
                <label htmlFor="birthdate">Birth Date</label>
                <input type="date" className="form-control" id="birthdate" name="birth_date" placeholder="birthdate"/>
            </div>
            <div className="form-group">
                <label htmlFor="address">address</label>
                <input type="text" className="form-control" id="address" name="address" placeholder="address"/>
            </div>
           

            <button type="submit" className="btn primary-button float-md-right">Register</button>

            {
                                this.state.loginErrors.length > 0 ? (
                                    <div className="card mt-3 mb-0">
                                        <div className="card-body">
                                            <ul className="list-unstyled mb-0">
                                                {errorsJSX}
                                            </ul>
                                        </div>
                                    </div>
                                ) : null
                            }

        </form>
    </div>
</main>

  
        </div>

        );
    }
}


export default Register;


import React from 'react';
import {Link, NavLink} from 'react-router-dom';

class Product extends React.Component {

    render() {
        return (
            <div className="col-lg-4 col-md-6 mb-5 mix {{$categoryName . ' ' . $with_discount . ' ' . $is_new}}">
               
                <div className="product category pb-4 m-auto m-md-0">
                  
                    <div className="image">
                      
                        <img src={this.props.imageUrl} alt=""/>
                       
                        <span
                            className='state discount position-absolute rounded text-white'>{this.props.discount}%</span>

                        {
                           this.props.is_new ? (<span className="state new text-white rounded">New</span> ) : null
                        }

                        {
                            this.props.is_soldOut ? (<span className="state sold-out text-white rounded text-center"> Sold Out</span>) : null
                        }

                        <img src={this.props.imageUrl} alt=""/>
                        
                    </div>
                    

                  
                    <h3 className="item-name h5 text-center m-0 mt-2"> {this.props.name}</h3>
                    <h3 className="item-name h5 text-center m-0 mt-2 text-danger"> {this.props.category_id} </h3>
                    
                    <div className="wrapper text-center my-2 px-4">
                        <p className="extra mb-2">
                   <span className="mr-1 badge badge-purple badge-pill">
                        Price : {this.props.selling_price} $
                   </span>
                            {
                                this.props.is_soldOut ? null : (<span className="mr-1 badge badge-info badge-pill mt-1">Amount : {this.props.quantity}</span>)
                            }

                          
                        </p>
                        
                        <div className="wrapper d-flex justify-content-center">
                            <p id="product-rating-{{$topSale->product_id}}" className="rating mb-0"
                               data-rating="{{$productRating}}">Rate</p>
                        </div>
                        
                    </div>
                    <p className="item-summary mb-2 px-4" title="{{$product->description}}">
                        {this.props.description}
                    </p>
                   
                    <div className="buttons-wrapper text-center">
                        <button className="addToCart primary mr-2">
                            <Link className="p-0" to="/productdetails" exact>
                                Details
                            </Link>
                        </button>

                        {
                            this.props.is_soldOut ? null : (<button data-price="{{$product->selling_price}}" data-discount="{{$product->discount}}"
                                                               data-max="{{$totalAmount->q}}"
                                                               data-name="{{$product->name}}" data-imageUrl="{{asset('/assets/images/p-02-01.jpg')}}"
                                                               data-id="{{$topSale->product_id}}" className="addToCart primary">Add to Cart
                            </button>)
                        }


                    </div>

                </div>

            </div>
        );
    }
}

export default Product;
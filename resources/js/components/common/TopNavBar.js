import React from 'react';
import {Link, NavLink} from 'react-router-dom';

class TopNavBar extends React.Component {
    render() {
        return (

            <div className="top-bar">
                <div className="container">
                    <ul className="m-0 list-unstyled d-flex align-items-center">
                        <li className="mr-auto">
                            <div className="dropdown">
                                <button className="dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> EN
                                </button>
                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a className="dropdown-item" href="#">Ar</a>
                                    <a className="dropdown-item" href="#">Tu</a>
                                </div>
                            </div>
                        </li>
                        <li className="mr-3">
                            <a className="nav-link" href="#">Sign out</a>
                            <NavLink className="nav-link" activeClassName="active"
                                     exact to="/login">Sign in</NavLink>
                        </li>
                        <li className="mr-3">
                            <Link to="dashboard">
                                <i className="fa fa-tachometer-alt"></i>
                                <span className="ml-2">Dashboard</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="cart">
                                <i className="fa fa-cart-plus"></i>
                                <span className="products-number ml-2">555</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default TopNavBar;
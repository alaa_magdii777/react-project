import React from 'react';
import {Link, NavLink} from 'react-router-dom';

class NavBar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg">
                <div className="container">
                    <NavLink className="navbar-brand" to="/" exact>
                        <img className="rounded-circle" src="https://via.placeholder.com/50/#62A718/FFFFFF" alt=""/>
                    </NavLink>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon">
                            <i className="fa fa-align-center"></i>
                        </span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav w-100">
                            <li className="nav-item active">
                                <NavLink className="nav-link" to="/" exact>Home</NavLink>
                            </li>
                            <li className="nav-item dropdown">
                                <NavLink className="nav-link dropdown-toggle" activeClassName='active' to="categories" id="categories-dropdown"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Categories
                                </NavLink>
                                <div className="dropdown-menu" aria-labelledby="categories-dropdown">
                                    <NavLink className="dropdown-item" activeClassName='active' to="categories" exact>All</NavLink>
                                    <NavLink className="dropdown-item" activeClassName='active' to="Category" exact>Cat 01</NavLink>
                                </div>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link"  to="/recommendation" exact>Recommendations</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default NavBar;
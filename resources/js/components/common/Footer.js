import React from 'react';
import {Link} from 'react-router-dom';

class Footer extends React.Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-2 mb-5 mb-lg-0">
                            <div className="footer-column quik-links">
                                <h3 className="h4 mb-4">Quick Links</h3>
                                <ul className="list-unstyled list">
                                    <li className="mb-2">
                                        <Link to="aboutus">
                                            <i className="fa fa-caret-right mr-2"></i>
                                            About Us
                                        </Link>
                                    </li>
                                    {/*<li className="mb-2">*/}
                                    {/*    <a href="#">*/}
                                    {/*        <i className="fa fa-caret-right mr-2"></i>*/}
                                    {/*        Customer Service*/}
                                    {/*    </a>*/}
                                    {/*</li>*/}
                                    <li className="mb-2">
                                        <Link to="privacypolicy">
                                            <i className="fa fa-caret-right mr-2"></i>
                                            Privacy Policy
                                        </Link>
                                    </li>
                                    <li className="mb-2">
                                        <Link to="/contactus" exact>
                                            <i className="fa fa-caret-right mr-2"></i>
                                            Contact Us
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/faq" exact>
                                            <i className="fa fa-caret-right mr-2"></i>
                                            FAQ
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 mb-5 mb-lg-0">
                            <div className="footer-column contact-us">
                                <h3 className="h4 mb-4">Contact Us</h3>
                                <ul className="list-unstyled list">
                                    <li className="mb-2">
                                        <a href="#"><
                                            i className="fa fa-id-card mr-2"></i>
                                            id venenatis
                                            a condimentum vitae sapien pellentesque habitant morbi tristique senectus et
                                            netus et
                                        </a>
                                    </li>
                                    <li className="mb-2">
                                        <a href="#">
                                            <i className="fa fa-phone-square mr-2"></i>
                                            (000) 000-000-0000
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i className="fa fa-fax mr-2"></i>
                                            (000) 000-000-0000
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-6 mb-5 mb-lg-0">
                            <div className="footer-column map">
                                <h3 className="h4 mb-4">Location</h3>
                                <iframe className="w-100"
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13673.85366520079!2d31.36574148969448!3d31.041196241327384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f79dd3586ece8b%3A0x8a183606fd57cbb2!2sThe+Olympic+Village!5e0!3m2!1sen!2seg!4v1565045792920!5m2!1sen!2seg"
                                        height="450" frameBorder="0" style={{border: 0}} allowFullScreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
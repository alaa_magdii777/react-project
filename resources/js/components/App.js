import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import {Provider} from 'react-redux';
import store from './../redux/ConfigureStore';
import {URL_PREFIX} from './../configs';

import Home from "./home/Home";
import Login from "./auth/Login";
import Register from "./auth/Register";
import ProductDetails from "./product/ProductDetails";
import Category from "./category/Category";
import Cart from "./cart/Cart";
import Faq from "./faq/Faq";
import Privacypolicy from "./privacypolicy/Privacypolicy";
import Recommendation from "./recommendation/Recommendation";
import Categories from "./categories/Categories";
import ContactUs from "./contactUs/ContactUs";
import AboutUs from "./aboutUs/AboutUs";
import Dashboard from "./dashboard/Dashboard";
import Products from "./dashboard/products/Products";
// import Product from "./common/Product";

class App extends React.Component {

    render() {
        return (
            <div>
                <Provider store={store}>
                    <BrowserRouter>
                        <Switch>
                            {/*user routes*/}
                            <Route path={URL_PREFIX} exact component={Login}/>
                            <Route path={URL_PREFIX + "home"} exact component={Home}/>
                            {/*<Route path={URL_PREFIX + "register"} exact component={Register}/>*/}
                            {/*<Route path={URL_PREFIX + "faq"} exact component={Faq}/>*/}
                            {/*<Route path={URL_PREFIX + "recommendation"} exact component={Recommendation}/>*/}
                            {/*<Route path={URL_PREFIX + "categories"} exact component={Categories}/>*/}
                            {/*<Route path={URL_PREFIX + "contactus"} exact component={ContactUs}/>*/}
                            {/*<Route path={URL_PREFIX + "category"} exact component={Category}/>*/}
                            {/*<Route path={URL_PREFIX + "cart"} exact component={Cart}/>*/}
                            {/*<Route path={URL_PREFIX + "productdetails"} exact component={ProductDetails}/>*/}
                            {/*<Route path={URL_PREFIX + "contacts"} exact component={ContactUs}/>*/}
                            {/*<Route path={URL_PREFIX + "aboutus"} exact component={AboutUs}/>*/}
                            {/*<Route path={URL_PREFIX + "privacypolicy"} exact component={Privacypolicy}/>*/}

                            {/*Dashboard routes*/}
                            <Route path={URL_PREFIX + "/dashboard"} exact component={Dashboard}/>
                            <Route path={URL_PREFIX + "/dashboard/products"} exact component={Products}/>

                        </Switch>
                    </BrowserRouter>
                </Provider>
            </div>

        );
    }
}

export default App;

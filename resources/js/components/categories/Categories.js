import React from 'react';
import TopNavBar from "../common/TopNavBar";
import NavBar from "../common/NavBar";
import Footer from "../common/Footer";
import CategoryCard from "./CategoryCard";
import axios from 'axios';

class  Categories extends React.Component{
    constructor () {
        super();
        this.state = {
            categories:[],
            errors:[],

        }
    }

    _fetchCategoriesData() {
        axios.get('http://localhost/api/categories').then((response) => {
            this.setState({categories: [...response.data.data]});
        }).catch((errors) => {
            this.setState({errors: errors});
        });
    }

    render() {
        let categoriesJSX = this.state.categories.map((item, index) => {
           return <CategoryCard key={item.id} {...item}/>
        });

        return (
            <div onLoad={ () => {
                this._fetchCategoriesData();
            }}>
                <TopNavBar/>
                <NavBar/>
                <header className="main-header categories-header">
                    <div className="container">
                        <h1>Categories</h1>
                        <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.</p>
                    </div>
                </header>

                <main className="categories-page">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 mb-lg-0 mb-5">
                                <section className="categories-controls">
                                    <form>
                                        <section className="form-group sort">
                                            <h2 className="h4 text-center">Sorting</h2>
                                            <hr/>
                                            <p className="input-field m-0 px-4">
                                                <input className="mr-2" type="radio" name="sort" id="filter-name-desc"/>
                                                    <i className="fa fa-long-arrow-alt-down"></i>
                                                    <label htmlFor="filter-name-desc">Name Desc</label>
                                            </p>
                                            <p className="input-field m-0 px-4">
                                                <input className="mr-2" type="radio" name="sort" id="filter-name-asc"/>
                                                    <i className="fa fa-long-arrow-alt-up"></i>
                                                    <label htmlFor= "filter-name-asc">Name Asc</label>
                                            </p>
                                        </section>
                                        <hr/>
                                        <section className="form-group filter mb-0">
                                            <h2 className="h4 text-center">Filtering</h2>
                                            <hr/>
                                            <p className="input-field m-0 px-4">
                                                <input type="checkbox" className="mr-2" name="filer[]" id="all-ages"/>
                                                    <label htmlFor="all-ages">All Ages</label>
                                            </p>
                                            <p className="input-field m-0 px-4">
                                                <input type="checkbox" className="mr-2" name="filer[]" id="kids"/>
                                                    <label htmlFor="kids">Kids</label>
                                            </p>
                                            <p className="input-field m-0 px-4">
                                                <input type="checkbox" className="mr-2" name="filer[]" id="men"/>
                                                    <label htmlFor="men">Men</label>
                                            </p>
                                            <p className="input-field m-0 px-4">
                                                <input type="checkbox" className="mr-2" name="filer[]" id="women"/>
                                                    <label htmlFor="women" className="mb-0">Wemen</label>
                                            </p>
                                        </section>
                                    </form>
                                </section>
                            </div>
                            <div className="col-lg-9">
                                <section className="categories">
                                    <form action="" className="search-category-form">
                                        <p className="input-field d-flex">
                                            <button type="submit"><i className="fa fa-search"></i></button>
                                            <input type="text" id="search-category" name="search-category" placeholder='search'/>
                                        </p>
                                    </form>
                                    <div className="row">{categoriesJSX}</div>
                                    <div className="row">
                                        <button className="btn btn-circle btn-danger btn-lg mx-auto">See More</button>
                                    </div>
                                </section>
                            </div>
                        </div>
                </div>
            </main>
             <Footer/>
            </div>
        );
    }
}
    export default Categories;
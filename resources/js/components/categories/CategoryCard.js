import React from 'react';
import {Link} from 'react-router-dom';

class CategoryCard extends React.Component {
    render (){
        return (
            <div className="col-md-4 mb-5">
                <div className="category pb-4">
                    <div className="image">
                        <img src={this.props.imgURL} alt=""/>
                    </div>
                    <h2 className="item-name h4 text-center m-0 mt-2 font-weight-bold">
                        {this.props.name}
                    </h2>
                    <div className="wrapper text-center my-2 px-4">
                        <p className="extra mb-2"><span className="products-number">ProductNumber : {this.props.inStock}</span></p>
                        <p className='cat mb-2'><span className="mr-2">Kid</span><span
                            className="mr-2">Men</span><span>Women</span></p>
                    </div>
                    <p className="item-summary mb-0 px-4">{this.props.description}</p>
                    <div className="buttons-wrapper text-center">
                        <button className="primary-link mt-2"><Link to={'/category/' + this.props.id}>Display Products</Link></button>
                    </div>
                </div>
            </div>
        );
    }
}

export  default CategoryCard;
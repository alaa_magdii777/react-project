import React from 'react';
import axios from 'axios';
import NavBar from '../common/NavBar';
import TopNavBar from '../common/TopNavBar';
import Footer from '../common/Footer';





class AboutUs extends React.Component{
      
    constructor() {
        super();
        this.state = {
            sponser: [],
            aboutus:[],
            review:[],
            errors: [],

        };
    }



    
    _fetchAboutData() {
        axios.get('http://localhost/api/aboutus').then((response) => {
            this.setState({aboutus: [...response.data.data]});
        }).catch((errors) => {
            this.setState({errors: errors});
            
        });
    }



    _fetchSponserData() {
        axios.get('http://localhost/api/sponsers').then((response) => {
            this.setState({sponser: [...response.data.data]});
        }).catch((errors) => {
            this.setState({errors: errors});
            
        });
    }

    _fetchReviewData() {
        axios.get('http://localhost/api/reviews').then((response) => {
            this.setState({review: [...response.data.data]});
        }).catch((errors) => {
            this.setState({errors: errors});
            
        });
    }

    render() {

        let AboutJSX = this.state.aboutus.map((item, index) => {
            return (<p className="mb-2" key={item.id} >{item.vision}</p>);
            
        });


        let AboutJSXm = this.state.aboutus.map((item, index) => {
            return (<p className="mb-2" key={item.id} >{item.mission}</p>);
            
        });


        let SponserJSX = this.state.sponser.map((item, index) => {
            return (<img className="col-md-3 mb-2" key={item.id} src={item.path}/>);
            
        });



        let ReviewJSX = this.state.review.map((item, index) => {
            
                  return (<div className=" col-md-3 card-body shadow m-3" key={item.id}>{item.review} </div>)

            }); 
       

        return (
            <div onLoad={() => {
                this._fetchAboutData();
                this. _fetchSponserData() ;
                this. _fetchReviewData() ;
            }}>
            
                <TopNavBar/>
                <NavBar/>

            <header className="main-header about-header">
        <div className="container">
            <h1>About Us</h1>
            <h3>Site config</h3>
        </div>
    </header>
   
    <nav className="breadcrumb-nav mt-5 mb-5" aria-label="breadcrumb">
        <div className="container">
            <ol className="breadcrumb mb-0">
                <li className="breadcrumb-item"><a href="/">Home</a></li>
                <li className="breadcrumb-item active" aria-current="page">About Us</li>
            </ol>
        </div>
    </nav>
   
    <main className='about-us pb-5'>
        <div className="container">
           
            <div className="items-wrapper mb-5">

            <div className="row mt-5 mb-5">
 
              <div className="col-md-4">

                   <div className="image">
                        <img className="w-100" src="assets/images/about/illustration.png"/>
                    </div>

              </div>

              <div className="col-md-4">

                    <div className="image">
                        <img className="w-100" src="assets/images/about/illustration.png"/>
                    </div>

             </div>

            <div className="col-md-4">

                <div className="image">
                    <img className="w-100" src="assets/images/about/illustration.png"/>
                </div>

            </div>
 

            </div>
            <div className="row">
                   
                  
            
           
                <div className="col-md-6">
                     
                     {AboutJSX }

                </div>


                <div className="col-md-6">

                   {AboutJSXm }

                </div>
            
                
                    
                   
         
            
               </div>
            </div>  
            {/* comment */}

            
            <section className="anonymous-section">
                <div className="row">
                  
                    <div className="col-lg-6 mb-lg-0 mb-5">
                        <div>
                            <h2>OUR VISION</h2>
                        
                            <p className="lead mb-0"></p></div>
                    </div>
                  
                    <div className="col-lg-6">
                        <div>
                            <h2>OUR MISSION</h2>
                           
                            <p className="lead mb-0"></p>
                        </div>
                    </div>
                </div>
            </section>

         
            <section className="anonymous-section">
                <h2 className="text-center">What We Provide ?</h2>
                <div className="row">
                  
                    <div className="col-lg-4 mb-lg-0 mb-5">
                        <div className="wrapper">
                            <h2>Best Prices &amp; Offers</h2>

                            <div className="wrapper d-md-flex">
                               
                                <div className="image mr-3 mb-md-0 mb-3">
                                    <i className="fa fa-shopping-cart fa-3x"></i>                                </div>
                                
                                <div className="text">
                                    <p className="lead mb-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <div className="col-lg-4 mb-lg-0 mb-5">
                        <div className="wrapper">
                            <h2>Easy Returns</h2>

                            <div className="wrapper d-md-flex">
                              
                                <div className="image mr-3 mb-md-0 mb-3">
                                    <i className="fa fa-exchange-alt fa-3x"></i>
                                </div>
                                
                                <div className="text">
                                    <p className="lead mb-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    <div className="col-lg-4 mb-lg-0 mb-5">
                        <div className="wrapper">
                            <h2>Great Deals Discount </h2>

                            <div className="wrapper d-md-flex">
                                
                                <div className="image mr-3 mb-md-0 mb-3">
                                    <i className="fa fa-tag fa-3x"></i>
                                </div>
                              
                                <div className="text">
                                    <p className="lead mb-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
          
            <section className="anonymous-section">
                <h2 className="text-center"> SPONSORS </h2>

                <div className="row">
                
                    {/* <div className="col-md-4 mb-4"> */}
                        {/* <div className="image"> */}
                          {SponserJSX}
                            {/* <img src="https://lorempixel.com/400/400/cats/Faker/?74142"/> */}
                        {/* </div> */}
                    {/* </div> */}
              
                </div>
            </section>
          
            <section className="anonymous-section ml-5">
                <h2 className="text-md-center">Our Reviews </h2>
            

            
                <div className="row text-center ml-5">
               
                   {ReviewJSX}
              
                                                    
                </div> 
                {/* end f row 5 */}


            

                

            </section>

        </div>
    </main>


            <Footer/>
            </div>

        );
    }
}


export default AboutUs;

import React from 'react';
import axios from 'axios';
import TopNavBar from "../common/TopNavBar";
import NavBar from "../common/NavBar";
import Footer from "../common/Footer";

class ContactUs extends React.Component {
    constructor() {
        super();
        this.state = {
            contactInfo: [],
            contactInfoErrors: []
        }
    }

    _getContactInfoHandelr() {
        axios.get('http://localhost/api/contact-us').then((response) => {
            this.setState({contactInfo: response.data.data[0]});
            console.log(response.data.data[0]);
        }).catch((errors) => {
            this.setState({contactInfoErrors: [...this.state.contactInfoErrors, errors]});
        });
        //console.table(this.state.contactInfo['address']);
    }


    render() {
        let addressJSX = this.state.contactInfo['address'];
        let phoneOneJSX = this.state.contactInfo['phone_one'];
        let phoneTwoJSX = this.state.contactInfo['phone_two'];
        let phoneThreeJSX = this.state.contactInfo['phone_three'];
        let faxJSX = this.state.contactInfo['fax'];

        return (
            <div onLoad={() => this._getContactInfoHandelr()}>
                <TopNavBar/>
                <NavBar/>
                <header className="main-header about-header">
                    <div className="container">
                        <h1>Contact Us</h1>
                        <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut
                            labore et dolore magna aliqua. Vehicula ipsum a arcu cursus vitae congue mauris rhoncus.
                            Maecenas</p>
                    </div>
                </header>

                <nav className="breadcrumb-nav mt-3" aria-label="breadcrumb">
                    <div className="container">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="#">Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">Contact Us</li>
                        </ol>
                    </div>
                </nav>

                <main className='contact-us'>
                    <div className="container">
                        <section className="map">
                            <iframe className="w-100"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13673.85366520079!2d31.36574148969448!3d31.041196241327384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f79dd3586ece8b%3A0x8a183606fd57cbb2!2sThe+Olympic+Village!5e0!3m2!1sen!2seg!4v1565045792920!5m2!1sen!2seg"
                                    height="450" frameborder="0" style={{border: 0}} allowfullscreen></iframe>
                        </section>
                    </div>

                    <section className="contact-info">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6 mb-lg-0 px-0">
                                    <section className="info h-100 w-100">
                                        <h2 className="mb-5">Contact Information</h2>
                                        <div className="text mb-4">
                                            <h3 className="h4 mb-3">Address</h3>
                                            <p>{addressJSX}</p>
                                        </div>
                                        <div className="text">
                                            <h3 className="h4 mb-3">Telephones and Faxes</h3>
                                            <ul className="list-unstyled list mb-0">
                                                <li className="mb-2"><i className="fa-phone-volume mr-2"></i>{phoneOneJSX}
                                                </li>
                                                <li className="mb-2"><i className="fa-phone-volume mr-2"></i>{phoneTwoJSX}
                                                </li>
                                                <li><i className="fa-phone-volume mr-2 mb-2"></i>{phoneOneJSX}</li>
                                                <li><i className="fa-fax mr-2"></i>{faxJSX}</li>
                                            </ul>
                                        </div>
                                    </section>
                                </div>
                                <div className="col-lg-6">
                                    {/*@include('user.common.errors')*/}
                                    <form action="" className="contact-form" method="POST">
                                        {/*@csrf*/}
                                        <h2 className="mb-5">Your Feedback</h2>
                                        <p className="input-field m-0 mb-3 d-sm-table d-block">
                                            <label className="mb-3 d-sm-table-cell d-block" for="full-name">Name*</label>
                                            <input type="text" name="name" id="name"/>
                                        </p>
                                        <p className="input-field m-0 mb-3 d-sm-table d-block">
                                            <label className="mb-3 d-sm-table-cell d-block" for="email">Email*</label>
                                            <input type="email" name="email" id="email"/>
                                        </p>
                                        <p className="input-field m-0 mb-3 d-sm-table d-block">
                                            <label className="mb-3 d-sm-table-cell d-block" for="phone">Phone</label>
                                            <input type="text" name="phone" id="phone"/>
                                        </p>
                                        <p className="input-field m-0 d-sm-table d-block">
                                            <label className="mb-3 d-sm-table-cell d-block" for="feedback">Feedback*</label>
                                            <textarea type="text" name="feedback" id="feedback"> </textarea>
                                        </p>
                                        <div className="button-wrapper text-sm-right text-center">
                                            <button className="mt-3 p-2" type="submit">
                                                Send
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
                <Footer/>
            </div>
        );
    }
}

export default ContactUs;
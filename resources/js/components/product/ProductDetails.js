import React from 'react';
import TopNavBar from "../common/TopNavBar";
import NavBar from "../common/NavBar";
import Footer from "../common/Footer";
import axios from 'axios';

class ProductDetails extends React.Component {

    constructor() {
        super();
        this.state = {};
    }
    _fetchProductsData() {

        axios.get('http://localhost/api/products').then((response) => {
            console.log(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
    render() {
        return (
            <div onLoad={ () => {
                this._fetchProductsData();
            }}>
                <TopNavBar/>
                <NavBar/>
                <header className="main-header product-details-header">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2 order-md-last order-first mb-5 mb-md-0">
                                <div className="availability float-md-right float-none">
                                    Available
                                </div>
                            </div>
                            <div className="col-md-10">
                                <h1> Product Name</h1>
                                <p className='lead'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Vehicula ipsum a arcu
                                    cursus vitae congue mauris rhoncus. Maecenas accumsan lacus vel facilisis.
                                </p>
                            </div>
                        </div>
                    </div>
                </header>

                <main className='product-details-page'>
                    <div className="container">
                        <section className="product-overview mb-5">
                            <div className="row">
                                <div className="col-lg-7">
                                    <div className="info">
                                        <h2 className="px-4">productName</h2>
                                        <hr/>
                                            <ul className="list-unstyled px-4">
                                                <li className="mb-2">Price:
                                                    <span>productPrice</span>
                                                </li>
                                                <li className="mb-2">Discount:
                                                    <span>productDiscount</span>
                                                </li>
                                                <li className="mb-2">Description:
                                                    <span>productDesc</span>
                                                </li>
                                                <li className="mb-2">Price After Discount :
                                                    <span>555</span>
                                                </li>
                                                <li className="mb-2">Age Categories:
                                                    <span>productKeyWords</span>
                                                </li>
                                                <li>Categories:
                                                    <span>Category 01,Category 02</span>
                                                </li>
                                            </ul>
                                    </div>
                                </div>
                                <div className="col-lg-5 mb-lg-0 mb-5 order-lg-last order-first">
                                    <div className="image h-100 text-center text-lg-right">
                                        <img className="h-100" src="https://via.placeholder.com/200x200/#62A718/FFFFFF" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className='product-details'>
                            <section className="details specific mb-4">
                                <h3 className="d-flex align-items-center px-4 h4 mb-0">
                                    <span>Other Specific Details</span>
                                    <i id="expand-specificDetails" className="fa fa-caret-right expand-details ml-auto"></i>
                                </h3>
                                <div className="dropdown-wrapper px-4" id="specificDetails">
                                    <hr/>
                                        <p>Sagittis nisl rhoncus mattis rhoncus urna neque viverra justo. Habitant morbi
                                            tristique senectus et netus et malesuada. Dignissim convallis aenean et
                                            tortor at risus viverra. Tellus molestie nunc non blandit massa enim nec
                                            dui. Morbi tristique senectus et netus et. Nisl condimentum id venenatis a
                                            condimentum vitae. Commodo odio aenean sed adipiscing diam. Velit dignissim
                                            sodales ut eu sem. Tincidunt lobortis feugiat vivamus at augue eget arcu
                                            dictum varius. Ipsum dolor sit amet consectetur adipiscing. Turpis massa sed
                                            elementum tempus egestas. Neque aliquam vestibulum morbi blandit cursus
                                            risus at ultrices. Molestie at elementum eu facilisis sed. Non quam lacus
                                            suspendisse faucibus interdum posuere. Viverra orci sagittis eu volutpat
                                            odio facilisis. Mi tempus imperdiet nulla malesuada.</p>

                                </div>
                            </section>

                            <section className="details description mb-4">
                                <h3 className="d-flex align-items-center px-4 h4 mb-0"><span>Product Description</span>
                                    <i id="expand-description" className="fa fa-caret-right expand-details ml-auto"></i>
                                </h3>
                                <div className="dropdown-wrapper px-4" id="description">
                                    <hr/>
                                        <p> productDesc</p>
                                </div>
                            </section>

                            <section className="details images mb-5">
                                <h3 className="d-flex align-items-center px-4 h4 mb-0">
                                    <span>More Images on the Product</span>
                                    <i id="expand-productImages" className="fa fa-caret-right expand-details ml-auto"></i>
                                </h3>
                                <div className="dropdown-wrapper px-4" id="productImages">
                                    <hr/>
                                        <div className="images-wrapper d-md-flex">
                                            <div className="image mb-3">
                                                <img src="https://via.placeholder.com/100/#62A718/FFFFFF" alt=""/>
                                            </div>

                                        </div>
                                </div>
                            </section>

                        </section>
                            </div>
                        </main>


                <Footer/>
            </div>


        );
    }
}

export default ProductDetails;
import React from 'react';
import NavBar from "../common/NavBar";
import TopNavBar from "../common/TopNavBar";
import Footer from "../common/Footer";
import {Link} from "react-router-dom";

class Cart extends React.Component{
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <TopNavBar/>
                <NavBar/>
                <main className="cart py-5">
                    <div className="container">
                        <div className="empty-cart text-center">
                            <h1>Your cart is empty</h1>
                            <p className="mb-5">Go to <Link className="tertiary" to="/">shopping </Link>now</p>
                            <img className="w-100" src="assets/images/cart/empty-cart.svg" alt=""/>
                        </div>

                        <table id="cart-table"
                               data-toolbar="#toolbar"
                               data-search="true"
                               data-show-refresh="true"
                               data-show-toggle="true"
                               data-show-fullscreen="true"
                               data-show-columns="true"
                               data-show-columns-toggle-all="true"
                               data-detail-view="true"
                               data-show-export="true"
                               data-click-to-select="true"
                               data-detail-formatter="detailFormatter"
                               data-minimum-count-columns="2"
                               data-show-pagination-switch="true"
                               data-pagination="true"
                               data-id-field="id"
                               data-page-list="[10, 25, 50, 100, all]"
                               data-show-footer="true"
                               data-side-pagination="server"
                               data-url="https://examples.wenzhixin.net.cn/examples/bootstrap_table/data"
                               data-response-handler="responseHandler">
                            <thead class="upper">
                            <th>selected items</th>
                            <th>quantity</th>
                            <th>unit price</th>
                            <th>edit</th>
                            <th>subtotal</th>
                            </thead>
                            <tbody id="cartBody">
                            </tbody>
                        </table>
                        <form class="cart-controls" action="" method="POST">
                            <input class="mr-3" type="checkbox" name='select-all' id="select-all"/>
                            <label for="select-all">Select all</label>
                            <button class="secondary" type="button"><a href="">Delete selected</a></button>
                        </form>
                    </div>
                </main>
                <Footer/>
            </div>
        );
    }

}

export default Cart;
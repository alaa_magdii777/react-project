import React from 'react';
import SideBar from './common/SideBar';
import NavBar from './common/NavBar';
import axios from 'axios';
//import {withRouter} from 'react-router';
//import Footer from './common/Footer';

class Dashboard extends React.Component{
    constructor() {
        super();
        this.state = {
            productsNumber: []
        }
    }

    _getDashDataHandler() {
        console.log('no');
        axios.get('http://localhost/api/dashboard/').then((response)=> {
            console.log(response.data.data.count);
            this.setState({productsNumber: response.data.data.count});
        }).catch((errors)=> {
                console.log(errors);
        });
    }
    render() {
        return(
            <div id="wrapper" onLoad={() => {
                console.log('no');
                this._getDashDataHandler();
            }
            }>
                <SideBar/>
                <div className="modal fade" id="logoutModal" tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">Select "Logout" below if you are ready to end your current
                                session.
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <a className="btn btn-primary" href="/logout">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="content-wrapper" className="d-flex flex-column">
                    <div id="content">
                        <NavBar/>
                        <div className="dash dash-page">
                            <div className="wrapper">
                                <div className="container-fluid dash">
                                    <h1 className="dash-title mb-5">Dashboard</h1>
                                    <div className="row">
                                        <div className="col-lg-4 col-md-5 mb-4">
                                            <a className="card text-white bg-primary mb-3 mx-auto mx-md-0" style={{maxWidth: 18 +'rem'}} href="dashboard/products/">
                                                <div className="card-body">
                                                    <h5 className="card-title d-flex justify-items-center">
                                                        <i className="fa fa-shopping-cart mr-2"></i>
                                                        <span className="mr-auto">Products</span>
                                                        <span className="badge badge-light text-muted">{this.state.productsNumber}</span>
                                                    </h5>
                                                    <p className="card-text">View, Add, Delete, Update Products</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-lg-4 col-md-5 mb-4">
                                            <a className="card text-white bg-success mb-3 mx-auto mx-md-0 " style={{maxWidth: 18 +'rem'}} href="dashboard/categories/add-category">
                                                <div className="card-body">
                                                    <h5 className="card-title">Categories</h5>
                                                    <p className="card-text">View, Add, Delete, Update Categories</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div className="col-lg-4 col-md-5">
                                            <a className="card text-white bg-warning mb-3 mx-auto mx-md-0" style={{maxWidth: 18 +'rem'}} href="dashboard/support/">
                                                <div className="card-body">
                                                    <h5 className="card-title"><i className="fa fa-comment-alt mr-2"></i>Support</h5>
                                                    <p className="card-text">Receive & Respond to user's Issues</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<Footer/>*/}
            </div>
        );
    }
}

export  default  Dashboard;
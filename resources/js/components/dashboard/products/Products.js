import React from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

class Products extends React.Component{
    constructor() {
        super();
        this.state = {
            products: []
        }
    }

    render() {
        return (
            <div className="dash products-page">
                <div className="wrapper">
                    <div className="container clearfix">
                        <h1 className="dash-title mb-5">Products</h1>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                                <li className="breadcrumb-item active" aria-current="page">Products</li>
                            </ol>
                        </nav>

                        <button type="button" className="btn btn-primary primary float-right">
                            <a href="/dashboard/products/create"> <i className="fas fa-fw fa-plus-circle"></i>
                                Add Products</a>
                        </button>
                        <div class="badge badge-light btn float-left">{/*{{$productsNumber}}*/} Products found</div>
                        <div className="clearfix"></div>

                        <main>
                            <div className="row">
                                <table className="text-center" data-toggle="table">
                                    <thead className="bg-primary text-light">
                                    <tr>
                                        <th>NO.</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Quantity</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {/*@foreach($products as $product)
                                    @php
                                    $imageObj = \App\Image::where('product_id', '=', $product->id)->first();
                                    $image = $imageObj->path . $imageObj->name;
                                    $quantity = \App\WarehouseProduct::Select(DB::raw('sum(quantity) as q'))
                                    ->where('product_id', '=', $product->id)->first()['q'] ?? 0;
                                    $counter++;
                                    @endphp*/}
                                    <tr>
                                        <td>{/*{{($products->currentpage() - 1) * $products->perpage() + $counter}}*/}</td>
                                        <td>{/*{{$product->name}}*/}</td>
                                        <td>{/*{{$product->buying_price}}*/}$</td>
                                        <td>{/*{{$product->discount}}*/}%</td>
                                        <td>{/*{{$quantity}}*/}</td>
                                        <td><Link to="/dashboard/products/{{$product->id}}/edit"
                                               className="align-self-start mt-auto">View Product</Link></td>
                                        <td><Link to="/dashboard/products/{{$product->id}}/delete"
                                               className="align-self-start mt-auto">Delete Product</Link></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="pagination-wrapper mt-5">
                                {/*{{$products->links()}}*/}
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

export default Products;
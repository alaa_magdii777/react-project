import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';


// Packages
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import './components/dashboard/sb-admin-2.css';

import 'jquery/dist/jquery.min';
import 'popper.js/dist/umd/popper.min';
import 'bootstrap/dist/js/bootstrap.min';


ReactDOM.render(<App />, document.getElementById('root'));

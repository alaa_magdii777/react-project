<?php

Route::get('/v2', function (){
    return view('react.index');
});



// Authentication routes
Route::get('/login', 'AuthController@getLoginView')->name('login');
Route::post('/login', 'AuthController@doLogin');
Route::get('/logout', 'AuthController@doLogout');

// User routes
Route::get('/', 'IndexController@index');
Route::get('/categories', 'CategoriesController@index');
Route::get('/{catID}/category', 'CategoryController@getCatView');
Route::get('/{prodID}/product-details', 'ProductDetailsController@getDetailsView');
Route::get('/cart', 'CartController@getCart');
Route::get('/recommendations', 'RecommendationsController@index');
Route::get('/about', 'AboutController@index');
Route::get('/faq', 'FAQController@getFAQView');
Route::get('/privacypolicy', 'PrivacyPolicyController@index');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/contact-us', 'ContactUsController@index')->name('contact-us');
    Route::post('/contact-us', 'ContactUsController@sendGuestMessage');
    Route::get('/cart', 'CartController@index');
    Route::get('/add-to-cart-{productID}', 'CartController@doAddToCart');
    Route::get('/delete-cart-item-{itemID}', 'CartController@doDeleteItem');
});

Route::group([], function () {
    Route::get('/register', 'RegisterController@index')->name('register');
    Route::post('/register', 'RegisterController@NewUser');
});

//Dashboard routes
Route::group(['middleware' => ['isAdmin']], function () {
    // Best Practice:
    Route::get('/dashboard', 'AuthDashController@getDashView');
    Route::get('/dashboard/products', 'ProductsDashController@getIndexView');
    Route::get('/dashboard/products/create', 'ProductsDashController@getAddProductView');
    Route::post('/dashboard/products/create', 'ProductsDashController@doAddProduct');
    Route::get('/dashboard/products/{productID}/delete', 'ProductsDashController@getDeleteProductView');
    Route::get('/dashboard/products/{productID}/edit', 'ProductsDashController@getEditView');
    Route::post('/dashboard/products/{productID}/edit', 'ProductsDashController@doEditProduct');
    Route::get('/dashboard/categories/add-category', 'CategoriesDashController@getAddCategoryView');
    Route::get('/dashboard/categories/category-form', 'CategoriesDashController@formView');
    Route::post('/dashboard/categories/category-form', 'CategoriesDashController@formAddCategory');
});

Route::group(['middleware' => ['isSupport']], function () {
    Route::get('/dashboard/support/', 'SupportDashController@getSupportView');
});

//Test routes
Route::get('/test1', 'TestController@test1');
Route::post('/test1', 'TestController@test2');


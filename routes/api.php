<?php

//use Illuminate\Http\Request;
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => ['CORS']], function () {
    //Auth controllers
    Route::post('/login', 'APIControllers\AuthAPIController@doLogin');
    Route::post('/register','APIControllers\RegisterAPIController@doLogin');
    //User controllers
    Route::Post('/contact-us', 'APIControllers\ContactUsAPIController@doSendMessage');
});

Route::get('/products', 'APIControllers\ProductsAPIController@getProducts');
Route::get('/products/{pid}', 'APIControllers\ProductsAPIController@getSelectedProduct');
Route::get('/sliders', 'APIControllers\SlidersAPIController@getSliders');
Route::get('/order_details', 'APIControllers\TopSalesAPIController@getProductDetails');


Route::get('/categories', 'APIControllers\CategoriesAPIController@getCategories');
Route::get('/contact-us', 'APIControllers\ContactUsAPIController@getContactInfo');
Route::get('/categories/{pid}', 'APIControllers\CategoriesAPIController@getSelectedCategory');
Route::get('/aboutus', 'APIControllers\AboutusAPIController@getAboutus');
Route::get('/sponsers', 'APIControllers\SponsersAPIController@getSponsers');
Route::get('/reviews', 'APIControllers\ReviewsAPIController@getReviews');


Route::get('/chats', 'APIControllers\ChatsAPIController@getMessages');
//Dashboard Routes
Route::get('/dashboard/products', 'APIControllers\Dashboard\ProductsDashAPIController@getProducts');
Route::get('/dashboard/products/{productID}/edit', 'APIControllers\Dashboard\ProductsDashAPIController@getProduct');
Route::get('/dashboard/categories', 'APIControllers\Dashboard\CategoriesDashAPIController@getCategories');
Route::get('/dashboard', 'APIControllers\Dashboard\DashAPIController@getDashData');


